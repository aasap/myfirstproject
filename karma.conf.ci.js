var path = require('path');
var webpack = require('webpack');
var  puppeteer = require('puppeteer');
process.env.CHROME_BIN = puppeteer.executablePath();

module.exports = function(config) {
  config.set({
    //basePath: (path.resolve(__dirname, '../')),
    frameworks: ['jasmine'],
    files: [
      {
        pattern: 'node_modules/moment/moment.js'
      },
      {
        pattern: 'client/app/scripts/apput.js'
      },
      {
        pattern: 'node_modules/angular-mocks/angular-mocks.js'
      },
      {
        pattern: 'test/spec/app.controller.js'
      },
      {
        pattern: 'test/spec/**/**/*.js'
      }
    ],
    preprocessors: {
      'client/app/scripts/apput.js': ['webpack']
    },

    webpack: {
      entry: './client/app/scripts/apput.js',
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: [/app\/lib/, /node_modules/],
            use: [
              {
                loader: 'babel-loader?presets[]=env'
              },
              {
                loader: 'istanbul-instrumenter-loader',
                options: {
                  esModules: true
                }
              }
            ]
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: 'ngtemplate-loader?relativeTo=' + path.resolve(__dirname, './client/app')
              },
              {
                loader: 'html-loader'
              }
            ]
          },
          {
            test: /\.(css|scss|sass)$/,
            use: [
              {
                loader: 'css-loader'
              },
              {
                loader: 'sass-loader'
              }
            ]
          },
          {
            test: /\.less$/,
            use: [
              {
                loader: 'css-loader'
              },
              {
                loader: 'less-loader'
              }
            ]
          },
          {
            test: /\.(woff(2)?|eot|ttf|svg)(.?[a-z0-9=.]+)?$/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: 'assets/fonts/[name].[ext]'
                }
              }
            ]
          },
          {
            test: /\.(png|jpeg|jpg)(.?[a-z0-9=.]+)?$/,
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: 'assets/images/[name].[ext]'
                }
              }
            ]
          }
        ]
      },
      plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery',
          'window.jQuery': 'jquery',
          moment: 'moment'
        }),
        new webpack.DefinePlugin({
          $$DEBUG_INFO_ENABLED: true,
          $$REDIRECTION_DISABLED: true
        })
      ],
      output: {
        filename: '[name].bundle.js',
        publicPath: '/',
        path: path.resolve(__dirname, 'test')
      }
    },

    reporters: ['coverage', 'junit', 'progress'],

    port: 8099,
    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true,
    colors: true,
    autoWatch: false,
    browsers: ['ChromeHeadlessNoSandbox'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },
    concurrency: Infinity,
    junitReporter: {
      outputFile: 'target/surefire-reports/TEST-results.xml'
    },
    coverageReporter: {
      reporters: [
        {
          type: 'cobertura',
          dir: 'results/',
          file: 'coverage.xml'
        },
        {
          type: 'lcov',
          dir: 'results/',
          subdir: '.'
        },
        {
          type: 'html',
          dir: 'coverage/'
        }
      ]
    },

    jasmineDiffReporter: {
      pretty: true
    },

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO ||
    // LOG_DEBUG
    logLevel: config.LOG_ERROR
  });
};
