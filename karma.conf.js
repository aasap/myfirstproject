var path = require('path');
var webpack = require('webpack');
var HappyPack = require('happypack');

module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],
    files: [
      {
        pattern: 'node_modules/moment/moment.js'
      },
      {
        pattern: 'client/app/scripts/apput.js'
      },
      {
        pattern: 'node_modules/angular-mocks/angular-mocks.js'
      },
      {
        pattern: 'test/spec/app.controller.js'
      },
      {
        pattern: 'test/spec/**/*.js'
      },
      {
        pattern: 'test/spec/**/**/*.js'
      }
    ],
    preprocessors: {
      'client/app/scripts/apput.js': ['webpack']
    },
    webpack: {
      entry: './client/app/scripts/apput.js',
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: [/app\/lib/, /node_modules/],
            use: [
              {
                loader: 'happypack/loader?id=js'
              },
              {
                loader: 'istanbul-instrumenter-loader',
                options: {
                  esModules: true
                }
              }
            ]
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: 'ngtemplate-loader?relativeTo=' + path.resolve(__dirname, './client/app')
              },
              {
                loader: 'happypack/loader?id=html'
              }
            ]
          },
          {
            test: /\.(css|scss|sass)$/,
            use: [
              {
                loader: 'css-loader'
              },
              {
                loader: 'sass-loader'
              }
            ]
          },
          {
            test: /\.less$/,
            use: [
              {
                loader: 'css-loader'
              },
              {
                loader: 'less-loader'
              }
            ]
          },
          {
            test: /\.(woff(2)?|eot|ttf|svg)(.?[a-z0-9=.]+)?$/,
            use: [
              {
                loader: 'happypack/loader?id=file-fonts'
              }
            ]
          },
          {
            test: /\.(png|jpeg|jpg)(.?[a-z0-9=.]+)?$/,
            use: [
              {
                loader: 'happypack/loader?id=file-images'
              }
            ]
          }
        ]
      },
      plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery',
          'window.jQuery': 'jquery',
          moment: 'moment'
        }),
        new HappyPack({
          id: 'js',
          threads: 3,
          loaders: ['babel-loader?presets[]=env']
        }),
        new HappyPack({
          id: 'file-fonts',
          threads: 2,
          loaders: [
            {
              loader: 'file-loader',
              options: {
                name: 'assets/fonts/[name].[ext]'
              }
            }
          ]
        }),
        new HappyPack({
          id: 'file-images',
          threads: 2,
          loaders: [
            {
              loader: 'file-loader',
              options: {
                name: 'assets/images/[name].[ext]'
              }
            }
          ]
        }),
        new HappyPack({
          id: 'html',
          threads: 2,
          loaders: ['html-loader']
        }),
        new webpack.DefinePlugin({
          $$DEBUG_INFO_ENABLED: true,
          $$REDIRECTION_DISABLED: true
        })
      ],
      output: {
        filename: '[name].bundle.js',
        publicPath: '/',
        path: path.resolve(__dirname, 'test')
      }
    },

    reporters: ['coverage', 'junit', 'progress', 'growl'],
    port: 8099,
    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,
    colors: true,
    autoWatch: true,
    browsers: ['Chrome'],

    // you can define custom flags
    customLaunchers: {
      PhantomJS2_custom: {
        base: 'PhantomJS2',
        options: {
          windowName: 'my-window',
          settings: {
            webSecurityEnabled: false
          }
        },
        flags: ['--load-images=true'],
        debug: false
      }
    },

    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    },

    junitReporter: {
      outputFile: 'target/surefire-reports/TEST-results.xml'
    },
    coverageReporter: {
      reporters: [
        {
          type: 'cobertura',
          dir: 'results/',
          file: 'coverage.xml'
        },
        {
          type: 'lcov',
          dir: 'results/',
          subdir: '.'
        },
        {
          type: 'html',
          dir: 'coverage/'
        }
      ]
    },

    jasmineDiffReporter: {
      pretty: true
    },

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO ||
    // LOG_DEBUG
    logLevel: config.LOG_DEBUG

    // Uncomment the following lines if you are using grunt's server to run the
    // tests
    /*proxies: {
            '/ora-internal-provider-api': 'http://127.0.0.1:8080'
        },*/
  });
};
