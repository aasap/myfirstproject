var webpack = require('webpack');
var path = require('path');
var config = require('./webpack.config');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CompressionPlugin = require('compression-webpack-plugin');

var ExtractTextPlugin = require('extract-text-webpack-plugin');
let extractCSS = new ExtractTextPlugin('css/vendor.css');
let extractLESS = new ExtractTextPlugin('css/style.css');

config.output = {
  filename: '[name].bundle.js',
  publicPath: '',
  pathinfo: false,
  path: path.resolve(__dirname, '.tmp')
};

config.cache = false;

config.module.rules = config.module.rules.concat([
  {
    test: /\.js$/,
    exclude: [/app\/lib/, /node_modules/],
    use: [
      {
        loader: 'ng-annotate-loader'
      },
      {
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    ]
  },
  {
    test: /\.html$/,
    use: [
      {
        loader: 'ngtemplate-loader?relativeTo=' + path.resolve(__dirname, './client/app')
      },
      {
        loader: 'html-loader'
      }
    ]
  },
  {
    test: /\.(css|scss|sass)$/,
    use: extractCSS.extract({
      fallback: 'style-loader',
      use: [
        {
          loader: 'css-loader',
          options: {
            url: false,
            minimize: true
          }
        },
        {
          loader: 'sass-loader'
        }
      ]
    })
  },
  {
    test: /\.less$/,
    use: extractLESS.extract({
      fallback: 'style-loader',
      use: [
        {
          loader: 'css-loader',
          options: {
            url: false,
            minimize: true
          }
        },
        {
          loader: 'less-loader'
        }
      ]
    })
  },
  {
    test: /\.(woff(2)?|eot|ttf|svg)(.?[a-z0-9=.]+)?$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[hash].[ext]'
        }
      }
    ]
  },
  {
    test: /\.(png|jpeg|jpg)(.?[a-z0-9=.]+)?$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          name: 'assets/images/[name].[ext]'
        }
      },
      {
        loader: 'img-loader',
        options: {
          gifsicle: {
            interlaced: false
          },
          mozjpeg: {
            progressive: true,
            arithmetic: false
          },
          optipng: false, // disabled
          pngquant: {
            floyd: 0.5,
            speed: 2
          },
          svgo: {
            plugins: [
              {
                removeTitle: true
              },
              {
                convertPathData: false
              }
            ]
          }
        }
      }
    ]
  }
]);

config.plugins = config.plugins.concat([
  //Use to export css in external files vendor.css and style.css
  extractCSS,
  extractLESS,

  //Disable ng-scope and ng-binding from generated HTML
  new webpack.DefinePlugin({
    $$DEBUG_INFO_ENABLED: false,
    $$REDIRECTION_DISABLED: false
  }),

  new CompressionPlugin({
    asset: '[path].gz[query]',
    algorithm: 'gzip',
    test: /\.js$|\.css$/,
    threshold: 10240,
    minRatio: 0.8
  }),

  // Reduces bundles total size
  new webpack.optimize.UglifyJsPlugin({
    sourceMap: true,
    warnings: false, // Suppress uglification warnings
    pure_getters: true,
    //unsafe: true,
    //unsafe_comps: true,
    screw_ie8: true,
    //keep_fnames: true,
    //beautify: true,
    mangle: {
      // You can specify all variables that should not be mangled.
      // For example if your vendor dependency doesn't use modules
      // and relies on global variables. Most of angular modules relies on
      // angular global variable, so we should keep it unchanged
      except: ['require', 'angular', '_', 'd3', 'moment', '$']
    },
    output: {
      comments: false
    },
    exclude: [/\.min\.js$/gi] // skip pre-minified libs
  }),

  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

  new webpack.NoEmitOnErrorsPlugin(),

  new HtmlWebpackPlugin({
    template: 'client/app/index.html',
    inject: 'body',
    hash: true
  }),

  new CopyWebpackPlugin([
    {
      from: 'client/app/.htaccess',
      to: '../dist'
    },
    {
      from: 'client/app/403.html',
      to: '../dist'
    },
    {
      from: 'client/app/404.html',
      to: '../dist'
    },
    {
      from: 'client/app/favicon.ico',
      to: '../dist',
      copyUnmodified: true
    },
    {
      from: 'client/app/maintenance.html',
      to: '../dist',
      transform: function(content, path) {
        return content.toString().replace('<base href="/" />', '<base href="/oragui-msccr/" />');
      }
    },
    {
      from: 'client/app/timeout.html',
      to: '../dist',
      transform: function(content, path) {
        return content.toString().replace('<base href="/" />', '<base href="/oragui-msccr/" />');
      }
    },
    {
      from: 'client/app/timeoutext.html',
      to: '../dist',
      transform: function(content, path) {
        return content.toString().replace('<base href="/" />', '<base href="/oragui-msccr/" />');
      }
    },
    {
      from: 'client/app/config',
      to: '../dist/config'
    },
    {
      from: 'client/app/mocks',
      to: '../dist/mocks'
    },
    {
      from: 'client/app/i18n',
      to: '../dist/i18n'
    }
  ])
]);

module.exports = config;

        except: ['angular','_','d3','moment','$','$uibModal','$scope','$uibModalInstance']  