'use strict';

describe('Directive: tipsDirective', function () {

    // load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    var element,
        scope,
        element;

    beforeEach(angular.mock.inject(function ($rootScope, $compile, $httpBackend, _appConfig_) {
        scope = $rootScope.$new();

        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});

        element = $compile('<span oratips></span>')(scope);
        scope.$digest();
    }));

    afterEach(angular.mock.inject(function ($httpBackend) {
        $httpBackend.flush();
    }));

    it('element should be defined', inject(function () {
        expect(element).toBeDefined();
    }));

});