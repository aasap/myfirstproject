'use strict';

describe('Service: histobusiness', function() {
    // load the service's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // instantiate service
    var $httpBackend, paiConstant, histobusiness, oraI18n;

    beforeEach(angular.mock.inject(function(_$httpBackend_, _paiConstant_, _histobusiness_, _oraI18n_) {
        $httpBackend = _$httpBackend_;
        paiConstant = _paiConstant_;
        histobusiness = _histobusiness_;
        oraI18n = _oraI18n_;
        oraI18n.oraI18nLabels = {};
    }));


    
    it('computeFillColorPaiComponent should return fill-transparent', function() {
        var month = {

               month: "2016-08",
               onPeriod: false,
               measure: 6.0,
               contractualThreshold: 18.9,
               applicability: {
                    applicable: true
                },
            contractualCommitmentFulfilled: true,
            metrics : []
        };
        var result = 'fill-transparent';

        expect(histobusiness.computeFillColorPaiComponent(month, 'fill-white', 'fill-transparent'))
            .toEqual(result);
    });
    
    it('computeFillColorPaiComponent should return fill_white', function() {
        var month = {

               month: "2016-08",
               onPeriod: false,
               measure: 6.0,
               contractualThreshold: 18.9,
               applicability: {
                    applicable: false
                },
            contractualCommitmentFulfilled: true,
            metrics : []
        };
        var result = 'fill-white';

        expect(histobusiness.computeFillColorPaiComponent(month, 'fill-white'))
            .toEqual(result);
    });
    
    it('computeFillColorPaiComponent should return fill_white', function() {
        var month = {

               month: "2016-08",
               onPeriod: false,
               contractualThreshold: 18.9,
               applicability: {
                    applicable: true
                },
            contractualCommitmentFulfilled: true,
            metrics : []
        };
        var result = 'fill-white';

        expect(histobusiness.computeFillColorPaiComponent(month, 'fill-white'))
            .toEqual(result);
    });
    
    it('computeFillColorPaiComponent should return fill-brand-blue', function() {
        var month = {

               month: "2016-08",
               onPeriod: false,
               measure: 0,
               contractualThreshold: 18.9,
               applicability: {
                    applicable: true
                },
            contractualCommitmentFulfilled: true,
            metrics : []
        };
        var result = 'fill-brand-blue';

        expect(histobusiness.computeFillColorPaiComponent(month, 'fill-white', 'fill-brand-blue'))
            .toEqual(result);
    });
    

    it('calculate the right dimensions', function() {
        var values = {
            size: {
                top: 10,
                right: 10,
                bottom: 10,
                left: 10,
                height: 10
            },
            histogramId: 'histoGTR'
        };
        var result = {
            'margin': {
                'top': 10,
                'right': 10,
                'bottom': 10,
                'left': 10
            },
            'width': -20,
            'height': -10
        }
        expect(histobusiness.calculateDimension(values))
            .toEqual(result);
    });
    
    //************PAI new component
    it('#formatDataPaiComponent should be format the values', function() {
        oraI18n.oraI18nLabels.paiEligCondLabel = "Conditions d'éligibilité";
        var indicator = {
        	  name: "proactivityRate",
                  shortLabel: "Taux de proactivité short",
                  longLabel: "Taux de proactivité",
                  contractual: false,
                  unit: "%",
                  applicability: {
                      applicable: true
                  },
        	  metricsDefinitions: [
        	     {
                      name: "clientResponsibilityRate",
                      shortLabel: "clientResponsibilityRate short",
                      longLabel: "clientResponsibilityRate long",
                      unit: "%",
                      applicabilityCriteria: true,
                      category: {
                          name: "",
                          label: ""
                      }
                  },
                  {
                      name: "totalIncidentNumber",
                      shortLabel: "totalIncidentNumber short",
                      longLabel: "totalIncidentNumber long",
                      unit: "%",
                      applicabilityCriteria: true,
                      category: {
                          name: "",
                          label: ""
                      }
                  },
                  {
                      name: "totalAccessNumber",
                      shortLabel: "totalAccessNumber short",
                      longLabel: "totalAccessNumber long",
                      unit: "",
                      applicabilityCriteria: true,
                      category: {
                          name: "" ,
                          label: ""
                      }
                  }
              ],
        	monthlyHistory: [
        	                    {
        	                            month: "2016-10",
        	                            onPeriod: false,
        	                            measure: 95,
        	                            contractualThreshold: 75,
        	                            applicability: {
        	                                applicable: true
        	                            },
        	                            contractualCommitmentFulfilled: true,
        	                            metrics: [{
        	                                    name: "clientResponsibilityRate",
        	                                    measure: 25,
        	                                    evolution: 8.1,
        	                                    evolutionRate: 8.1,
        	                                    applicability: {
        	                                        applicable: true
        	                                    }
        	                                },
        	                                {
        	                                    name: "totalIncidentNumber",
        	                                    measure: null,
        	                                    evolution: 5,
        	                                    evolutionRate: 8.2,
        	                                    applicability: {
        	                                        applicable: false
        	                                    }
        	                                },
        	                                {
        	                                    name: "totalAccessNumber",
        	                                    measure: 10,
        	                                    evolution: 8.2,
        	                                    evolutionRate: 8.2,
        	                                    applicability: {
        	                                        applicable: true
        	                                    }
        	                                }
        	                            ]
        	                        }
        	                    ]
        };
        var seriesClass = {
            paiOrange: {
                data: 1475280000000,
                fn: null
            },
            paiCust: {
                data: 1475280000000,
                fn: null
            },
            paiOrangeThreshold: {
                data: 1475280000000,
                fn: null
            },
            paiCustThreshold: {
                data: 1475280000000,
                fn: null
            }
        }

        var result = {
            dataTable: {
                value: {
                    title: 'Taux de proactivité short',
                    data: [{
                        date: 1475280000000,
                        value: '95',
                        bgColor: 'fill-transparent'
                    }],
                    strideLineText: 4,
                    stride: 25,
                    length: 13,
                    fractionSize: 0,
                    colorFunction: Function
                },
                label: {
                    title: "Conditions d'éligibilité",
                    data: [],
                    strideLineText: 10,
                    stride: 25,
                    length: 0
                },
                customerValue: {
                    title: 'clientResponsibilityRate long',
                    data: [{
                        date: 1475280000000,
                        value: '25',
                        bgColor: 'fill-transparent'
                    }],
                    strideLineText: 4,
                    stride: 25,
                    length: 13,
                    fractionSize: 0,
                    colorFunction: Function
                },
                nbTotIncident: {
                    title: "totalIncidentNumber long",
                    data: [{
                        date: 1475280000000,
                        value: '5',
                        bgColor: 'fill-transparent'
                    }],
                    strideLineText: 4,
                    stride: 19,
                    length: 13,
                    colorFunction: Function
                }
            },
            dataHisto: {
                paiOrange: {
                    data: [{
                        x: 1475280000000,
                        y: 95,
                        //threshold: 75,
                        colorCircle: 'fill-brand-blue'
                    }],
                },
                paiCust: {
                    data: [{
                        x: 1475280000000,
                        y: 25,
                        colorCircle: 'fill-black'
                    }],
                },
                paiOrangeThreshold: {
                    data: [{
                            x: 1475280000000,
                            y: 75,
                            firstPoint: true
                        },
                        {
                            x: 1475280000000,
                            y: 75,
                            firstPoint: false
                        }
                    ],
                },
                paiCustThreshold: {
                    data: [{
                            x: 1475280000000,
                            y: 20,
                            firstPoint: true
                        },
                        {
                            x: 1475280000000,
                            y: 20,
                            firstPoint: false
                        }
                    ],
                }
            },
            NA: false,
            months: [1475280000000]
        };

        var res = histobusiness.formatDataPaiHistoComponent(indicator, seriesClass);

        expect(res.dataTable.value.title).toEqual(result.dataTable.value.title);
        expect(res.dataTable.value.data).toEqual(result.dataTable.value.data);
        expect(res.dataTable.label.title).toEqual(result.dataTable.label.title);
        expect(res.dataTable.clientResponsibilityRate.title).toEqual(result.dataTable.customerValue.title);
        expect(res.dataTable.totalIncidentNumber.title).toEqual(result.dataTable.nbTotIncident.title);
        expect(res.dataHisto.paiOrange.data).toEqual(result.dataHisto.paiOrange.data);
    });
    //************************

});