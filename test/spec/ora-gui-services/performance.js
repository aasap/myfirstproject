'use strict';

describe('Service: performanceNew', function() {
    // load the service's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // instantiate service
    var appConfig;
    var performanceNew;
    var subscription;
    var reports;
    var utils;

    beforeEach(angular.mock.inject(function(_performanceNew_, _appConfig_, _subscription_, _reports_, _utils_) {
        performanceNew = _performanceNew_;
        appConfig = _appConfig_;
        subscription = _subscription_;
        reports = _reports_;
        utils =_utils_;
        reports.selected = {
            id: "000000"
        }

        reports.report = {
            sections: [{
                name: "qualityOfService",
                indicators: [{
                    name: "performanceRoundTripDelay",
                    onPeriod: {
                        information: [{
                            href: "pathsPerformance"
                        }]
                    }
                }]
            }]
        };

        subscription.selectedSubscription = {
            cdRefcom: "XXXX0000"
        }
    }));

    it('getPerformanceTable with filter should works', inject(function($httpBackend) {
        var filterPath = 'engagement=false';
        appConfig['msccr'] = {
            "enabled": true,
            "apiUrl": "htps://qa.api-scc.reporting-analytics.equant.com",
            "redirectionUrl": "/ora-reporting-api-msccr",
            "basePath": "/api/v1"
        };
        var path = appConfig["msccr"].redirectionUrl + appConfig["msccr"].basePath + '/msccr/XXXX0000/reports/000000/pathsPerformance?' + filterPath + '&limit=10' + '&offset=11';
        utils.sccrApiDisabled = false;
        $httpBackend.expectGET('config/config.json').respond({});
       
        $httpBackend.expectGET(path).respond({});
        var href = performanceNew.getUrlPerformanceDetailTable();
        performanceNew.getPerformanceTable(href, 10, 11, filterPath);
        $httpBackend.expectGET(appConfig.urlApi + '/user').respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/subscriptions/subscription').respond({});
        $httpBackend.flush();
    }));

    it('getPerformanceTable with no filter should works', inject(function($httpBackend) {
        appConfig['msccr'] = {
            "enabled": true,
            "apiUrl": "htps://qa.api-scc.reporting-analytics.equant.com",
            "redirectionUrl": "/ora-reporting-api-msccr",
            "basePath": "/api/v1"
        };
        utils.sccrApiDisabled = false;
        var path = appConfig["msccr"].redirectionUrl + appConfig["msccr"].basePath + '/msccr/XXXX0000/reports/000000/pathsPerformance?';

        $httpBackend.expectGET('config/config.json').respond({});
       

        $httpBackend.expectGET(path).respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/user').respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/subscriptions/subscription').respond({});
        performanceNew.getPerformanceTable('pathsPerformance');
        $httpBackend.flush();
    }));

    it('getAllPerformanceTable should works', inject(function($httpBackend) {
        appConfig['msccr'] = {
            "enabled": true,
            "apiUrl": "htps://qa.api-scc.reporting-analytics.equant.com",
            "redirectionUrl": "/ora-reporting-api-msccr",
            "basePath": "/api/v1"
        };
        utils.sccrApiDisabled = false;
        var path = appConfig["msccr"].redirectionUrl + appConfig["msccr"].basePath + '/msccr/XXXX0000/reports/000000/pathsPerformance?';

        $httpBackend.expectGET('config/config.json').respond({});
       
        $httpBackend.expectGET(path).respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/user').respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(appConfig.urlApi + '/subscriptions/subscription').respond({});
        performanceNew.getAllPerformanceTable('pathsPerformance');
        $httpBackend.flush();
    }));

});