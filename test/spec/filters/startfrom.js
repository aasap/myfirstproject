'use strict';

describe('Filter: startFrom', function () {

    // load the filter's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // initialize a new instance of the filter before each test
    var startFrom;
    beforeEach(angular.mock.inject(function ($filter) {
        startFrom = $filter('startFrom');
    }));

    it('should return initial value', function () {
        var test = [1, 2, 3, 40];
        var res = startFrom(test, 0);
        expect(res).toEqual(test);
    });
    it('should return less value than initial', function () {
        var test = [1, 2, 3, 40];
        var test2 = [2, 3, 40];
        var res = startFrom(test, 1);
        expect(res).toEqual(test2);
    });
    it('should return []', function () {
        var test = null;
        var res = startFrom(test, 1);
        expect(res).toEqual([]);
    });
});