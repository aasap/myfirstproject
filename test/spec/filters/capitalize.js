'use strict';

describe('Filter: capitalize', function () {

  // load the filter's module
  beforeEach(angular.mock.module('ora-internal-gui'));

  // initialize a new instance of the filter before each test
  var capitalize;
  beforeEach(angular.mock.inject(function ($filter) {
    capitalize = $filter('capitalize');
  }));

  it('should return corresponding txt for the input', function () {
    var text = 'my lower string to capitalize';
    expect(capitalize(text)).toBe('My lower string to capitalize');
  });

  it('should return the saime text', function () {
    var text = 'String already capitalized';
    expect(capitalize(text)).toBe('String already capitalized');
  });

  it('should return corresponding txt for the input', function () {
    var text = 'MY UPER STRING';
    expect(capitalize(text)).toBe('My uper string');
  });

  it('should return corresponding txt for the input', function () {
    var text = ' start by space';
    expect(capitalize(text)).toBe('Start by space');
  });

  it('should return undefined ', function () {
    expect(capitalize()).toBe(undefined);
  });

  it('should return undefined ', function () {
    expect(capitalize(null)).toBe(undefined);
  });

});
