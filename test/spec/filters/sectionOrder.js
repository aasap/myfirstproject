'use strict';

describe('Filter: secionOrder', function () {

    // load the filter's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // initialize a new instance of the filter before each test
    var scope;
    var $filter;

    beforeEach(angular.mock.inject(function ( _$filter_, $rootScope, $httpBackend, _appConfig_) {

        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET('/ora-internal-provider-api/rest/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});

        scope = $rootScope.$new();
        $filter = _$filter_;

    }));

    afterEach(angular.mock.inject(function ($httpBackend) {
        $httpBackend.flush();
    }));

    it('should return undefined ', function () {
        expect(scope).toBeDefined();
        expect($filter).toBeDefined();
    });


    it('should be ordered ', function () {
        var data1 = [ { name: 'inventory' }, { name: 'incident' },{ name: 'qualityOfService' }, { name: 'usage' }];
        var data2 = [{ name: 'qualityOfService' }, { name: 'usage' }, { name: 'incident' }, { name: 'inventory' }];
        var res = $filter('sectionsOrder')(data2);
        expect(res).toEqual(data1);
    });

    it('should be ordered when one missing  ', function () {
        var data1 = [ { name: 'inventory' }, { name: 'incident' }, { name: 'usage' }];
        var data2 = [ { name: 'usage' }, { name: 'incident' }, { name: 'inventory' }];
        var res = $filter('sectionsOrder')(data2);
        expect(res).toEqual(data1);
    });

    it('Should be return an array of some values if some data values ​​are unknown', function () {
        var data1 = [ { name: 'inventory' }, { name: 'incident' }];
        var data2 = [{ name: 'test1' }, { name: 'test2' }, { name: 'incident' }, { name: 'inventory' }];
        var res = $filter('sectionsOrder')(data2);
        expect(res).toEqual(data1);
    });

    it('Should be return an empty array if all data values ​​are unknown', function () {
        var data1 = [];
        var data2 = [{ name: 'test1' }, { name: 'test2' }, { name: 'test3' }, { name: 'test4' }];
        var res = $filter('sectionsOrder')(data2);
        expect(res).toEqual(data1);
    });

    it('Should be return an empty array if data is empty ', function () {
        var data = [];
        var res = $filter('sectionsOrder')(data);
        expect(res).toEqual(data);
    });

});
