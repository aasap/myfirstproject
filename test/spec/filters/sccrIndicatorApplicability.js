'use strict';

describe('Filter: sccrIndicatorApplicability', function () {

    // load the filter's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // initialize a new instance of the filter before each test
    var sccrIndicatorApplicability;
    beforeEach(angular.mock.inject(function ($filter) {
        sccrIndicatorApplicability = $filter('sccrIndicatorApplicability');
    }));



    it('should return applicable', function () {
            var indicator = {
                "name": "indicatorName",
                "applicability": {
                    "applicable": true
                },
            };

            expect(sccrIndicatorApplicability(indicator)).toEqual(true);
        });
         it('should return applicable', function () {
            var indicator = {
                "name": "indicatorName",
                "applicability": {
                    "applicable": true
                },
            };

            expect(sccrIndicatorApplicability(indicator)).toEqual(true);
        });
        it('should return applicable', function () {
            var indicator = {
                "name": "indicatorName",
                "applicability": {
                    "applicable": false
                },
            };

            expect(sccrIndicatorApplicability(indicator)).toEqual(false);
        });
 });
