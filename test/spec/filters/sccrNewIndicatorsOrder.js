'use strict';

describe('Filter: sccrNewIndicatorsOrder', function () {

    // load the filter's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // initialize a new instance of the filter before each test
    var sccrNewIndicatorsOrder;
    beforeEach(angular.mock.inject(function ($filter) {
        sccrNewIndicatorsOrder = $filter('sccrNewIndicatorsOrder');
    }));


    it('Inventory order is correct  ', function () {
        var order = ['SCCROffersEvolution'];
        var section = { 
            "name": 'inventory',
            "indicators":[
                {"name":"SCCROffersEvolution"}
            ]
        }
        var expectedResult = { 
            "name": 'inventory',
            "indicators":[
                {"name":"SCCROffersEvolution"}
            ]
        }
        expect(sccrNewIndicatorsOrder(section,order)).toEqual(expectedResult);
    });
    it('Incident order is correct  ', function () {
        var order = ['incidentsWithServiceInterruptionNumber'];
        var section = { 
            "name": 'incident',
            "indicators":[
                {"name":"incidentsWithServiceInterruptionNumber"}
            ]
        }
        var expectedResult = { 
            "name": 'incident',
            "indicators":[
                {"name":"incidentsWithServiceInterruptionNumber"}
            ]
        }
        expect(sccrNewIndicatorsOrder(section,order)).toEqual(expectedResult);
    });

    it('usage order is correct  ', function () {
        var order = ['SCCRDataVolume','SCCRDataLoad'];
        var section = { 
            "name": 'usage',
            "indicators":[
                {"name":"SCCRDataVolume"},
                {"name":"SCCRDataLoad"}
            ]
        }
        var expectedResult = { 
            "name": 'usage',
            "indicators":[
                {"name":"SCCRDataVolume"},
                {"name":"SCCRDataLoad"}
            ]
        }
        expect(sccrNewIndicatorsOrder(section,order)).toEqual(expectedResult);
    });

    it('qualityOfService order is correct  ', function () {
        var order =  ['globalVpnInternetAvailabilityRate', 'globalEthernetAvailabilityRate', 'accessAvailabilityRate', 'GTTRFulfilment', 'proactivityRate','MSIFulfilment', 'performance'];
        var section = { 
            "name": 'qualityOfService',
            "indicators":[
                {"name":"globalVpnInternetAvailabilityRate"},
                {"name":"globalEthernetAvailabilityRate"},
                {"name":"accessAvailabilityRate"},
                {"name":"GTTRFulfilment"},
                {"name":"proactivityRate"},
                {"name":"MSIFulfilment"},
                {"name":"performance"}
            ]
        }
        var expectedResult = { 
            "name": 'qualityOfService',
            "indicators":[
                {"name":"globalVpnInternetAvailabilityRate"},
                {"name":"globalEthernetAvailabilityRate"},
                {"name":"accessAvailabilityRate"},
                {"name":"GTTRFulfilment"},
                {"name":"proactivityRate"},
                {"name":"MSIFulfilment"},
                {"name":"performance"}
            ]
        }
        expect(sccrNewIndicatorsOrder(section,order)).toEqual(expectedResult);
    });
    
});
