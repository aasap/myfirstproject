'use strict';

describe('Filter: periodDisplayFrequency', function () {

  // load the filter's module
  beforeEach(angular.mock.module('ora-internal-gui'));

  // initialize a new instance of the filter before each test
  var periodDisplayFrequency;
  var period = {};
  beforeEach(angular.mock.inject(function ($filter) {
    periodDisplayFrequency = $filter('periodDisplayFrequency');
  }));

  it('should return monthly frequency', function () {
    period.frequency = 'MEN'
    var res = 'Mensuelle';

    expect(periodDisplayFrequency(period)).toEqual(res);
  });

  it('should return Annual frequency', function () {
    period.frequency = 'ANN'
    var res = 'Annuelle';

    expect(periodDisplayFrequency(period)).toEqual(res);
  });

  it('should return undefined ', function () {
    period.frequency = 'BAD'
    var res = undefined;

    expect(periodDisplayFrequency(period)).toEqual(res);
  });

  it('should return undefined ', function () {
    var res = undefined;

    expect(periodDisplayFrequency()).toEqual(res);
  });


});
