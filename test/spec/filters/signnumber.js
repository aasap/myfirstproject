'use strict';

describe('Filter: signNumber', function() {

    // load the filter's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // initialize a new instance of the filter before each test
    var signNumber;
    beforeEach(angular.mock.inject(function($filter) {
        signNumber = $filter('signNumber');
    }));

    it('should return the input prefixed with "signNumber filter:"', function() {
        var text = '1';
        expect(signNumber(text)).toBe('+' + text);
        text = '-1';
        expect(signNumber(text)).toBe(text);
        text = '0';
        expect(signNumber(text)).toBe('+' + text);
    });

});