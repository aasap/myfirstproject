'use strict';

describe('Filter: applyClass', function () {

    // load the filter's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    // initialize a new instance of the filter before each test
    var applyClass;
    beforeEach(angular.mock.inject(function ($filter) {
        applyClass = $filter('applyClass');
    }));

    it('should apply class: stripped', function () {
        var classes = {
            'selected': function (row) {
                return row.isSelected;
            },
            'stripped': function (row) {
                return row.isStripped;
            }
        }, row = {
            isSelected: false,
            isStripped: true
        }, result = {
            'selected': false,
            'stripped': true
        };

        expect(applyClass(classes, row)).toEqual(result);
    });

    it('should apply class: selected', function () {
        var classes = {
            'selected': function (row) {
                return row.isSelected;
            },
            'stripped': function (row) {
                return row.isStripped;
            }
        }, row = {
            isSelected: true,
            isStripped: false
        }, result = {
            'selected': true,
            'stripped': false
        };

        expect(applyClass(classes, row)).toEqual(result);
    });

    it('should apply class: selected, stripped', function () {
        var classes = {
            'selected': function (row) {
                return row.isSelected;
            },
            'stripped': function (row) {
                return row.isStripped;
            }
        }, row = {
            isSelected: true,
            isStripped: true
        }, result = {
            'selected': true,
            'stripped': true
        };

        expect(applyClass(classes, row)).toEqual(result);
    });

});
