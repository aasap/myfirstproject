'use strict';

describe('Filter: utc', function () {

  // load the filter's module
  beforeEach(angular.mock.module('ora-internal-gui'));

  // initialize a new instance of the filter before each test
  var utc;
  beforeEach(angular.mock.inject(function ($filter) {
    utc = $filter('utc');
  }));

  it('should return the corresponding date', function () {
    var date = 1451952000000;
    var res = new Date('01/05/16 00:00:00');

    expect(utc(date)).toEqual(res);
  });

  it('should return undefined', function () {
    var date = 'BAD';
    var res = undefined;

    expect(utc(date)).toEqual(res);
  });

});
