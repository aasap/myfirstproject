'use strict';

describe('Directive: Incident', function() {

    var incident;

    var $scope;

    var  indicator = {
        "name": "incidentsWithServiceInterruptionNumber",
        "shortLabel": "Incidents service interrompu",
        "longLabel": "Nombre d'incidents service interrompu",
        "unit": "",
        "contractual": false,
        "applicability": {
            "applicable": true
        },
        "metricsDefinitions": [{
                "name": "OrangeResponsibility",
                "shortLabel": "Orange",
                "longLabel": "responsabilité Orange",
                "unit": "",
                "applicabilityCriteria": false
            },
            {
                "name": "CustomerResponsibility",
                "shortLabel": "Client",
                "longLabel": "responsabilité client",
                "unit": "",
                "applicabilityCriteria": false
            },
            {
                "name": "ThirdPartyResponsibility",
                "shortLabel": "Tiers",
                "longLabel": "Responsabilité tiers",
                "unit": "",
                "applicabilityCriteria": false
            },
            {
                "name": "OtherResponsibility",
                "shortLabel": "Autres",
                "longLabel": "Autres",
                "unit": "",
                "applicabilityCriteria": true
            }
        ],
        "dimensionProvided": "no",
        "onPeriod": {
            "measure": 13.8,
            "evolution": 10.9,
            "evolutionRate": 15.6,
            "applicability": {
                "applicable": true
            },
            "metrics": [{
                    "name": "OrangeResponsibility",
                    "measure": 5,
                    "evolution": 149.0,
                    "evolutionRate": 98.8

                },
                {
                    "name": "CustomerResponsibility",
                    "measure": 1,
                    "evolution": 3.0,
                    "evolutionRate": 97.7

                },
                {
                    "name": "ThirdPartyResponsibility",
                    "measure": 2,
                    "evolution": 2.0,
                    "evolutionRate": 85.5
                },
                {
                    "name": "OtherResponsibility",
                    "measure": 1,
                    "evolution": 2.0,
                    "evolutionRate": 79.9,
                    "applicability": {
                        "applicable": true
                    }
                }
            ]
        },
        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 1,
                        "evolution": 8.1,
                        "evolutionRate": 8.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 11,
                        "evolution": 8.3,
                        "evolutionRate": 8.3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 12,
                        "evolution": 8.4,
                        "evolutionRate": 8.4,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 9,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 9.3,
                        "evolutionRate": 9.3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 9.4,
                        "evolutionRate": 9.4,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 10,
                        "evolution": 10.1,
                        "evolutionRate": 10.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 10.2,
                        "evolutionRate": 10.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 10.3,
                        "evolutionRate": 10.3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 10.4,
                        "evolutionRate": 10.4,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 10,
                        "evolution": 11.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 11.2,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 11.3,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 11.4,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 92.0,
                "contractualThreshold": 52.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 12,
                        "evolution": 12.1,
                        "evolutionRate": 12.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 12.2,
                        "evolutionRate": 12.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 12.3,
                        "evolutionRate": 12.3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 12.4,
                        "evolutionRate": 12.4,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": 98.0,
                "contractualThreshold": 55.6,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 7,
                        "evolution": 1.1,
                        "evolutionRate": 7.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 7,
                        "evolution": 1.2,
                        "evolutionRate": 7.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 6,
                        "evolution": 1.3,
                        "evolutionRate": 6.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 1.4,
                        "evolutionRate": 6.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 97.0,
                "contractualThreshold": 17.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 10,
                        "evolution": 2.1,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 5,
                        "evolution": 2.2,
                        "evolutionRate": 5.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 4,
                        "evolution": 2.3,
                        "evolutionRate": 4.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 2.4,
                        "evolutionRate": 10.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 99.0,
                "contractualThreshold": 78.8,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 10,
                        "evolution": 3.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 3.2,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 3.3,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 3.4,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "measure": 85.0,
                "contractualThreshold": 42.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 8,
                        "evolution": 4.1,
                        "evolutionRate": 4.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 4.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 4.3,
                        "evolutionRate": 10.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 4.4,
                        "evolutionRate": 12.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 97.0,
                "contractualThreshold": 37.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 6,
                        "evolution": 5.1,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 5.2,
                        "evolutionRate": 6.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 10,
                        "evolution": 5.3,
                        "evolutionRate": 5.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 5.4,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 69.0,
                "contractualThreshold": 13.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 10,
                        "evolution": 6.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 0,
                        "evolution": 6.2,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 6.3,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 15,
                        "evolution": 6.4,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 87.0,
                "contractualThreshold": 67.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 4,
                        "evolution": 7.1,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 9,
                        "evolution": 7.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 10,
                        "evolution": 7.3,
                        "evolutionRate": 7.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 11,
                        "evolution": 7.4,
                        "evolutionRate": 4.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": 97.0,
                "contractualThreshold": 37.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "OrangeResponsibility",
                        "measure": 10,
                        "evolution": 8.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "CustomerResponsibility",
                        "measure": 3,
                        "evolution": 8.2,
                        "evolutionRate": 7.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "ThirdPartyResponsibility",
                        "measure": 0,
                        "evolution": 8.3,
                        "evolutionRate": 6.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "OtherResponsibility",
                        "measure": 18,
                        "evolution": 8.4,
                        "evolutionRate": 5.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            }
        ]
    };
  //   load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        incident = $compile("<ora-incidents-indicator indicator='indicator' class='col-md-12'></ora-incidents-indicator>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('incident should be defined', inject(function() {
        expect(incident).toBeDefined();
    }));

    it('indicatorGeneralInfo should be defined', inject(function() {
        var indicatorGeneralInfo = {
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "unit": indicator.unit,
            "contractual": indicator.contractual
        };
        expect(incident.isolateScope().indicatorGeneralInfo).toBeDefined();
        expect(incident.isolateScope().indicatorGeneralInfo).toEqual(indicatorGeneralInfo);
            
    }));

    it('historicOptions should be defined', inject(function() {
        var historicOptions = {
            isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-yellow',
                seriesClass: [{
                    name: 'OrangeResponsibility',
                    bgClass: 'bg-orange',
                    fill: 'fill-orange',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'CustomerResponsibility',
                    bgClass: 'bg-dark',
                    fill: 'fill-black',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'ThirdPartyResponsibility',
                    bgClass: 'bg-incident-tiers-gray',
                    fill: 'fill-incident_tiers',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'OtherResponsibility',
                    bgClass: 'bg-incident-others-gray',
                    fill: 'fill-incident_others',
                    typeHisto: 'bar',
                    unit: ''
                }
                ]
        };
        expect(incident.isolateScope().historicOptions).toBeDefined();
        expect(incident.isolateScope().historicOptions).toEqual(historicOptions);
    })); 

    it('historicId should be defined', inject(function(){
        var historicId = 'mSccrHistoINCIDENT';
        expect(incident.isolateScope().historicId).toBeDefined();
        expect(incident.isolateScope().historicId).toEqual(historicId);
    }));

    it('configurations should be defined', inject(function(){
        var configurations = [ {
            'name': 'OrangeResponsibility'
        }, {
            'name': 'CustomerResponsibility'
        }, {
            'name': 'ThirdPartyResponsibility'
        }, {
            'name': 'OtherResponsibility'
        }];
        expect(incident.isolateScope().configurations).toBeDefined();
        for(var item = 0; item<4 ; item++){
            expect(incident.isolateScope().configurations[item].name).toEqual(configurations[item].name);
        }
    }));
});