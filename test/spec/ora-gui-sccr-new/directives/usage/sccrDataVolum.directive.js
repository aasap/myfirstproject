'use strict';

describe('Directive: sccrDataVolum internet', function() {

    var volumElement;

    var $scope;
    
    var indicator = {
        "name": "SCCRDataVolume",
        "shortLabel": "Volumétrie",
        "longLabel": "Volumétrie totale",
        "unit": "%",
        "contractual": true,
        "applicability": {
            "applicable": true
        },
        "metricsDefinitions": [{
                "name": "outVolume",
                "shortLabel": "Emission short",
                "longLabel": "Emission long",
                "unit": "%",
                "applicabilityCriteria": true,
                "category": {
                    "name": "",
                    "label": ""
                }
            },
            {
                "name": "inVolume",
                "shortLabel": "Reception short",
                "longLabel": "Reception long",
                "unit": "%",
                "applicabilityCriteria": true,
                "category": {
                    "name": "",
                    "label": ""
                }
            }
        ],
        "dimensionProvided": "yes",
        "onPeriod": {
            "measure": 13.8,
            "evolution": 10.9,
            "evolutionRate": 15.6,
            "contractualThreshold": 87.4,
            "applicability": {
                "applicable": true
            },
            "contractualCommitmentFulfilled": true,
            "metrics": [{
                    "name": "outVolume",
                    "measure": 132464742392936,
                    "evolution": 149.0,
                    "evolutionRate": 98.8,
                    "applicability": {
                        "applicable": true
                    }
                },
                {
                    "name": "inVolume",
                    "measure": 133718326241096,
                    "evolution": 0,
                    "evolutionRate": 97.7,
                    "applicability": {
                        "applicable": true
                    }
                }
            ],
            "information": [{
                "label": "Top 5 des accès les plus chargés",
                "href": "",
                "rows": [{
                        "type": "title",
                        "cells": [
                            {
                                "key": "siteName",
                                "value": "Site"
                            },
                            {
                                "key": "accessName",
                                "value": "Accès"
                            },
                            {
                                "key": "overrunNumber",
                                "value": "Nombre de dépassements"
                            },
                            {
                                "key": "averageDuration",
                                "value": "Durée moyenne"
                            },
                            {
                                "key": "maximumLoadRate",
                                "value": "Taux de charge maximum"
                            },
                            {
                                "key": "way",
                                "value": "Sens"
                            },
                            {
                                "key": "duration",
                                "value": "durée"
                            }
                        ]
                    },
                    {
                        "type": "content",
                        "cells": [{
                                "key": "siteName",
                                "value": "103569875"
                            },
                            {
                                "key": "accessName",
                                "value": "10000000"
                            },
                            {
                                "key": "overrunNumber",
                                "value": "324"
                            },
                            {
                                "key": "averageDuration",
                                "value": "162677"
                            },
                            {
                                "key": "maximumLoadRate",
                                "value": "330."
                            },
                            {
                                "key": "way",
                                "value": "Emission"
                            },
                            {
                                "key": "duration",
                                "value": "05"
                            }
                        ]
                    },
                    {
                        "type": "content",
                        "cells": [{
                                "key": "siteName",
                                "value": "LAURAS"
                            },
                            {
                                "key": "accessName",
                                "value": "roquefort-ADSL"
                            },
                            {
                                "key": "overrunNumber",
                                "value": "14"
                            },
                            {
                                "key": "averageDuration",
                                "value": "1627"
                            },
                            {
                                "key": "maximumLoadRate",
                                "value": "105.34"
                            },
                            {
                                "key": "way",
                                "value": "Reception"
                            },
                            {
                                "key": "duration",
                                "value": "04"
                            }
                        ]
                    },
                    {
                        "type": "content",
                        "cells": [{
                                "key": "siteName",
                                "value": "LOUVILLE"
                            },
                            {
                                "key": "accessName",
                                "value": "louville-n-RPOA0"
                            },
                            {
                                "key": "overrunNumber",
                                "value": "156"
                            },
                            {
                                "key": "averageDuration",
                                "value": "1628"
                            },
                            {
                                "key": "maximumLoadRate",
                                "value": "90.00"
                            },
                            {
                                "key": "way",
                                "value": "Emission"
                            },
                            {
                                "key": "duration",
                                "value": "03"
                            }
                        ]
                    },
                    {
                        "type": "content",
                        "cells": [{
                                "key": "siteName",
                                "value": "ALBI FAUCH"
                            },
                            {
                                "key": "accessName",
                                "value": "albi-n-EFM0"
                            },
                            {
                                "key": "overrunNumber",
                                "value": "213"
                            },
                            {
                                "key": "averageDuration",
                                "value": "1530"
                            },
                            {
                                "key": "maximumLoadRate",
                                "value": "108000.15"
                            },
                            {
                                "key": "way",
                                "value": "Emission"
                            },
                            {
                                "key": "duration",
                                "value": "02"
                            }
                        ]
                    },
                    {
                        "type": "content",
                        "cells": [{
                                "key": "siteName",
                                "value": "RAUDIN"
                            },
                            {
                                "key": "accessName",
                                "value": "raudin-n-RPOAD"
                            },
                            {
                                "key": "overrunNumber",
                                "value": "623"
                            },
                            {
                                "key": "averageDuration",
                                "value": "1533"
                            },
                            {
                                "key": "maximumLoadRate",
                                "value": "102.31"
                            },
                            {
                                "key": "way",
                                "value": "Emission"
                            },
                            {
                                "key": "duration",
                                "value": "01"
                            }
                        ]
                    }
                ]
            }]
        },
        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589637,
                        "evolution": 8.1,
                        "evolutionRate": 8.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589639,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589637,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589636,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589638,
                        "evolution": 10.1,
                        "evolutionRate": 10.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589634,
                        "evolution": 10.2,
                        "evolutionRate": 10.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1596377,
                        "evolution": 11.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589633,
                        "evolution": 11.2,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 92.0,
                "contractualThreshold": 52.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589637,
                        "evolution": 12.1,
                        "evolutionRate": 12.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589537,
                        "evolution": 12.2,
                        "evolutionRate": 12.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": 98.0,
                "contractualThreshold": 55.6,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589635,
                        "evolution": 1.1,
                        "evolutionRate": 7.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589639,
                        "evolution": 1.2,
                        "evolutionRate": 7.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 97.0,
                "contractualThreshold": 17.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1689637,
                        "evolution": 2.1,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589637,
                        "evolution": 2.2,
                        "evolutionRate": 5.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 99.0,
                "contractualThreshold": 78.8,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589737,
                        "evolution": 3.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589537,
                        "evolution": 3.2,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "measure": 85.0,
                "contractualThreshold": 42.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589637,
                        "evolution": 4.1,
                        "evolutionRate": 4.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589633,
                        "evolution": 4.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 97.0,
                "contractualThreshold": 37.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589637,
                        "evolution": 5.1,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589638,
                        "evolution": 5.2,
                        "evolutionRate": 6.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 1589644,
                "contractualThreshold": 13.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589644,
                        "evolution": 6.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589667,
                        "evolution": 6.2,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 87.0,
                "contractualThreshold": 67.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1489637,
                        "evolution": 7.1,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589637,
                        "evolution": 7.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": 97.0,
                "contractualThreshold": 37.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "outVolume",
                        "measure": 1589639,
                        "evolution": 8.1,
                        "evolutionRate": 3.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "inVolume",
                        "measure": 1589637,
                        "evolution": 8.2,
                        "evolutionRate": 7.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            }
        ],
        "dimension": {
            "name": "Dimension identifier!!!!",
            "dimensionLabel": "Accès de référence",
            "dimensionIndicatorLabel": "Volumétrie accès de référence",
            "values": [{
                    "dimensionValue": "RODEZ RUE E-SINGLA -rodez-n-GE1/0",
                    "dimensionGroup": "0236987410",
                    "applicability": {
                        "applicable": true,
                        "inapplicabilityReason": "Non applicable sur cette période"
                    },
                    "onPeriod": {
                        "measure": 10.0,
                        "evolution": 12.0,
                        "evolutionRate": 14.0,
                        "contractualThreshold": 84.5,
                        "applicability": {
                            "applicable": true
                        },
                        "contractualCommitmentFulfilled": true,
                        "metrics": {
                            "name": "metric",
                            "measure": 10.0,
                            "evolution": 12.0,
                            "evolutionRate": 14.0,
                            "contractualThreshold": 84.5,
                            "applicability": {
                                "applicable": true
                            }
                        }
                    },
                    "monthlyHistory": [{
                            "month": "2016-08",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 18.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 1,
                                    "evolution": 8.1,
                                    "evolutionRate": 8.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 10,
                                    "evolution": 8.2,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-09",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 18.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 9,
                                    "evolution": 9.1,
                                    "evolutionRate": 9.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 9.2,
                                    "evolutionRate": 9.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-10",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 18.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 10.1,
                                    "evolutionRate": 10.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 10.2,
                                    "evolutionRate": 10.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-11",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 18.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 11.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 11.2,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-12",
                            "onPeriod": false,
                            "measure": 92.0,
                            "contractualThreshold": 52.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 12,
                                    "evolution": 12.1,
                                    "evolutionRate": 12.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 12.2,
                                    "evolutionRate": 12.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-01",
                            "onPeriod": false,
                            "measure": 98.0,
                            "contractualThreshold": 55.6,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 7,
                                    "evolution": 1.1,
                                    "evolutionRate": 7.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 7,
                                    "evolution": 1.2,
                                    "evolutionRate": 7.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-02",
                            "onPeriod": false,
                            "measure": 97.0,
                            "contractualThreshold": 17.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 2.1,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 5,
                                    "evolution": 2.2,
                                    "evolutionRate": 5.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-03",
                            "onPeriod": false,
                            "measure": 99.0,
                            "contractualThreshold": 78.8,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 3.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 3.2,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-04",
                            "onPeriod": false,
                            "measure": 85.0,
                            "contractualThreshold": 42.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 8,
                                    "evolution": 4.1,
                                    "evolutionRate": 4.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 4.2,
                                    "evolutionRate": 9.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-05",
                            "onPeriod": false,
                            "measure": 97.0,
                            "contractualThreshold": 37.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 6,
                                    "evolution": 5.1,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 5.2,
                                    "evolutionRate": 6.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-06",
                            "onPeriod": true,
                            "measure": 69.0,
                            "contractualThreshold": 13.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 6.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 6.2,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-07",
                            "onPeriod": true,
                            "measure": 87.0,
                            "contractualThreshold": 67.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 4,
                                    "evolution": 7.1,
                                    "evolutionRate": 9.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 9,
                                    "evolution": 7.2,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-08",
                            "onPeriod": true,
                            "measure": 97.0,
                            "contractualThreshold": 37.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 8.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 3,
                                    "evolution": 8.2,
                                    "evolutionRate": 7.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    "dimensionValue": "RODEZ RUE E-SINGLA -rodez-n-GE1/1",
                    "dimensionGroup": "02369874101",
                    "applicability": {
                        "applicable": false,
                        "inapplicabilityReason": "Non applicable sur cette période"
                    },
                    "onPeriod": {
                        "measure": 10.0,
                        "evolution": 12.0,
                        "evolutionRate": 14.0,
                        "contractualThreshold": 84.5,
                        "applicability": {
                            "applicable": true
                        },
                        "contractualCommitmentFulfilled": true,
                        "metrics": {
                            "name": "metric",
                            "measure": 10.0,
                            "evolution": 12.0,
                            "evolutionRate": 14.0,
                            "contractualThreshold": 84.5,
                            "applicability": {
                                "applicable": true
                            }
                        }
                    },
                    "monthlyHistory": [{
                            "month": "2016-08",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 8.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 9999,
                                    "evolution": 8.1,
                                    "evolutionRate": 8.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 1001,
                                    "evolution": 8.2,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-09",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 18.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 9,
                                    "evolution": 9.1,
                                    "evolutionRate": 9.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 9.2,
                                    "evolutionRate": 9.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-10",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 18.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 10.1,
                                    "evolutionRate": 10.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 10.2,
                                    "evolutionRate": 10.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-11",
                            "onPeriod": false,
                            "measure": 6.0,
                            "contractualThreshold": 18.9,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 11.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 11.2,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2016-12",
                            "onPeriod": false,
                            "measure": 92.0,
                            "contractualThreshold": 52.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 12,
                                    "evolution": 12.1,
                                    "evolutionRate": 12.1,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 12.2,
                                    "evolutionRate": 12.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-01",
                            "onPeriod": false,
                            "measure": 98.0,
                            "contractualThreshold": 55.6,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 7,
                                    "evolution": 1.1,
                                    "evolutionRate": 7.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 7,
                                    "evolution": 1.2,
                                    "evolutionRate": 7.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-02",
                            "onPeriod": false,
                            "measure": 97.0,
                            "contractualThreshold": 17.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 2.1,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 5,
                                    "evolution": 2.2,
                                    "evolutionRate": 5.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-03",
                            "onPeriod": false,
                            "measure": 99.0,
                            "contractualThreshold": 78.8,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 3.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 3.2,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-04",
                            "onPeriod": false,
                            "measure": 85.0,
                            "contractualThreshold": 42.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 8,
                                    "evolution": 4.1,
                                    "evolutionRate": 4.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 4.2,
                                    "evolutionRate": 9.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-05",
                            "onPeriod": false,
                            "measure": 97.0,
                            "contractualThreshold": 37.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 6,
                                    "evolution": 5.1,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 5.2,
                                    "evolutionRate": 6.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-06",
                            "onPeriod": true,
                            "measure": 69.0,
                            "contractualThreshold": 13.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 6.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 0,
                                    "evolution": 6.2,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-07",
                            "onPeriod": true,
                            "measure": 87.0,
                            "contractualThreshold": 67.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 4,
                                    "evolution": 7.1,
                                    "evolutionRate": 9.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 9,
                                    "evolution": 7.2,
                                    "evolutionRate": 8.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        },
                        {
                            "month": "2017-08",
                            "onPeriod": true,
                            "measure": 97.0,
                            "contractualThreshold": 37.0,
                            "applicability": {
                                "applicable": true
                            },
                            "contractualCommitmentFulfilled": false,
                            "metrics": [{
                                    "name": "outVolume",
                                    "measure": 10,
                                    "evolution": 8.1,
                                    "evolutionRate": 3.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                },
                                {
                                    "name": "inVolume",
                                    "measure": 3,
                                    "evolution": 8.2,
                                    "evolutionRate": 7.2,
                                    "applicability": {
                                        "applicable": true
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    }
  //   load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {

        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
  
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        volumElement = $compile("<ora-sccr-data-volume indicator='indicator' class='col-md-12'></ora-sccr-data-volume>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('volumElement should be defined', inject(function() {
        expect(volumElement).toBeDefined();
    }));


    it('configurations should be defined', inject(function() {
        var configurations = [{
                    "name": "outVolume",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
                {
                    "name": "inVolume",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
            ];
        expect(volumElement.isolateScope().configurations).toBeDefined();
        expect(volumElement.isolateScope().filtredDetailsTabConf).toBeDefined();
        for(var item = 0 ; item < 2 ;item++){
            expect(volumElement.isolateScope().configurations[item].name).toEqual(configurations[item].name);
            expect(volumElement.isolateScope().configurations[item].typeOfData).toEqual(configurations[item].typeOfData);
            expect(volumElement.isolateScope().configurations[item].isColor).toEqual(configurations[item].isColor);
            expect(volumElement.isolateScope().configurations[item].nonApplicableValue).toEqual(configurations[item].nonApplicableValue);
            expect(volumElement.isolateScope().configurations[item].showInapplicabilityReason).toEqual(configurations[item].showInapplicabilityReason);
        }
    }));
    
    it('historicOptions should be defined', inject(function() {
        var historicOptions = {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-purple',
                seriesClass: [{
                        name: 'outVolume',
                        stroke: 'stroke-brand-dark-blue',
                        typeHisto: 'line'
                    },
                    {
                        name: 'inVolume',
                        stroke: 'stroke-brand-dark-yellow',
                        typeHisto: 'line'
                    }
                ]
            };

        expect(volumElement.isolateScope().historicOptions).toBeDefined();
        expect(volumElement.isolateScope().historicOptions).toEqual(historicOptions);
    }));


    it('indicatorInfo should be defined', inject(function() {
        var indicatorInfo = {
                "name": indicator.name,
                "shortLabel": indicator.shortLabel,
                "longLabel": indicator.longLabel,
                "contractual": indicator.contractual,
                "unit": indicator.unit,
                "tobeConverted": true,
                "conversionFilterName": 'unitConverter',
                "conversionFilterAddidionalArg": true
            };
        expect(volumElement.isolateScope().indicatorInfo).toBeDefined();
        expect(volumElement.isolateScope().indicatorInfo).toEqual(indicatorInfo);
    }));
    it('historicId should be defined', inject(function() {
        var historicId = 'sccrNewUsageVolumHisto';
        expect(volumElement.isolateScope().historicId).toBeDefined();
        expect(volumElement.isolateScope().historicId).toEqual(historicId);
    }));

    it('usageStyles should be defined', inject(function() {
        var usageStyles = 'usage-table-style';
        expect(volumElement.isolateScope().usageStyles).toBeDefined();
        expect(volumElement.isolateScope().usageStyles).toEqual(usageStyles);
    }));

    it('New top chage table', inject(function( $compile, usagedata, $q) {
        spyOn(usagedata, 'getTopChargeData').and.callFake(function(indicator) {
            return $q.when({
                data: []
            });
        });
        indicator.onPeriod.information[0].href = '/topcharge'
        let element = $compile("<ora-sccr-data-volume indicator='indicator' class='col-md-12'></ora-sccr-data-volume>")($scope);
        $scope.$digest(); 
        expect(volumElement.isolateScope().filtredDetailsTabConf).toBeDefined();
        expect(volumElement.isolateScope().filtredDetailsTabConf.filters).toBeDefined();
        expect(volumElement.isolateScope().filtredDetailsTabConf.fields).toBeDefined();
        expect(volumElement.isolateScope().filtredDetailsTabConf.headers).toBeDefined();
        expect(element.html().includes('ora-generic-details-table-filtred')).toBeTruthy();
        expect(element.html().includes('ora-generic-detailed-record-table')).toBeFalsy();
    }));



});