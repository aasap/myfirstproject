'use strict';

describe('Directive: options ', function() {

    var options;

    var $scope;
    
    var indicator = {
        "name": "SCCROptionsEvolution",
        "shortLabel": "Options",
        "longLabel": "Options",
        "unit": "",
        "contractual": false,
        "applicability": {
            "applicable": true
        },
        "metricsDefinitions": [{
                "name": "optimization",
                "shortLabel": "Optimisation",
                "longLabel": "Optimisation",
                "unit": ""
            },
            {
                "name": "proactivity",
                "shortLabel": "Proactivité",
                "longLabel": "Proactivité",
                "unit": ""
            },
            {
                "name": "availability",
                "shortLabel": "Disponibilité",
                "longLabel": "Disponibilité",
                "unit": ""
            },
            {
                "name": "internetExpertise",
                "shortLabel": "Expertise Internet",
                "longLabel": "Expertise Internet",
                "unit": ""
            },
            {
                "name": "performance",
                "shortLabel": "Performance",
                "longLabel": "Performance",
                "unit": ""
            },
            {
                "name": "applicativePerformanceEAMRiverbed",
                "shortLabel": "Performance Applicative EAM Riverbed",
                "longLabel": "Performance Applicative EAM Riverbed",
                "unit": ""
            },
            {
                "name": "applicativePerformanceNetworkBoost",
                "shortLabel": "Performance Applicative Network Boost",
                "longLabel": "Performance Applicative Network Boost",
                "unit": ""
            },
            {
                "name": "personalizedSupervision",
                "shortLabel": "Supervision Personnalisée",
                "longLabel": "Supervision Personnalisée",
                "unit": ""
            },
            {
                "name": "digitalMonitoring",
                "shortLabel": "Vision Equipement",
                "longLabel": "Vision Equipement",
                "unit": ""
            }
        ],
        "onPeriod": {
            "applicability": {
                "applicable": true
            },
            "metrics": [{
                    "name": "optimization",
                    "evolution": 76.0
                },
                {
                    "name": "proactivity",
                    "measure": 0,
                    "evolution": 76
                },
                {
                    "name": "availability",
                    "measure": 3959.0,
                    "evolution": 209.0
                },
                {
                    "name": "internetExpertise",
                    "measure": 3959.0,
                    "evolution": 209.0
                },
                {
                    "name": "performance",
                    "measure": 33,
                    "evolution": 0
                },
                {
                    "name": "applicativePerformanceEAMRiverbed",
                    "measure": 0,
                    "evolution": 0
                },
                {
                    "name": "applicativePerformanceNetworkBoost",
                    "measure": 0,
                    "evolution": 22
                },
                {
                    "name": "personalizedSupervision",
                    "measure": 0,
                    "evolution": 0
                },
                {
                    "name": "digitalMonitoring",
                    "measure": 22,
                    "evolution": 0
                }
            ]
        }
    };
  //   load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        options = $compile(" <ora-parc-options  indicator ='indicator' class='col-md-12'></ora-parc-options>")($scope);

        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('options should be defined', inject(function() {
        expect(options).toBeDefined();
    }));

    it('subscribedMetrics must be defined', function(){
        var subscribedMetrics = ["applicativePerformanceEAMRiverbed", "applicativePerformanceNetworkBoost", "personalizedSupervision", "digitalMonitoring"];;
        expect(options.isolateScope().subscribedMetrics).toBeDefined();
        expect(options.isolateScope().subscribedMetrics).toEqual(subscribedMetrics);
    })
});