'use strict';

describe('Directive: GTR', function() {

    var gtrElement;

    var $scope;

    var indicator = {
        "name": "GTTRFulfilment",
        "shortLabel": "GTR",
        "longLabel": "Respect de la GTR",
        "contractual": true,
        "unit": "%",
        "metricsDefinitions": [{
                "name": "incidentsRespectingGTTR",
                "shortLabel": "GTR respectée",
                "longLabel": "Nbr incidents GTR respectée",
                "unit": ""
            },
            {
                "name": "incidentsExceedingGTTR",
                "shortLabel": "GTR excédée",
                "longLabel": "Nbr incidents GTR excédée",
                "unit": ""
            }
        ],
        "onPeriod": {
            "measure": 95.33,
            "evolution": 0,
            "unit": "",
            "contractualThreshold": 100,
            "applicability": {
                "applicable": true
            },
            "contractualCommitmentFulfilled": false,
            "metrics": [
                {
                    "name": "incidentsRespectingGTTR",
                    "measure": 62,
                    "evolutionRate": -10
                },
                {
                    "name": "incidentsExceedingGTTR",
                    "measure": 4,
                    "evolutionRate": -50
                }
            ],
            "information" : [{
                "href" : 'gtr'
            }]
        },
        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "measure": 94.74,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 18
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 1
                    }
                ]
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "measure": 92.86,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 26
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 2
                    }
                ]
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 90.91,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 30
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 3
                    }
                ]
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 85.71,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 18
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 3
                    }
                ]
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 13
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 0
                    }
                ]
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": 91.67,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 22
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 2
                    }
                ]
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 80.95,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 17
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 4
                    }
                ]
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 85.71,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 12
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 2
                    }
                ]
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "measure": 88.89,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 8
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 1
                    }
                ]
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 96.77,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 30
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 1
                    }
                ]
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 93.75,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 30
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 2
                    }
                ]
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 10
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 1
                    }
                ]
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": 96.55,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "incidentsRespectingGTTR",
                        "measure": 28
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "measure": 1
                    }
                ]
            }
        ]
    };

    // load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function( $httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function($compile, _appConfig_) {
        gtrElement = $compile("<ora-qos-gttr indicator='indicator' class='col-md-12'></ora-qos-gttr>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('gtrElement should be defined', inject(function() {
        expect(gtrElement).toBeDefined();
    }));

    it('historicId should be defined', inject(function() {
        var historicId = 'msccrHistoGTTR';
        expect(gtrElement.isolateScope().historicId).toBeDefined();
        expect(gtrElement.isolateScope().historicId).toEqual(historicId);
    }));

    it('configurations should be defined', inject(function() {
        var configurations =  [{
            "name": "GTTRFulfilment",
            "typeOfData": 'Indicator',
            "isColor": false,
            "nonApplicableValue": '-',
            "isTranslated": true,
            "isHeaderVisible":true
        },
        {
            "name": "incidentsExceedingGTTR",
            "typeOfData": 'Metrics',
            "isColor": false,
            "nonApplicableValue": '-',
            "isHeaderVisible":true
        },
        {
            "name": "incidentsRespectingGTTR",
            "typeOfData": 'Metrics',
            "isColor": false,
            "nonApplicableValue": '-',
            "isHeaderVisible":true

        }
    ];
        expect(gtrElement.isolateScope().configurations).toBeDefined();
        expect(gtrElement.isolateScope().configurations).toEqual(configurations);
    }));

    it('metricsDefinitions should be defined', inject(function() {
        expect(gtrElement.isolateScope().metricsDefinitions).toBeDefined();
        expect(gtrElement.isolateScope().metricsDefinitions).toEqual(indicator.metricsDefinitions);
    }));

    it('monthlyHistory should be defined', inject(function() {
        expect(gtrElement.isolateScope().monthlyHistory).toBeDefined();
        expect(gtrElement.isolateScope().monthlyHistory).toEqual(indicator.monthlyHistory);
    }));

    it('historicOptions should be defined', inject(function() {
        var historicOptions = {
            isStartEnable: true,
            asLegend: true,
            annualSpecialDisplay: false,
            highlightClass: 'fill-brand-light-blue',
            seriesClass: [{
                    name: 'incidentsRespectingGTTR',
                    bgClass: 'bg-green',
                    fill: 'fill-green',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'incidentsExceedingGTTR',
                    bgClass: 'bg-red',
                    fill: 'fill-red',
                    typeHisto: 'bar',
                    unit: ''
                },
            ]
        };
        expect(gtrElement.isolateScope().historicOptions).toBeDefined();
        expect(gtrElement.isolateScope().historicOptions).toEqual(historicOptions);
    }));

    it('indicatorGeneralInfo should be defined', inject(function() {
        var indicatorGeneralInfo =  {
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "contractual": indicator.contractual,
            "unit": indicator.unit,
            "tobeConverted": false,
            "conversionFilterName": ''
        };
        expect(gtrElement.isolateScope().indicatorGeneralInfo).toBeDefined();
        expect(gtrElement.isolateScope().indicatorGeneralInfo).toEqual(indicatorGeneralInfo);
    }));

    it('gtr details table directive should be integrated', inject(function() {
        expect(gtrElement.isolateScope().type).toBeDefined();
        expect(gtrElement.isolateScope().type).toEqual('msccr'); 
        expect(gtrElement.html().includes('indicator.onPeriod.information && indicator.onPeriod.information[0] && indicator.onPeriod.information[0].href')).toBeTruthy();
        expect(gtrElement.html().includes('<gtr-table report-type="type"')).toBeTruthy();
        expect(gtrElement.html().includes('</gtr-table>')).toBeTruthy();
    }));

});
