'use strict';

describe('Directive: VPN internet', function() {

    var vpnElement;

    var $scope;
    
    var indicator ={
        "name": "globalVpnInternetAvailabilityRate",
        "shortLabel": "Disponibilité",
        "longLabel": "Disponibilité globale VPN + Internet",
        "contractual": true,
        "unit": "%",
        "applicability": {
            "applicable": true,
            "inapplicabilityCode": "notSubscribed",
            "inapplicabilityReason": "Inapplicability reason"
        },
        "onPeriod": {
            "available": true,
            "measure": 100,
            "evolution": 0,
            "unit": "",
            "contractualThreshold": 99.2,
            "applicability": {
                "applicable": false
            },
            "contractualCommitmentFulfilled": true
        },
        "annualHistory": [{
                "year": "2016-01",
                "onPeriod": true,
                "measure": 0,
                "contractualThreshold": 5,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "year": "2017-01",
                "onPeriod": true,
                "measure": 5,
                "contractualThreshold": 5,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "year": "2018-01",
                "onPeriod": true,
                "measure": 3,
                "contractualThreshold": 5,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "year": "2019-01",
                "onPeriod": true,
                "measure": 4,
                "contractualThreshold": 5,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            }
        ],
        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": 99,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                }
            }
        ]
    };
    var frequency = 'monthly';
  //   load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        vpnElement = $compile("<ora-qos-gar-internet indicator='indicator' frequency='frequency' class='col-md-12'></ora-qos-gar-internet>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('vpnElement should be defined', inject(function() {
        expect(vpnElement).toBeDefined();
    }));

    it('historicId should be defined', inject(function() {
        var historicId = 'qosGarInternet';
        expect(vpnElement.isolateScope().historicId).toBeDefined();
        expect(vpnElement.isolateScope().historicId).toEqual(historicId);
    }));

    it('garInternetConfigurations should be defined', inject(function() {
        var garInternetConfigurations = [
        {
            "name": "globalVpnInternetAvailabilityRate",
            "typeOfData": 'Indicator',
            "isColor": false,
            "unit": indicator.unit,
            "isTranslated": true
        },
    ];
        expect(vpnElement.isolateScope().garInternetConfigurations).toBeDefined();
        expect(vpnElement.isolateScope().garInternetConfigurations[0].name).toEqual(garInternetConfigurations[0].name);
        expect(vpnElement.isolateScope().garInternetConfigurations[0].typeOfData).toEqual(garInternetConfigurations[0].typeOfData);
        expect(vpnElement.isolateScope().garInternetConfigurations[0].isColor).toEqual(garInternetConfigurations[0].isColor);
        expect(vpnElement.isolateScope().garInternetConfigurations[0].unit).toEqual(garInternetConfigurations[0].unit);
        expect(vpnElement.isolateScope().garInternetConfigurations[0].isTranslated).toEqual(garInternetConfigurations[0].isTranslated);
    }));
    
    it('historicOptions should be defined', inject(function() {
        var historicOptions =  {
            isStartEnable: true,
            asLegend: true,
            annualSpecialDisplay: false,
            highlightClass: 'fill-brand-light-blue',
            seriesClass: [{
                name: indicator.name,
                bgClass: 'bg-brand-blue',
                fill: 'fill-brand-blue',
                typeHisto: 'bar',
                typeOfData: "indicator"
            }]
        };

        expect(vpnElement.isolateScope().historicOptions).toBeDefined();
        expect(vpnElement.isolateScope().historicOptions).toEqual(historicOptions);
    }));


    it('garInternetIndicatorInfo should be defined', inject(function() {
        var frequen = frequency;
        var garInternetIndicatorInfo = {
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "contractual": indicator.contractual,
            "unit": indicator.unit,
            "tobeConverted": false,
            "conversionFilterName": '',
            "isAnnualReport": ((frequen === 'ANN' || frequen === 'yearly') ? true : false)
        };
        expect(vpnElement.isolateScope().garInternetIndicatorInfo).toBeDefined();
        expect(vpnElement.isolateScope().garInternetIndicatorInfo).toEqual(garInternetIndicatorInfo);
    }));

    it('histoMetricsDefinition should be defined', inject(function() {
        var histoMetricsDefinition = [{
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "unit": indicator.unit
        }];

        expect(vpnElement.isolateScope().histoMetricsDefinition).toBeDefined();
        expect(vpnElement.isolateScope().histoMetricsDefinition).toEqual(histoMetricsDefinition);
    }));

    it('oneBarOption should be defined', inject(function() {
        var oneBarOption = {
            highlightClass: 'fill-brand-light-blue',
            axis: {
                y: {
                    format: ',.2'
                }
            },
            unit: indicator.unit,
            seriesClass: {
                measure: {
                    label: indicator.name,
                    longLabel : indicator.longLabel,
                    stroke: {
                        'class': 'fill-brand-light-blue'
                    },
                    types: ['bar'],
                    fillColor : 'fill-brand-blue',
                    bgClass: 'bg-brand-blue',
                    noContractual : true,
                },

            }
        };

        expect(vpnElement.isolateScope().oneBarOption).toBeDefined();
        expect(vpnElement.isolateScope().oneBarOption.highlightClass).toEqual(oneBarOption.highlightClass);
        expect(vpnElement.isolateScope().oneBarOption.axis).toEqual(oneBarOption.axis);
        expect(vpnElement.isolateScope().oneBarOption.unit).toEqual(oneBarOption.unit);
        expect(vpnElement.isolateScope().oneBarOption.seriesClass).toEqual(oneBarOption.seriesClass);
    }));

});