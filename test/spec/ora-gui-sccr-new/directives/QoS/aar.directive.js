'use strict';

describe('Directive: AAR', function() {

    var aarElement;

    var $scope;

    var indicator = {
        "name": "accessAvailabilityRate",
        "shortLabel": "% d'accès où la disponibilité est respectée",
        "longLabel": "% d'accès où la disponibilité est respectée Long  ",
        "contractual": true,
        "applicability": {
            "applicable": true
        },
        "unit": "%",
        "onPeriod": {
            "available": true,
            "measure": 91.4,
            "evolution": 0,
            "unit": "",
            "contractualThreshold": 99.2,
            "applicability": {
                "applicable": false
            },
            "contractualCommitmentFulfilled": true,
            "information": [{
                "label": "Les disponibilités non respectées sont:",
                "href": "aar"
            }]
        },
        "metricsDefinitions": [{
                "name": "accessExceedingAAR",
                "shortLabel": "Nombre d'accès où la disponibilité n'est pas respectée",
                "longLabel": "Nombre d'accès où la disponibilité n'est pas respectée Long",
                "unit": "",
                "applicabilityCriteria": false
            },
            {
                "name": "totalAccessAAR",
                "shortLabel": "Nombre total d'accès",
                "longLabel": "Nombre total d'accès Long",
                "unit": "",
                "applicabilityCriteria": false
            }
        ],

        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "applicability": {
                    "applicable": true
                },
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "measure": 100,
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 7,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "measure": 100,
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 7,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 7,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 386.0,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 90.91,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 11,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 8,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "accessExceedingAAR",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessAAR",
                        "measure": 13,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            }
        ]
    };

  //   load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function( $compile, _appConfig_) {
        aarElement = $compile("<ora-qos-aar indicator='indicator' class='col-md-12'></ora-qos-aar>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('aarElement should be defined', inject(function() {
        expect(aarElement).toBeDefined();
    }));

    it('aarElement should be correct', inject(function() {
        expect(aarElement.html().includes('<ora-generic-monthly-table')).toBeTruthy();
        expect(aarElement.html().includes('<div class="col-md-7">')).toBeTruthy();
        expect(aarElement.html().includes('<aar-table report-type="type"')).toBeTruthy();
    }));

    it('sould set the indicator href value to undefined', inject(function() {
        $scope.indicator.onPeriod.information[0].href = undefined;
        expect(!$scope.indicator.onPeriod.information[0].href).toBeTruthy();
    }));
    
    it('aar details table should not be displayed', inject(function() {
        //$scope.indicator.onPeriod.information[0].href = undefined;
        expect(aarElement.html().includes('<ora-generic-monthly-table')).toBeTruthy();
        expect(aarElement.html().includes('<div class="col-md-7">')).toBeTruthy();
        expect(aarElement.html().includes('<aar-table report-type="type"')).toBeFalsy();
    }));

    it('aarConfigurations should be defined', inject(function() {
        var aarConfigurations = [{
            "name": "accessAvailabilityRate",
            "typeOfData": 'Indicator',
            "isColor": false,
            "nonApplicableValue": '-',
            "isTranslated": true
        },
        {
            "name": "accessExceedingAAR",
            "typeOfData": 'Metrics',
            "isColor": true,
            "nonApplicableValue": '-',
            "isTranslated": true
        },
        {
            "name": "totalAccessAAR",
            "typeOfData": 'Metrics',
            "isColor": false,
            "nonApplicableValue": '-',
            "isTranslated": true
        }
     ];
        expect(aarElement.isolateScope().aarConfigurations).toBeDefined();

        for(var item = 0 ; item < 3 ;item++){
            expect(aarElement.isolateScope().aarConfigurations[item].name).toEqual(aarConfigurations[item].name);
            expect(aarElement.isolateScope().aarConfigurations[item].typeOfData).toEqual(aarConfigurations[item].typeOfData);
            expect(aarElement.isolateScope().aarConfigurations[item].isColor).toEqual(aarConfigurations[item].isColor);
            expect(aarElement.isolateScope().aarConfigurations[item].nonApplicableValue).toEqual(aarConfigurations[item].nonApplicableValue);
            expect(aarElement.isolateScope().aarConfigurations[item].isTranslated).toEqual(aarConfigurations[item].isTranslated);
        }
    }));

    it('information should be defined', inject(function() {
        expect(aarElement.isolateScope().information).toBeDefined();
        expect(aarElement.isolateScope().information).toEqual(indicator.onPeriod.information);
    })); 

    it('aarIndicatorInfo should be defined', inject(function() {
        var aarIndicatorInfo = {
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "contractual": indicator.contractual,
            "unit": indicator.unit,
            "tobeConverted": false,
            "conversionFilterName": ''
        };
        expect(aarElement.isolateScope().aarIndicatorInfo).toBeDefined();
        expect(aarElement.isolateScope().aarIndicatorInfo).toEqual(aarIndicatorInfo);
    }));


});