'use strict';

describe('Directive: paiIndicator', function() {

    var paiElement;

    var $scope;
    
    var indicator = {
        "name": "proactivityRate",
        "shortLabel": "Taux de proactivité ",
        "longLabel": "Taux de proactivité",
        "contractual": true,
        "unit": "%",
        "applicability": {
            "applicable": true
        },
        "metricsDefinitions": [{
                "name": "clientResponsibilityRate",
                "shortLabel": "clientResponsibilityRate short",
                "longLabel": "clientResponsibilityRate long",
                "unit": "%",
                "applicabilityCriteria": true,
                "category": {
                    "name": "",
                    "label": ""
                }
            },
            {
                "name": "totalIncidentNumber",
                "shortLabel": "totalIncidentNumber short",
                "longLabel": "totalIncidentNumber long",
                "unit": "%",
                "applicabilityCriteria": true,
                "category": {
                    "name": "",
                    "label": ""
                }
            },
            {
                "name": "totalAccessNumber",
                "shortLabel": "totalAccessNumber short",
                "longLabel": "totalAccessNumber long",
                "unit": "",
                "applicabilityCriteria": true,
                "category": {
                    "name": "",
                    "label": ""
                }
            }
        ],
        "onPeriod": {
            "available": true,
            "measure": 100,
            "evolution": 0,
            "unit": "",
            "contractualThreshold": 99.2,
            "applicability": {
                "applicable": true
            },
            "contractualCommitmentFulfilled": true
        },
        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": true,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 1,
                        "evolution": 8.1,
                        "evolutionRate": 8.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": null,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": false
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": true,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 9,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 0,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 9,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 0,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 6.0,
                "contractualThreshold": 18.9,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 15,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 5,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 92.0,
                "contractualThreshold": 52.0,
                "applicability": {
                    "applicable": false
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 9,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 3,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": false
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": null,
                "contractualThreshold": 55.6,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": null,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": false
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 10,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": false
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 97.0,
                "contractualThreshold": 17.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": true,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 7,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 11,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 99.0,
                "contractualThreshold": 78.8,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 12,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 17,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "measure": 85.0,
                "contractualThreshold": 42.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 8,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 3,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 97.0,
                "contractualThreshold": 37.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 9,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 10,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 69.0,

                "applicability": {
                    "applicable": false
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 21,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": false
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 7,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 10,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 87.0,
                "contractualThreshold": 67.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": false,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 9,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 0,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 15,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": null,
                "contractualThreshold": 37.0,
                "applicability": {
                    "applicable": true
                },
                "contractualCommitmentFulfilled": true,
                "metrics": [{
                        "name": "clientResponsibilityRate",
                        "measure": 19,
                        "evolution": 9.1,
                        "evolutionRate": 9.1,
                        "applicability": {
                            "applicable": false
                        }
                    },
                    {
                        "name": "totalIncidentNumber",
                        "measure": 10,
                        "evolution": 9.2,
                        "evolutionRate": 9.2,
                        "applicability": {
                            "applicable": false
                        }
                    },
                    {
                        "name": "totalAccessNumber",
                        "measure": 20,
                        "evolution": 8.2,
                        "evolutionRate": 8.2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            }
        ]
    }
            
  
  //   load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        paiElement = $compile("<pai-indicator indicator='indicator' class='col-md-12'></pai-indicator>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('paiElement should be defined', inject(function() {
        expect(paiElement).toBeDefined();
    }));

    it('historicId should be defined', inject(function() {
        var historicId = 'PAIhistoricId';
        expect(paiElement.isolateScope().PAIhistoricId).toBeDefined();
        expect(paiElement.isolateScope().PAIhistoricId).toEqual(historicId);
    }));

 
    it('pai indicatorInfo should be defined', inject(function() {
        var indicatorInfo = {
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "contractual": false,
            "unit": indicator.unit,
            "tobeConverted": false,
        };
        expect(paiElement.isolateScope().indicatorInfo).toBeDefined();
        expect(paiElement.isolateScope().indicatorInfo).toEqual(indicatorInfo);
    }));

    it('pai configurations should be defined', inject(function() {
        var configurations =[
            {
                "name": indicator.name,
                "typeOfData": 'Indicator',
                "isColor": false,
                "nonApplicableValue": 'NA',
                "isTranslated": true,
                "unit":indicator.unit
            },            
        {
            "name": "totalAccessNumber",
            "typeOfData": 'Metrics',
            "isTranslated": true,
            "isColor": false,
            "noNaColumn": true
        },
];
        expect(paiElement.isolateScope().configurations).toBeDefined();
        for(var item = 0 ; item < 2 ;item++){
            expect(paiElement.isolateScope().configurations[item].name).toEqual(configurations[item].name);
            expect(paiElement.isolateScope().configurations[item].typeOfData).toEqual(configurations[item].typeOfData);
            expect(paiElement.isolateScope().configurations[item].isColor).toEqual(configurations[item].isColor);
            expect(paiElement.isolateScope().configurations[item].unit).toEqual(configurations[item].unit);
            expect(paiElement.isolateScope().configurations[item].isTranslated).toEqual(configurations[item].isTranslated);
            expect(paiElement.isolateScope().configurations[item].nonApplicableValue).toEqual(configurations[item].nonApplicableValue);
        }
    }));
});