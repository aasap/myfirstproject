'use strict';

describe('Directive: IMS', function() {

    var imsElement;

    var $scope;

    var indicator =  {
        "name": "MSIFulfilment",
        "shortLabel": "% d'accés où I'IMS est repectée",
        "longLabel": "Interruption Maximale de Service",
        "contractual": true,
        "applicability": {
            "applicable": true,
            "inapplicabilityReason":"Non applicable sur cette période"
        },
        "unit": "%",
        "onPeriod": {
            "available": true,
            "measure": 98,
            "evolution": 0,
            "unit": "",
            "contractualThreshold": 99.2,
            "applicability": {
                "applicable": true
            },
            "contractualCommitmentFulfilled": true,
            "information": [{
                "label": "Les accés pour lesquels les IMS sont calculées sont:",
                "href": "pathsIms"
            }]
        },
        "metricsDefinitions": [{
                "name": "sitesRespectingMSI",
                "shortLabel": "Nombre d'IMS respectée",
                "longLabel": "Nombre d'IMS respectée Long",
                "unit": "",
                "applicabilityCriteria": false
            },
            {
                "name": "sitesExceedingMSI",
                "shortLabel": "Nombre d'IMS non respectée",
                "longLabel": "Nombre d'IMS non respectée",
                "unit": "",
                "applicabilityCriteria": false
            }
        ],
        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "applicability": {
                    "applicable": true
                },
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 5,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "applicability": {
                    "applicable": true
                },
                "measure": 75,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 6,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 50,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 1,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 1,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": 75,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 1,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 4,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "measure": 100,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 3,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 2,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 1,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 5,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 0,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": 75,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                },
                "metrics": [{
                        "name": "sitesRespectingMSI",
                        "measure": 6,
                        "applicability": {
                            "applicable": true
                        }
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "measure": 2,
                        "applicability": {
                            "applicable": true
                        }
                    }
                ]
            }
        ]
    };

    // load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
       $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        imsElement = $compile("<ora-sccr-qos-ims indicator='indicator' class='col-md-12'></ora-sccr-qos-ims>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('imsElement should be defined', inject(function() {
        expect(imsElement).toBeDefined();
    }));

    it('historicId should be defined', inject(function() {
        var historicId = 'mSccrHistoMSI';
        expect(imsElement.isolateScope().historicId).toBeDefined();
        expect(imsElement.isolateScope().historicId).toEqual(historicId);
    }));

    it('configurations should be defined', inject(function() {
        var configurations = [{
            "name": "MSIFulfilment",
            "typeOfData": 'Indicator',
            "isColor": false,
            "isTranslated": true
        },
        {
            "name": "sitesExceedingMSI",
            "typeOfData": 'Metrics',
            "isColor": false
        },
        {
            "name": "sitesRespectingMSI",
            "typeOfData": 'Metrics',
            "isColor": false
        }
        ];
        expect(imsElement.isolateScope().configurations).toBeDefined();
        for(var item = 0 ; item < 3 ;item++){
            expect(imsElement.isolateScope().configurations[item].name).toEqual(configurations[item].name);
            expect(imsElement.isolateScope().configurations[item].typeOfData).toEqual(configurations[item].typeOfData);
            expect(imsElement.isolateScope().configurations[item].isColor).toEqual(configurations[item].isColor);
        }
    }));

    it('historicOptions should be defined', inject(function() {
        var historicOptions = {
            isStartEnable: true,
            asLegend: true,
            annualSpecialDisplay: false,
            highlightClass: 'fill-brand-light-blue',
            seriesClass: [{
                    name: 'sitesRespectingMSI',
                    bgClass: 'bg-green',
                    fill: 'fill-green',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'sitesExceedingMSI',
                    bgClass: 'bg-red',
                    fill: 'fill-red',
                    typeHisto: 'bar',
                    unit: ''
                },
            ]
        };
        expect(imsElement.isolateScope().historicOptions).toBeDefined();
        expect(imsElement.isolateScope().historicOptions).toEqual(historicOptions);
    }));

    it('indicatorGeneralInfo should be defined', inject(function() {
        var indicatorGeneralInfo =  {
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "contractual": indicator.contractual,
            "unit": indicator.unit
        };
        expect(imsElement.isolateScope().indicatorGeneralInfo).toBeDefined();
        expect(imsElement.isolateScope().indicatorGeneralInfo).toEqual(indicatorGeneralInfo);
        expect(imsElement.isolateScope().type).toBeDefined();
        expect(imsElement.isolateScope().type).toEqual('msccr');
    }));

});