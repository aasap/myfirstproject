'use strict';

describe('Directive: GAR Ethernet', function() {

    var garElement;

    var $scope;
    
    var indicator = {
        "name": "globalEthernetAvailabilityRate",
        "shortLabel": "Disponibilité",
        "longLabel": "Disponibilité globale Ethernet",
        "unit": "%",
        "contractual": true,
        "applicability": {
            "applicable": true,
            "inapplicabilityCode": "notEnoughOffers",
            "inapplicabilityReason": "Inapplicability reason"
        },
        "onPeriod": {
            "available": true,
            "measure": 100,
            "evolution": 0,
            "unit": "",
            "contractualThreshold": 99.2,
            "applicability": {
                "applicable": false
            },
            "contractualCommitmentFulfilled": true
        },
        "annualHistory": [{
                "year": "2016-10",
                "onPeriod": true,
                "measure": 7.8,
                "contractualThreshold": 8,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "year": "2017-11",
                "onPeriod": true,
                "measure": 7,
                "contractualThreshold": 8,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "year": "2018-12",
                "onPeriod": true,
                "measure": 10,
                "contractualThreshold": 8,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "year": "2019-01",
                "onPeriod": true,
                "measure": 10,
                "contractualThreshold": 8,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            }
        ],
        "monthlyHistory": [{
                "month": "2016-08",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-09",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 90,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-10",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-11",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2016-12",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-01",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-02",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-03",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-04",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-05",
                "onPeriod": false,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-06",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-07",
                "onPeriod": true,
                "measure": 100,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": true,
                "applicability": {
                    "applicable": true
                }
            },
            {
                "month": "2017-08",
                "onPeriod": true,
                "measure": 99.5,
                "contractualThreshold": 100,
                "contractualCommitmentFulfilled": false,
                "applicability": {
                    "applicable": true
                }
            }
        ]
    };
    var frequency = ' yearly';
  //   load the directive's module
    beforeEach(angular.mock.module('ora-internal-gui'));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $scope = $rootScope.$new();
        $scope.indicator = indicator;
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        $httpBackend.expectGET('config/config.json').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/user').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/perimeters/perimeter').respond({});
        $httpBackend.expectGET(_appConfig_.urlApi + '/subscriptions/subscription').respond({});
    }));

    beforeEach(angular.mock.inject(function($rootScope, $compile, $httpBackend, _appConfig_) {
        garElement = $compile("<ora-qos-gar-ethernet indicator='indicator' section='section' frequency='frequency' class='col-md-12'></ora-qos-gar-ethernet>")($scope);
        $scope.$digest();
    }));

    afterEach(angular.mock.inject(function($httpBackend) {
        $httpBackend.flush();
    }));

    it('garElement should be defined', inject(function() {
        expect(garElement).toBeDefined();
    }));

    it('historicId should be defined', inject(function() {
        var historicId = 'qosGarEthernet';
        expect(garElement.isolateScope().historicId).toBeDefined();
        expect(garElement.isolateScope().historicId).toEqual(historicId);
    }));

    it('garEthernetConfigurations should be defined', inject(function() {
        var garEthernetConfigurations = [
                {
                    "name": "globalEthernetAvailabilityRate",
                    "typeOfData": 'Indicator',
                    "isColor": false,
                    "unit": indicator.unit,
                    "isTranslated": true
                }
            ];
        expect(garElement.isolateScope().garEthernetConfigurations).toBeDefined();
            expect(garElement.isolateScope().garEthernetConfigurations[0].name).toEqual(garEthernetConfigurations[0].name);
            expect(garElement.isolateScope().garEthernetConfigurations[0].typeOfData).toEqual(garEthernetConfigurations[0].typeOfData);
            expect(garElement.isolateScope().garEthernetConfigurations[0].isColor).toEqual(garEthernetConfigurations[0].isColor);
            expect(garElement.isolateScope().garEthernetConfigurations[0].unit).toEqual(garEthernetConfigurations[0].unit);
            expect(garElement.isolateScope().garEthernetConfigurations[0].isTranslated).toEqual(garEthernetConfigurations[0].isTranslated);
    }));
    
    it('historicOptions should be defined', inject(function() {
        var historicOptions =  {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-blue',
                seriesClass: [{
                    name: indicator.name,
                    bgClass: 'bg-brand-blue',
                    fill: 'fill-brand-blue',
                    typeHisto: 'bar',
                    typeOfData: "indicator"
                }]
            };

        expect(garElement.isolateScope().historicOptions).toBeDefined();
        expect(garElement.isolateScope().historicOptions).toEqual(historicOptions);
    }));


    it('garEthernetIndicatorInfo should be defined', inject(function() {
        var frequen = frequency;
        var garEthernetIndicatorInfo =  {
                "name": indicator.name,
                "shortLabel": indicator.shortLabel,
                "longLabel": indicator.longLabel,
                "contractual": indicator.contractual,
                "unit": indicator.unit,
                "tobeConverted": false,
                "conversionFilterName": '' 
            };
        expect(garElement.isolateScope().garEthernetIndicatorInfo).toBeDefined();
        expect(garElement.isolateScope().garEthernetIndicatorInfo).toEqual(garEthernetIndicatorInfo);
    }));

    it('histoMetricsDefinition should be defined', inject(function() {
        var histoMetricsDefinition =  [{
            "name": indicator.name,
            "shortLabel": indicator.shortLabel,
            "longLabel": indicator.longLabel,
            "unit": indicator.unit
        }];

        expect(garElement.isolateScope().histoMetricsDefinition).toBeDefined();
        expect(garElement.isolateScope().histoMetricsDefinition).toEqual(histoMetricsDefinition);
    }));

    it('oneBarOption should be defined', inject(function() {
        var oneBarOption =  {
            highlightClass: 'fill-brand-light-blue',
            axis: {
                y: {
                    format: ',.2'
                }
            },
            unit: indicator.unit,
            seriesClass: {
                measure: {
                    label: indicator.name,
                    longLabel : indicator.longLabel,
                    stroke: {
                        'class': 'fill-brand-light-blue'
                    },
                    types: ['bar'],
                    fillColor : 'fill-brand-blue',
                    bgClass: 'bg-brand-blue',
                    noContractual : true,
                },
            }
        }; 

        expect(garElement.isolateScope().oneBarOption).toBeDefined();
        expect(garElement.isolateScope().oneBarOption.highlightClass).toEqual(oneBarOption.highlightClass);
        expect(garElement.isolateScope().oneBarOption.axis).toEqual(oneBarOption.axis);
        expect(garElement.isolateScope().oneBarOption.unit).toEqual(oneBarOption.unit);
        expect(garElement.isolateScope().oneBarOption.seriesClass).toEqual(oneBarOption.seriesClass);
    }));

});