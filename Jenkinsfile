#!groovy
node {
    env.NODEJS_HOME = "${tool "NodeJS LTS"}"
    // on linux / mac
    env.PATH="${env.NODEJS_HOME}/bin:${env.PATH}"

    stage("Checkout") {
        retry(3) {
            checkout scm
        }
    }

    def packageJSON = readJSON file: "package.json"
    def JSON_VERSION = define_goroco(packageJSON.version)
    def PROJECT_NAME = "ORA-MSCCR"
    def TAR_NAME = PROJECT_NAME + "-" + JSON_VERSION + ".tar"
    def GROUP_ID = "05o"
    echo JSON_VERSION
    echo PROJECT_NAME
    echo TAR_NAME

    // Install dependencies only if the package.json changed
    stage('Install dependencies') {
        try {
          def PREVIOUS_VERSION = readFile file: "VERSION.txt"
          /*if (PREVIOUS_VERSION && PREVIOUS_VERSION != JSON_VERSION ){
            install_dependencies()
          }*/
          install_dependencies()
        } catch (Exception e) {
          echo "No file VERSION.txt, force npm install"
          install_dependencies()
        }

         writeFile file: 'VERSION.txt', text: JSON_VERSION
    }
    
    stage("Build") {
      sh "npm run build"
      sh "tar -cvf ${TAR_NAME} -C ./dist ."
    }
    
    try {
        stage("Unit tests") {
        sh "npm run testCi"
        }
    } catch (Exception e) {
        echo "Unit tests failed, but we still continue"
    }

    
   if (params.SONAR) {
      stage("SonarQube") {
        sh "npm run sonar-scanner"
      }

					
						
										   
    }

    stage('Publish') {
     nexusPublisher nexusInstanceId: 'NEXUS_ORA', nexusRepositoryId: 'releases', packages: 
     [
         [
             $class: 'MavenPackage', mavenAssetList: [
             [classifier: '', extension: '', filePath: TAR_NAME]
            ], 
            mavenCoordinate: [
                artifactId: PROJECT_NAME, 
                groupId: GROUP_ID, 
                packaging: 'tar', 
                version: JSON_VERSION
            ]
        ]
    ]
   }
}

def install_dependencies(){
    sh "rm -Rf ./node_modules && rm -f package-lock.json"
    sh 'npm install'
    sh 'sudo apt-get install libxss1'
}

def define_goroco(version){
  def tmp = version.tokenize('.')
  println tmp

  def release = 'G' + convert_two_digits(tmp[0])
  def major = 'R' + convert_two_digits(tmp[1])
  def minor = 'C' + convert_two_digits(tmp[2])

  return release + major + minor
}

def convert_two_digits(number){
  def tmp = number.toInteger()
  if(tmp >= 0 && tmp < 10){
    return '0' + tmp
  }

  return '' + tmp
}