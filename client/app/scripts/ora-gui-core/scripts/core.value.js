//'use strict'; //NOSONAR
/**
 * @ngdoc service
 * @name ora-gui-core.appConfig
 * @description
 * App config value
 */

const value = {
    appConfig: {
        urlApi: '/ora-internal-provider-api/rest',
        googleAnalytics: {
            sousDomaine: 'Reporting',
            universAffichage: 'Orange Reporting Analytics',
            sousUnivers: 'Application',
            domaineMarketing: 'Connectivité',
            //domaineMarketingMSccr: 'Connectivité consolidée',
            codeUnivers: 'ORA'
        }
    }
}

export default value;