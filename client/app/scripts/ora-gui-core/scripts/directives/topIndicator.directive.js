'use strict'; //NOSONAR
import oraTopIndicatorView from '../../views/topIndicator.html';
/**
 * @ngdoc directive
 * @name ora-gui-sccv.directive:oraTopIndicator
 */

const oraTopIndicator = function() {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraTopIndicatorView,
        transcluse: true,
        scope: {
            block: '=block',
            tooltips: '=tooltips'
        },
        link: function() {}
    };
};

export default oraTopIndicator;