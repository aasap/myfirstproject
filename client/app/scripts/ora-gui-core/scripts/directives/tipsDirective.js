/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraTips
 */

const oratips = function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $(function () {
                $(element).tooltip();
            });
        }
    };
};

export default oratips;