//'use strict'; //NOSONAR

/**
 * @ngdoc directive
 * @name ora-gui-core.directive:focus
 */

const focus = function ($timeout, textAngularManager) {
    'ngInject';
    return function ($scope, $element, $attr) {
        let editor = undefined;
        $scope.$watch(function () {
            const editorInstance = textAngularManager.retrieveEditor($attr.name);

            if ((editorInstance !== undefined) && (editor === undefined)) {
                editor = editorInstance;
                var timeout = 250; // wait 750 ms before evaluating
                // if we need to set focus to this element, then wait for the timeout and set focus
                $timeout(function () {
                    editorInstance.scope.displayElements.text.trigger('focus');
                }, timeout);
            }
        });

        if ($element[0]) {
            $timeout(function () {
                $element[0].focus();
            });
        }
    };
};

export default focus;