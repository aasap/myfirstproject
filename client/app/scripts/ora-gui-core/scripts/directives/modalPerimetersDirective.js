/**
 * Created by ggestin on 04/02/2016.
 */

//'use strict'; //NOSONAR

/**
 * @ngdoc directive
 * @name ora-gui-core.directive:stSummary
 */

const stSummary = function () {
    return {
        restrict: 'A',
        require: '^stTable',
        template: '<b ng-bind-template= "{{size > 1 ? size +\' périmètres\': size +\' périmètre\'}}"></b>',
        scope: {},
        link: function (scope, element, attr, ctrl) {
            scope.$watch(ctrl.getFilteredCollection, function (val) {
                scope.size = (val || []).length;
            });
        }
    };
};

export default stSummary;