import XlsxPopulate from 'xlsx-populate/browser/xlsx-populate.js';

/**
 * @ngdoc directive
 * @name ora-gui-core.directive:ora-export-to-excel
 */

const oraExportToExcel = function(perimeter, reports, $q, $filter, oraI18n, googleAnalytics,
    gtrService, ims, performanceNew, dimensionValuesConstant, excelConst) {
    'ngInject';
    return {
        restrict: 'E',
        template: '<a class="btn" ng-click="GenerateExcelFile()"><span><span class="icon-file-excel"></span></span></a>',
        scope: {
            export: '=export'
        },
        link: function(scope) {

            const columnName = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
            const filter = "engagementAchieved=false";

            scope.GenerateExcelFile = () => {
                const is_chrome = ((navigator.userAgent.toLowerCase().indexOf('chrome') > -1) && (navigator.vendor.toLowerCase().indexOf("google") > -1));
                googleAnalytics.logViewChange('Exporter rapport Excel', 'Reporting', 'Connectivité');

                let type = scope.export ? scope.export.type : '';
                if (is_chrome) {
                    return scope.generateBase64(type);
                } else {
                    return scope.generateBlob(type);
                }
            }

            scope.generateEmptyWorkBook = () => {
                return XlsxPopulate.fromBlankAsync();
            }

            scope.generateMSccr = function(type) {
                return scope.generateEmptyWorkBook()
                    .then(workbook => {

                        var deferred = $q.defer();
                        var promises = [];

                        const inventorySection = reports.report && reports.report.sections ? _.find(reports.report.sections, (s) => {
                            return s.name === 'inventory';
                        }) : null;

                        const incidentSection = reports.report && reports.report.sections ? _.find(reports.report.sections, (s) => {
                            return s.name === 'incident';
                        }) : null;

                        const qosSection = reports.report && reports.report.sections ? _.find(reports.report.sections, (s) => {
                            return s.name === 'qualityOfService';
                        }) : null;

                        const usageSection = reports.report && reports.report.sections ? _.find(reports.report.sections, (s) => {
                            return s.name === 'usage';
                        }) : null;

                        if (qosSection) {

                            const perfoRoundTripDelay = _.find(qosSection.indicators, (s) => {
                                return (s.name === 'performanceRoundTripDelay' && s.applicability && s.applicability.applicable);
                            });

                            const perfoPacketLossRate = _.find(qosSection.indicators, (s) => {
                                return (s.name === 'performancePacketLossRate' && s.applicability && s.applicability.applicable);
                            });

                            const perfoJitter = _.find(qosSection.indicators, (s) => {
                                return (s.name === 'performanceJitter' && s.applicability && s.applicability.applicable);
                            });
                            
                            let href = performanceNew.getUrlPerformanceDetailTable();
                            if ( href && (perfoRoundTripDelay || perfoJitter || perfoPacketLossRate) ) {
                                promises.push(performanceNew.getPerformanceTable(href, performanceNew.xTotal, 0, filter));
                            }

                            let mSIFulfilment = _.find(qosSection.indicators, (s) => {
                                return (s.name === 'MSIFulfilment' && s.applicability && s.applicability.applicable);
                            });

                            if(mSIFulfilment && mSIFulfilment.onPeriod && mSIFulfilment.onPeriod.information && mSIFulfilment.onPeriod.information[0].href){
                                promises.push(ims.getImsDetailsAll(mSIFulfilment, 'sccr'));
                            }

                            let gtrFulfilment = _.find(qosSection.indicators, (s) => {
                                return (s.name === 'GTTRFulfilment' && s.applicability && s.applicability.applicable);
                            });
                            
                            if(gtrFulfilment && gtrFulfilment.onPeriod && gtrFulfilment.onPeriod.information && gtrFulfilment.onPeriod.information[0].href){
                                promises.push(gtrService.getGtrDetailsAll(gtrFulfilment, scope.export.type.toLowerCase()));
                            }
                        }

                        $q.all(promises).finally(() => {
                            if (inventorySection && inventorySection.applicability && inventorySection.applicability.applicable) {
                                const offersIndicator = _.find(inventorySection.indicators, (s) => {
                                    return (s.name === 'SCCROffersEvolution' && s.applicability && s.applicability.applicable);
                                });

                                const sitesIndicator = _.find(inventorySection.indicators, (s) => {
                                    return (s.name === 'SCCRSitesEvolution' && s.applicability && s.applicability.applicable);
                                });

                                const optionsIndicator = _.find(inventorySection.indicators, (s) => {
                                    return (s.name === 'SCCROptionsEvolution' && s.applicability && s.applicability.applicable);
                                });

                                if (offersIndicator || sitesIndicator || optionsIndicator) {
                                    scope.inventorySheet(workbook, inventorySection.label, offersIndicator, sitesIndicator, optionsIndicator);
                                }
                            }

                            if (incidentSection && incidentSection.applicability && incidentSection.applicability.applicable) {
                                const incidentIndicator = _.find(incidentSection.indicators, (s) => {
                                    return (s.name === 'incidentsWithServiceInterruptionNumber' && s.applicability && s.applicability.applicable);
                                });

                                if (incidentIndicator) {
                                    scope.incidentSheet(workbook, incidentIndicator, incidentSection);
                                }
                            }

                            if (qosSection && qosSection.applicability && qosSection.applicability.applicable) {

                                const globalVpnInternetAvailabilityRate = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'globalVpnInternetAvailabilityRate' && s.applicability && s.applicability.applicable);
                                });

                                const globalEthernetAvailabilityRate = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'globalEthernetAvailabilityRate' && s.applicability && s.applicability.applicable);
                                });
                                const accessAvailabilityRate = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'accessAvailabilityRate' && s.applicability && s.applicability.applicable);
                                });
                                const GTTRFulfilment = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'GTTRFulfilment' && s.applicability && s.applicability.applicable);
                                });

                                const proactivityRate = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'proactivityRate' && s.applicability && s.applicability.applicable);
                                });

                                const MSIFulfilment = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'MSIFulfilment' && s.applicability && s.applicability.applicable);
                                });

                                const perfoRoundTripDelay = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'performanceRoundTripDelay' && s.applicability && s.applicability.applicable);
                                });

                                const perfoPacketLossRate = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'performancePacketLossRate' && s.applicability && s.applicability.applicable);
                                });

                                const perfoJitter = _.find(qosSection.indicators, (s) => {
                                    return (s.name === 'performanceJitter' && s.applicability && s.applicability.applicable);
                                });

                                if (globalVpnInternetAvailabilityRate || globalEthernetAvailabilityRate || accessAvailabilityRate || GTTRFulfilment || proactivityRate || MSIFulfilment || perfoRoundTripDelay || perfoPacketLossRate || perfoJitter) {
                                    scope.qosSheet(workbook, globalVpnInternetAvailabilityRate, globalEthernetAvailabilityRate, accessAvailabilityRate, GTTRFulfilment, proactivityRate, MSIFulfilment, perfoRoundTripDelay, perfoPacketLossRate, perfoJitter, qosSection);
                                }
                            }

                            if (usageSection && usageSection.applicability && usageSection.applicability.applicable) {
                                const SCCRDataVolume = _.find(usageSection.indicators, (s) => {
                                    return (s.name === 'SCCRDataVolume' && s.applicability && s.applicability.applicable);
                                });

                                const SCCRDataLoad = _.find(usageSection.indicators, (s) => {
                                    return (s.name === 'SCCRDataLoad' && s.applicability && s.applicability.applicable);
                                });

                                if (SCCRDataVolume) {
                                    scope.usageSheet(workbook, SCCRDataVolume, SCCRDataLoad, usageSection);
                                }
                            }

                            deferred.resolve(workbook);
                        });

                        return deferred.promise;

                    }).then((workbook) => {
                        // Write to file.
                        return workbook.outputAsync({
                            type: type
                        });
                    });
            }

            scope.generatePerformance = function(type) {
                return scope.generateEmptyWorkBook()
                    .then(workbook => {

                        const qualityOfService = _.filter(reports.report.sections, {
                            "name": "qualityOfService"
                        })[0];

                        if (qualityOfService) {

                            const performanceRoundTripDelay = qualityOfService.indicators ? _.find(qualityOfService.indicators, (s) => {
                                return s.name === "performanceRoundTripDelay"
                            }) : null;

                            const performanceJitter = qualityOfService.indicators ? _.find(qualityOfService.indicators, (s) => {
                                return s.name === "performanceJitter"
                            }) : null;

                            const performancePacketLossRate = qualityOfService.indicators ? _.find(qualityOfService.indicators, (s) => {
                                return s.name === "performancePacketLossRate"
                            }) : null;

                            if (performanceRoundTripDelay || performanceJitter || performancePacketLossRate) {
                                return performanceNew.getAllPerformanceTable('pathsPerformance').then((tableData) => {
                                    scope.performanceSheet(workbook, tableData);
                                    // Write to file.
                                    return workbook.outputAsync({
                                        type: type
                                    });
                                });
                            }
                        } else {
                            return null;
                        }
                    });
            }

            scope.performanceSheet = (workbook, tableData) => {
                const sheet = workbook.sheet(0).name("performance");

                addInformationLine(sheet);

                let index = 3;

                sheet.column('A').width(25);
                sheet.column('B').width(25);
                sheet.column('C').width(25);
                sheet.column('D').width(25);
                sheet.column('E').width(25);
                sheet.column('F').width(25);
                sheet.column('G').width(25);
                sheet.column('H').width(25);
                sheet.column('I').width(25);
                sheet.column('J').width(25);
                sheet.column('K').width(25);
                sheet.column('L').width(25);

                createPerformanceDetailTable(index, sheet, tableData);
            };

            function createPerformanceDetailTable(index, sheet, tableData) {

                let threshold = oraI18n.oraI18nLabels.PERF_DET_THRESHOLD_SHORTLABEL;
                let measure = oraI18n.oraI18nLabels.PERF_DET_MEASURE_SHORTLABEL;

                // Header
                sheet.range("A" + index + ":A" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_MONTH_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.range("B" + index + ":B" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_ENDPOINTA_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.range("C" + index + ":C" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_ENDPOINTB_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.range("D" + index + ":D" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_SERVICECLASS_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.range("E" + index + ":E" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_SAMPLERATE_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.range("F" + index + ":G" + index).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_ROUNDTRIPDELAY_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.range("H" + index + ":I" + index).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_PACKETLOSSRATE_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.range("J" + index + ":K" + index).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_JITTER_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                index = index + 1;

                sheet.cell("F" + index).value(threshold).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.cell("G" + index).value(measure).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.cell("H" + index).value(threshold).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.cell("I" + index).value(measure).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.cell("J" + index).value(threshold).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                sheet.cell("K" + index).value(measure).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                index = index + 1;

                let totalRoundTripDelayMeasure = 0;
                let totalPacketLossRateMeasure = 0;
                let totalJitterMeasure = 0;

                let totalRoundTripDelayKo = 0;
                let totalPacketLossRateKo = 0;
                let totalJitterKo = 0;

                // GTR detail table
                if (tableData && tableData.length > 0) {

                    // Sort the array by date
                    tableData.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    tableData.forEach((row) => {

                        let roundTripDelayTreshold = row.roundTripDelay && row.roundTripDelay.threshold != undefined ? row.roundTripDelay.threshold : 'NA';
                        let packetLossRateTreshold = row.packetLossRate && row.packetLossRate.threshold != undefined ? row.packetLossRate.threshold : 'NA';
                        let jitterTreshold = row.jitter && row.jitter.threshold != undefined ? row.jitter.threshold : 'NA';

                        let roundTripDelayMeasure = row.roundTripDelay && row.roundTripDelay.measure != undefined ? row.roundTripDelay.measure : 'NA';
                        let packetLossRateMeasure = row.packetLossRate && row.packetLossRate.measure != undefined ? row.packetLossRate.measure : 'NA';
                        let jitterMeasure = row.jitter && row.jitter.measure != undefined ? row.jitter.measure : 'NA';

                        sheet.cell("A" + index).value($filter('date')(new Date(row.month), "MMMM", '+0')).style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("B" + index).value(row.path && row.path.endpointA ? row.path.endpointA : 'NA').style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("C" + index).value(row.path && row.path.endpointB ? row.path.endpointB : 'NA').style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("D" + index).value(row.serviceClass ? row.serviceClass : 'NA').style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("E" + index).value(row.sampleRate != undefined ? row.sampleRate : 'NA').style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("F" + index).value(roundTripDelayTreshold).style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("G" + index).value(roundTripDelayMeasure).style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("H" + index).value(packetLossRateTreshold).style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("I" + index).value(packetLossRateMeasure).style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("J" + index).value(jitterTreshold).style("horizontalAlignment", "center").style("border", true);
                        sheet.cell("K" + index).value(jitterMeasure).style("horizontalAlignment", "center").style("border", true);

                        if (roundTripDelayTreshold !== 'NA' && roundTripDelayMeasure !== 'NA') {
                            totalRoundTripDelayMeasure = totalRoundTripDelayMeasure + 1;
                            sheet.cell("G" + index).style("fill", roundTripDelayMeasure <= roundTripDelayTreshold ? '009900' : 'E50000');
                            totalRoundTripDelayKo = roundTripDelayMeasure <= roundTripDelayTreshold ? totalRoundTripDelayKo : totalRoundTripDelayKo + 1;
                        }

                        if (packetLossRateTreshold !== 'NA' && packetLossRateMeasure !== 'NA') {
                            totalPacketLossRateMeasure = totalPacketLossRateMeasure + 1;
                            sheet.cell("I" + index).style("fill", packetLossRateMeasure <= packetLossRateTreshold ? '009900' : 'E50000');
                            totalPacketLossRateKo = packetLossRateMeasure <= packetLossRateTreshold ? totalPacketLossRateKo : totalPacketLossRateKo + 1;
                        }

                        if (jitterTreshold !== 'NA' && jitterMeasure !== 'NA') {
                            totalJitterMeasure = totalJitterMeasure + 1;
                            sheet.cell("K" + index).style("fill", jitterMeasure <= jitterTreshold ? '009900' : 'E50000');
                            totalJitterKo = jitterMeasure <= jitterTreshold ? totalJitterKo : totalJitterKo + 1;
                        }

                        index = index + 1;
                    });
                }

                index = index + 1;

                sheet.cell("F" + index).value("total").style("horizontalAlignment", "center").style("border", true);
                sheet.cell("G" + index).value("nb ko").style("horizontalAlignment", "center").style("border", true);
                sheet.cell("H" + index).value("total").style("horizontalAlignment", "center").style("border", true);
                sheet.cell("I" + index).value("nb ko").style("horizontalAlignment", "center").style("border", true);
                sheet.cell("J" + index).value("total").style("horizontalAlignment", "center").style("border", true);
                sheet.cell("K" + index).value("nb ko").style("horizontalAlignment", "center").style("border", true);

                index = index + 1;

                sheet.cell("F" + index).value(totalRoundTripDelayMeasure).style("horizontalAlignment", "center").style("border", true);
                sheet.cell("G" + index).value(totalRoundTripDelayKo).style("horizontalAlignment", "center").style("border", true);
                sheet.cell("H" + index).value(totalPacketLossRateMeasure).style("horizontalAlignment", "center").style("border", true);
                sheet.cell("I" + index).value(totalPacketLossRateKo).style("horizontalAlignment", "center").style("border", true);
                sheet.cell("J" + index).value(totalJitterMeasure).style("horizontalAlignment", "center").style("border", true);
                sheet.cell("K" + index).value(totalJitterKo).style("horizontalAlignment", "center").style("border", true);

                sheet.cell("G" + index).style("fill", totalRoundTripDelayKo <= 0 ? '009900' : 'E50000');
                sheet.cell("I" + index).style("fill", totalPacketLossRateKo <= 0 ? '009900' : 'E50000');
                sheet.cell("K" + index).style("fill", totalJitterKo <= 0 ? '009900' : 'E50000');

            }

            scope.inventorySheet = (workbook, label, offersIndicator, sitesIndicator, optionsIndicator) => {
                const sheet = workbook.sheet(0).name(label);

                addInformationLine(sheet);

                let index = 4;

                sheet.column('A').width(75);
                sheet.column('B').width(25);
                sheet.column('C').width(25);
                sheet.column('D').width(25);

                // First table
                sheet.cell("A3").value(oraI18n.oraI18nLabels.offersLabel).style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                sheet.cell("B3").value('+0').style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                sheet.cell("C3").value(oraI18n.oraI18nLabels.evolutionLabel).style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                sheet.cell("D3").value(oraI18n.oraI18nLabels.totalLabel).style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);

                if (offersIndicator && offersIndicator.onPeriod && offersIndicator.onPeriod.applicability &&
                    offersIndicator.onPeriod.applicability.applicable && offersIndicator.onPeriod.metrics) {
                    let totalEvolution = 0;
                    offersIndicator.onPeriod.metrics.forEach((element) => {
                        const metricDefinition = _.find(offersIndicator.metricsDefinitions, (s) => {
                            return s.name === element.name;
                        });

                        if (metricDefinition) {
                            sheet.cell("A" + (index)).value(metricDefinition.shortLabel).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("B" + (index)).value(metricDefinition.category ? '(' + metricDefinition.category.label + ')' : '').style("border", true);
                            sheet.cell("C" + (index)).value(element.evolution > 0 ? '+' + element.evolution : element.evolution).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("D" + (index)).value(element.measure).style("border", true).style("horizontalAlignment", "left");
                            totalEvolution = totalEvolution + element.evolution;
                            index = index + 1;
                        }
                    });

                    sheet.cell("B3").value(totalEvolution >= 0 ? '+' + totalEvolution : totalEvolution).style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                }
                index = index + 1;

                // Second table
                sheet.cell("A" + (index)).value(oraI18n.oraI18nLabels.sitesLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("fontColor", "ffffff").style("border", true);
                sheet.cell("B" + (index)).value('+0').style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("fontColor", "ffffff").style("border", true);
                sheet.cell("C" + (index)).value(oraI18n.oraI18nLabels.evolutionLabel).style("bold", true).style("horizontalAlignment", "center").style("fontColor", "ffffff").style("fill", "50BE87").style("border", true);
                sheet.cell("D" + (index)).value(oraI18n.oraI18nLabels.totalLabel).style("bold", true).style("horizontalAlignment", "center").style("fontColor", "ffffff").style("fill", "50BE87").style("border", true);

                index = index + 1;

                if (sitesIndicator && sitesIndicator.onPeriod && sitesIndicator.onPeriod.applicability &&
                    sitesIndicator.onPeriod.applicability.applicable) {
                    sheet.cell("A" + (index)).value('').style("border", true);
                    sheet.cell("B" + (index)).value('').style("border", true);
                    sheet.cell("C" + (index)).value(sitesIndicator.onPeriod.evolution > 0 ? '+' + sitesIndicator.onPeriod.evolution : sitesIndicator.onPeriod.evolution).style("border", true).style("horizontalAlignment", "left");
                    sheet.cell("D" + (index)).value(sitesIndicator.onPeriod.measure).style("border", true).style("horizontalAlignment", "left");
                    sheet.cell("B" + (index - 1)).value(sitesIndicator.onPeriod.evolution >= 0 ? '+' + sitesIndicator.onPeriod.evolution : sitesIndicator.onPeriod.evolution).style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("fontColor", "ffffff").style("border", true);
                    index = index + 1;
                }

                index = index + 1;

                // Third table
                sheet.cell("A" + (index)).value(scope.decodeStr(oraI18n.oraI18nLabels.optionsLabel)).style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                sheet.cell("B" + (index)).value('+0').style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                sheet.cell("C" + (index)).value(oraI18n.oraI18nLabels.evolutionLabel).style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                sheet.cell("D" + (index)).value(oraI18n.oraI18nLabels.totalLabel).style("fontColor", "ffffff").style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);

                let headerIndex = index;
                index = index + 1;

                if (optionsIndicator && optionsIndicator.onPeriod && optionsIndicator.onPeriod.applicability &&
                    optionsIndicator.onPeriod.applicability.applicable && optionsIndicator.onPeriod.metrics) {
                    const subscribedMetrics = ["applicativePerformanceEAMRiverbed", "applicativePerformanceNetworkBoost", "personalizedSupervision", "digitalMonitoring"];
                    let totalEvolution = 0;
                    let optionsWithMeasure = [];
                    let subscribedOptions = [];
                    let nonSubscribedOptions = [];

                    optionsIndicator.onPeriod.metrics.forEach((element) => {
                        const metricDefinition = _.find(optionsIndicator.metricsDefinitions, (s) => {
                            return s.name === element.name;
                        });

                        if (!!element.measure && element.measure > 0) {
                            if (_.indexOf(subscribedMetrics, element.name) > -1) {
                                subscribedOptions.push({
                                    measure: oraI18n.oraI18nLabels.subscribed,
                                    evolution: oraI18n.oraI18nLabels.subscribed,
                                    label: metricDefinition.shortLabel
                                });
                            } else {
                                optionsWithMeasure.push({
                                    measure: element.measure,
                                    evolution: element.evolution,
                                    label: metricDefinition.shortLabel
                                });
                            }
                        } else {
                            nonSubscribedOptions.push({
                                measure: oraI18n.oraI18nLabels.nonSubscribed,
                                evolution: oraI18n.oraI18nLabels.nonSubscribed,
                                label: metricDefinition.shortLabel
                            });
                        }
                    });

                    optionsWithMeasure = $filter('orderBy')(optionsWithMeasure, ['-measure', 'label']);
                    subscribedOptions = $filter('orderBy')(subscribedOptions, 'label');
                    nonSubscribedOptions = $filter('orderBy')(nonSubscribedOptions, 'label');

                    const arrayTmp = optionsWithMeasure.concat(subscribedOptions).concat(nonSubscribedOptions);

                    arrayTmp.forEach((element) => {
                        sheet.cell("A" + (index)).value(element.label).style("border", true).style("horizontalAlignment", "left");
                        sheet.cell("B" + (index)).value('').style("border", true);
                        sheet.cell("C" + (index)).value(element.evolution > 0 && typeof element.evolution != 'string' ? '+' + element.evolution : element.evolution).style("border", true).style("horizontalAlignment", "left");
                        sheet.cell("D" + (index)).value(element.measure).style("border", true).style("horizontalAlignment", "left");
                        totalEvolution = typeof element.evolution != 'string' ? totalEvolution + element.evolution : totalEvolution;
                        index = index + 1;
                    });

                    sheet.cell("B" + headerIndex).value(totalEvolution >= 0 ? '+' + totalEvolution : totalEvolution).style("bold", true).style("horizontalAlignment", "center").style("fill", "50BE87").style("border", true);
                }

                const conf = [{
                    metricName: 'SCCROffersEvolution',
                    typeOfData: 'indicator',
                    dataFrom: 0
                }, {
                    metricName: 'SCCRSitesEvolution',
                    typeOfData: 'indicator',
                    dataFrom: 1
                }];

                createThirteenMonthTable(sheet, [offersIndicator, sitesIndicator], conf, index, {
                    fill: "50BE87",
                    fontColor: "ffffff"
                }, 0);

                return true;
            }

            scope.incidentSheet = (workbook, incidentIndicator, incidentSection) => {
                const sheet = workbook.addSheet(incidentSection.label);
                const orange = oraI18n.oraI18nLabels.orange;
                const customer = oraI18n.oraI18nLabels.customer;
                const thirdParty = oraI18n.oraI18nLabels.thirdParty;
                const others = oraI18n.oraI18nLabels.others;
                let index = 4;

                sheet.column('A').width(25);
                sheet.column('B').width(25);
                sheet.column('C').width(25);
                sheet.column('D').width(25);
                sheet.column('E').width(25);

                // First line
                addInformationLine(sheet);

                let incidentOrangeMetricDef = _.find(incidentIndicator.metricsDefinitions, (s) => { return s.name === 'OrangeResponsibility';});

                let incidentCustomerMetricDef = _.find(incidentIndicator.metricsDefinitions, (s) => { return s.name === 'CustomerResponsibility';});

                let incidentThirdPartyMetricDef = _.find(incidentIndicator.metricsDefinitions, (s) => { return s.name === 'ThirdPartyResponsibility';});

                let incidentOtherMetricDef = _.find(incidentIndicator.metricsDefinitions, (s) => { return s.name === 'OtherResponsibility';});

                // First table header
                sheet.cell("A3").value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "FFD200").style("border", true);
                sheet.cell("B3").value(incidentOrangeMetricDef && incidentOrangeMetricDef.shortLabel && incidentOrangeMetricDef.shortLabel != null ? incidentOrangeMetricDef.shortLabel : orange).style("bold", true).style("horizontalAlignment", "center").style("fill", "FFD200").style("border", true);
                sheet.cell("C3").value(incidentCustomerMetricDef && incidentCustomerMetricDef.shortLabel && incidentCustomerMetricDef.shortLabel != null ? incidentCustomerMetricDef.shortLabel : customer).style("bold", true).style("horizontalAlignment", "center").style("fill", "FFD200").style("border", true);
                sheet.cell("D3").value(incidentThirdPartyMetricDef && incidentThirdPartyMetricDef.shortLabel && incidentThirdPartyMetricDef.shortLabel != null ? incidentThirdPartyMetricDef.shortLabel : thirdParty).style("bold", true).style("horizontalAlignment", "center").style("fill", "FFD200").style("border", true);
                sheet.cell("E3").value(incidentOtherMetricDef && incidentOtherMetricDef.shortLabel && incidentOtherMetricDef.shortLabel != null ? incidentOtherMetricDef.shortLabel : others).style("bold", true).style("horizontalAlignment", "center").style("fill", "FFD200").style("border", true);

                // Sort the array by date
                incidentIndicator.monthlyHistory.sort(function(a, b) {
                    return new Date(a.month) - new Date(b.month);
                });

                incidentIndicator.monthlyHistory.forEach((element, i) => {
                    if (element && element.onPeriod && element.applicability &&
                        element.applicability.applicable) {
                        sheet.cell("A" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                        sheet.cell("B" + (index)).value(element.metrics[0] && element.metrics[0].measure != null ? element.metrics[0].measure : 'NA').style("border", true).style("horizontalAlignment", "left");
                        sheet.cell("C" + (index)).value(element.metrics[0] && element.metrics[1].measure != null ? element.metrics[1].measure : 'NA').style("border", true).style("horizontalAlignment", "left");
                        sheet.cell("D" + (index)).value(element.metrics[0] && element.metrics[2].measure != null ? element.metrics[2].measure : 'NA').style("border", true).style("horizontalAlignment", "left");
                        sheet.cell("E" + (index)).value(element.metrics[0] && element.metrics[3].measure != null ? element.metrics[3].measure : 'NA').style("border", true).style("horizontalAlignment", "left");
                        index = index + 1;
                    }
                });

                const conf = [{
                    metricName: 'OrangeResponsibility',
                    typeOfData: 'metric',
                    dataFrom: 0
                }, {
                    metricName: 'CustomerResponsibility',
                    typeOfData: 'metric',
                    dataFrom: 0
                }, {
                    metricName: 'ThirdPartyResponsibility',
                    typeOfData: 'metric',
                    dataFrom: 0
                }, {
                    metricName: 'OtherResponsibility',
                    typeOfData: 'metric',
                    dataFrom: 0
                }];

                createThirteenMonthTable(sheet, [incidentIndicator], conf, index, {
                    fill: "FFD200",
                    fontColor: "000000"
                }, 0);
            }

            scope.usageSheet = (workbook, SCCRDataVolume, SCCRDataLoad, usageSection) => {
                const sheet = workbook.addSheet(usageSection.label);
                const usageVolume = oraI18n.oraI18nLabels.usageVolume;
                const volAccessRef = oraI18n.oraI18nLabels.volAccessRef;
                const loadAccessRef = oraI18n.oraI18nLabels.loadAccessRef;
                const usageSentLabel = oraI18n.oraI18nLabels.usageSentLabel;
                const usageReceivedLabel = oraI18n.oraI18nLabels.usageReceivedLabel;

                let index = 3;

                sheet.column('A').width(25);
                sheet.column('B').width(25);
                sheet.column('C').width(25);
                sheet.column('D').width(25);
                sheet.column('E').width(25);
                sheet.column('F').width(25);
                sheet.column('G').width(25);

                // First line
                addInformationLine(sheet);


                const conf = [{
                    metricName: 'outVolume',
                    typeOfData: 'metric',
                    dataFrom: 0,
                    filter: 'unitConverter'
                }, {
                    metricName: 'inVolume',
                    typeOfData: 'metric',
                    dataFrom: 0,
                    filter: 'unitConverter'
                }];

                if(SCCRDataVolume.dimensionProvided !== dimensionValuesConstant.ONLY){
                    sheet.cell("A"+index).value(usageVolume).style("bold", true);
                    index = index+2;
                    index = createUsageTables(sheet, SCCRDataVolume, index, conf);
                }

                if (SCCRDataVolume && SCCRDataVolume.onPeriod && SCCRDataVolume.onPeriod.information &&
                     SCCRDataVolume.onPeriod.information[0] && SCCRDataVolume.onPeriod.information[0].rows) {
                    const rowKeys = ['siteName', 'accessName', 'overrunNumber', 'averageDuration', 'maximumLoadRate', 'way'];
                    sheet.cell("A" + index).value(SCCRDataVolume.onPeriod.information[0].label).style("bold", true);
                    index = index + 1;
                    let tabRows = [];
                    let rowsCpy = [];
                    angular.copy(SCCRDataVolume.onPeriod.information[0].rows, rowsCpy);
                    let titles = _.remove(rowsCpy, function(row){
                        return row.type === 'title';
                    })[0];
                    angular.forEach(rowsCpy, function(row){
                let r = buildRoW(row);
                tabRows.push(r);
                    });
                    tabRows.sort(function(a, b) {
                if (Number(a.duration) === Number(b.duration)) {
                            //sort by the siteName (index = 0)
                            return a.siteName > b.siteName ? 1 : -1;
                        }
                        return Number(a.duration) < Number(b.duration) ? 1 : -1;
                    });
                    tabRows.unshift(buildRoW(titles));
                    tabRows.forEach((row, i) => {

                        rowKeys.forEach((rowKey, j) => {

                                let unit = rowKey === 'maximumLoadRate' ? '%' : '';
                                if (i == 0) {
                                    sheet.cell((columnName[j + 1]) + index).value(row[rowKey]).style("bold", true).style("horizontalAlignment", "center").style("fontColor", "ffffff").style("fill", "A885D8").style("border", true);
                                } else {
                                    let val = rowKey === 'averageDuration' ? $filter('timeNumber')(row[rowKey], oraI18n.selectedCdLang) : row[rowKey];
                                    sheet.cell((columnName[j + 1]) + index).value(val + unit).style("border", true);
                                }
                        });

                        index = index + 1;
                    });

                    index = index + 1;
                }

                //access ref part

                let dataLoadDisplayed = [];
                let displayedRefcom = [];

                const confVolume = [{
                    metricName: 'outVolume',
                    typeOfData: 'dimension',
                    dataFrom: 0,
                    label: usageSentLabel,
                    filter: 'unitConverter'
                }, {
                    metricName: 'inVolume',
                    typeOfData: 'dimension',
                    dataFrom: 0,
                    label: usageReceivedLabel,
                    filter: 'unitConverter'
                }];

                const confAccessLoad = [{
                    metricName: 'outLoad',
                    typeOfData: 'dimension',
                    dataFrom: 0,
                    label: usageSentLabel,
                    unit: '%'
                }, {
                    metricName: 'inLoad',
                    typeOfData: 'dimension',
                    dataFrom: 0,
                    label: usageReceivedLabel,
                    unit: '%'
                }];

                if (SCCRDataVolume && SCCRDataVolume.dimension && SCCRDataVolume.dimension.values) {
                    SCCRDataVolume.dimension.values = $filter('orderBy')(SCCRDataVolume.dimension.values, ['dimensionGroup', 'dimensionValue']);
                    SCCRDataVolume.dimension.values.forEach(element => {

                        //display the refcom
                        if (displayedRefcom.indexOf(element.dimensionGroup) < 0) {
                            sheet.cell("A" + index).value(oraI18n.oraI18nLabels.accesRefLong.replace('{refcom}', element.dimensionGroup)).style("bold", true);
                            index = index + 2;
                            displayedRefcom.push(element.dimensionGroup);
                        }

                        //display the non applicability reason
                        if (!element.applicability.applicable) {
                            sheet.cell("B" + index).value(element.applicability.inapplicabilityReason).style("border", true);
                            index = index + 2;
                        }
                        SCCRDataLoad.dimension.values = $filter('orderBy')(SCCRDataLoad.dimension.values, ['dimensionGroup', 'dimensionValue']);
                        //get all the access ref linked to the element refcom
                        let refComElementLoad = _.filter(SCCRDataLoad.dimension.values, (value) => {
                            return value.dimensionGroup === element.dimensionGroup && value.applicability.applicable;
                        });

                        angular.forEach(refComElementLoad, function(loadRefcom) {
                            {
                                if (loadRefcom.dimensionValue < element.dimensionValue && loadRefcom.applicability.applicable) {
                                    sheet.cell("A" + index).value(loadRefcom.dimensionValue).style("bold", true);
                                    index = index + 2;
                                    sheet.cell("A" + index).value(loadAccessRef).style("bold", true);
                                    index = index + 2;
                                    index = createUsageTables(sheet, loadRefcom, index, confAccessLoad);
                                    dataLoadDisplayed.push(loadRefcom.dimensionValue);
                                    _.remove(refComElementLoad, (val) => {
                                        return val.dimensionValue === loadRefcom.dimensionValue;
                                    });
                                }
                            }
                        });

                        if (element.monthlyHistory && element.monthlyHistory.length > 0) {

                            if (element.applicability.applicable) {
                                sheet.cell("A" + index).value(element.dimensionValue).style("bold", true);
                                index = index + 2;
                                sheet.cell("A" + index).value(volAccessRef).style("bold", true);
                                index = index + 2;

                                index = createUsageTables(sheet, element, index, confVolume);
                            }

                            if (SCCRDataLoad.dimension && SCCRDataLoad.dimension.values) {

                                let accessLoad = _.find(refComElementLoad, (value) => {
                                    return value.dimensionValue === element.dimensionValue;
                                });

                                if (accessLoad && dataLoadDisplayed.indexOf(accessLoad.dimensionValue) === -1 && accessLoad.applicability.applicable) {
                                    sheet.cell("A" + index).value(loadAccessRef).style("bold", true);
                                    index = index + 2;
                                    index = createUsageTables(sheet, accessLoad, index, confAccessLoad);
                                    dataLoadDisplayed.push(accessLoad.dimensionValue);
                                }

                                angular.forEach(refComElementLoad, function(loadRefcom) {

                                    if (!loadRefcom.monthlyHistory || !loadRefcom.monthlyHistory.length > 0 || !loadRefcom.applicability.applicable) {
                                        return;
                                    }

                                    let existingAccess = _.find(SCCRDataVolume.dimension.values, (value) => {
                                        return value.dimensionValue === loadRefcom.dimensionValue;
                                    });

                                    if (!existingAccess && dataLoadDisplayed.indexOf(loadRefcom.dimensionValue) === -1) {
                                        sheet.cell("A" + index).value(loadRefcom.dimensionValue).style("bold", true);
                                        index = index + 2;
                                        sheet.cell("A" + index).value(loadAccessRef).style("bold", true);
                                        index = index + 2;
                                        index = createUsageTables(sheet, loadRefcom, index, confAccessLoad);
                                        dataLoadDisplayed.push(loadRefcom.dimensionValue);
                                    }
                                });
                            }
                        }
                        dataLoadDisplayed.push(element.dimensionValue);
                    });
                }

                if (SCCRDataLoad && SCCRDataLoad.dimension && SCCRDataLoad.dimension.values) {
                    SCCRDataLoad.dimension.values = $filter('orderBy')(SCCRDataLoad.dimension.values, ['dimensionGroup', 'dimensionValue']);
                    SCCRDataLoad.dimension.values.forEach(elementLoad => {
                        if (dataLoadDisplayed.indexOf(elementLoad.dimensionValue) === -1) {

                            if (displayedRefcom.indexOf(elementLoad.dimensionGroup) < 0) {
                                sheet.cell("A" + index).value(oraI18n.oraI18nLabels.accesRefLong.replace('{refcom}', elementLoad.dimensionGroup)).style("bold", true);
                                index = index + 2;
                                displayedRefcom.push(elementLoad.dimensionGroup);
                            }
                            sheet.cell("A" + index).value(elementLoad.dimensionValue).style("bold", true);
                            index = index + 2;
                            sheet.cell("A" + index).value(loadAccessRef).style("bold", true);
                            index = index + 2;
                            index = createUsageTables(sheet, elementLoad, index, confAccessLoad);
                        }
                    });
                }
            }

            scope.qosSheet = (workbook, globalVpnInternetAvailabilityRate,
                globalEthernetAvailabilityRate, accessAvailabilityRate, GTTRFulfilment, proactivityRate,
                MSIFulfilment, perfoRoundTripDelay, perfoPacketLossRate, perfoJitter, qosSection) => {
                const sheet = workbook.addSheet(qosSection.label);
               // const globalGar = oraI18n.oraI18nLabels.globalGar;
                const vpnInternet = oraI18n.oraI18nLabels.vpnInternet;
                const ethernet = oraI18n.oraI18nLabels.ethernet;
                const tresholdValue = oraI18n.oraI18nLabels.tresholdValue
                const contractualTresholdValue = oraI18n.oraI18nLabels.contractualTresholdValue;
               // const mesure = oraI18n.oraI18nLabels.mesure;
                //const pourcentGtrAchieve = oraI18n.oraI18nLabels.pourcentGtrAchieve;
                //const gtrBreach = oraI18n.oraI18nLabels.gtrBreach;
                //const gtrAchieve = oraI18n.oraI18nLabels.gtrAchieve;
                // const pourcentImsAchieve = oraI18n.oraI18nLabels.pourcentImsAchieve;
                //const imsBreach = oraI18n.oraI18nLabels.imsBreach;
                // const imsAchieve = oraI18n.oraI18nLabels.imsAchieve;
               // const accessNumber = oraI18n.oraI18nLabels.accessNumber;
                const paiCustRespThrLabel = oraI18n.oraI18nLabels.paiCustRespThrLabel;
                const performanceLong = oraI18n.oraI18nLabels.performanceLong;
                const perfoRoutesBreach = oraI18n.oraI18nLabels.perfoRoutesBreach;
                const perfoTotalRoutes = oraI18n.oraI18nLabels.perfoTotalRoutes;
                //const qosPerfAchieveRatio = oraI18n.oraI18nLabels.qosPerfAchieveRatio;
                //const qosPerfBreachNb = oraI18n.oraI18nLabels.qosPerfBreachNb;
                //const qosPerfRoutesNb = oraI18n.oraI18nLabels.qosPerfRoutesNb;
                //const gtrTitle = oraI18n.oraI18nLabels.gtrTitle;
                const perfDetTitle = oraI18n.oraI18nLabels.perfDetTitle;
                let index = 3;

                sheet.column('A').width(25);
                sheet.column('B').width(35);
                sheet.column('C').width(35);
                sheet.column('D').width(35);
                sheet.column('E').width(35);
                sheet.column('F').width(35);
                sheet.column('G').width(35);
                sheet.column('H').width(35);
                sheet.column('I').width(35);
                sheet.column('J').width(35);
                sheet.column('K').width(35);
                sheet.column('L').width(35);
                // First line
                addInformationLine(sheet);
                if (globalVpnInternetAvailabilityRate) {
                    
                    sheet.cell("A" + index).value(globalVpnInternetAvailabilityRate.longLabel).style("bold", true);
                    let evolution = getEvolutionSign(globalVpnInternetAvailabilityRate);
                    sheet.cell("B" + index).value(evolution.value).style("fontColor", evolution.color);

                    index = index + 2;

                    // First table
                    sheet.cell("B" + index).value(vpnInternet).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(contractualTresholdValue).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(globalVpnInternetAvailabilityRate.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    let values = globalVpnInternetAvailabilityRate.annualHistory ? globalVpnInternetAvailabilityRate.annualHistory : globalVpnInternetAvailabilityRate.monthlyHistory;

                    // Sort the array by date
                    values.sort(function(a, b) {
                        let aValue = globalVpnInternetAvailabilityRate.annualHistory ? a.year : a.month;
                        let bValue = globalVpnInternetAvailabilityRate.annualHistory ? b.year : b.month;
                        return new Date(aValue) - new Date(bValue);
                    });

                    values.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {
                            sheet.cell("B" + (index)).value($filter('date')(new Date(globalVpnInternetAvailabilityRate.annualHistory ? element.year : element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.contractualThreshold != null ? element.contractualThreshold + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    let conf = [{
                        metricName: 'globalVpnInternetAvailabilityRate',
                        type: 'contractualThreshold',
                        dataFrom: 0,
                        unit: '%',
                        label: tresholdValue
                    }, {
                        metricName: 'globalVpnInternetAvailabilityRate',
                        typeOfData: 'indicator',
                        dataFrom: 0,
                        unit: '%',
                        label: 'Mesure'
                    }]

                    index = createThirteenMonthTable(sheet, [globalVpnInternetAvailabilityRate], conf, index, {
                        fill: "4BB4E6",
                        fontColor: "000000"
                    }, 1);

                    index = index + 2;
                }
                if (globalEthernetAvailabilityRate) {
                    sheet.cell("A" + index).value(globalEthernetAvailabilityRate.longLabel).style("bold", true);
                    let evolution = getEvolutionSign(globalEthernetAvailabilityRate);
                    sheet.cell("B" + index).value(evolution.value).style("fontColor", evolution.color);

                    index = index + 2;
                    // Second table
                    sheet.cell("B" + index).value(ethernet).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(contractualTresholdValue).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(globalEthernetAvailabilityRate.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    let values = globalEthernetAvailabilityRate.annualHistory ? globalEthernetAvailabilityRate.annualHistory : globalEthernetAvailabilityRate.monthlyHistory;

                    // Sort the array by date
                    values.sort(function(a, b) {
                        let aValue = globalEthernetAvailabilityRate.annualHistory ? a.year : a.month;
                        let bValue = globalEthernetAvailabilityRate.annualHistory ? b.year : b.month;
                        return new Date(aValue) - new Date(bValue);
                    });

                    values.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {
                            sheet.cell("B" + (index)).value($filter('date')(new Date(globalEthernetAvailabilityRate.annualHistory ? element.year : element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.contractualThreshold != null ? element.contractualThreshold + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    let conf = [{
                        metricName: 'globalEthernetAvailabilityRate',
                        type: 'contractualThreshold',
                        dataFrom: 0,
                        unit: '%',
                        label: tresholdValue
                    }, {
                        metricName: 'globalEthernetAvailabilityRate',
                        typeOfData: 'indicator',
                        dataFrom: 0,
                        unit: '%',
                        label: 'Mesure'
                    }]

                    index = createThirteenMonthTable(sheet, [globalEthernetAvailabilityRate], conf, index, {
                        fill: "4BB4E6",
                        fontColor: "000000"
                    }, 1);

                    index = index + 1;
                }

                if (accessAvailabilityRate) {
                    sheet.cell("A" + index).value(accessAvailabilityRate.longLabel).style("bold", true);
                    const accessExceeding = _.find(accessAvailabilityRate.metricsDefinitions, (s) => {
                        return s.name === 'accessExceedingAAR';
                    });
                    const totalAccess = _.find(accessAvailabilityRate.metricsDefinitions, (s) => {
                        return s.name === 'totalAccessAAR';
                    });
                    index = index + 2;

                    sheet.cell("B" + index).value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(accessAvailabilityRate.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true).style("wrapText", true);
                    sheet.cell("D" + index).value(accessExceeding.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true).style("wrapText", true);
                    sheet.cell("E" + index).value(totalAccess.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    // Sort the array by date
                    accessAvailabilityRate.monthlyHistory.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    accessAvailabilityRate.monthlyHistory.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {

                            const accessExceedingAAR = _.find(element.metrics, (s) => {
                                return s.name === 'accessExceedingAAR';
                            });

                            const totalAccessAAR = _.find(element.metrics, (s) => {
                                return s.name === 'totalAccessAAR';
                            });

                            sheet.cell("B" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(accessExceedingAAR && accessExceedingAAR.measure != null ? accessExceedingAAR.measure : 'NA').style("border", true);
                            sheet.cell("E" + (index)).value(totalAccessAAR && totalAccessAAR.measure != null ? totalAccessAAR.measure : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    index = index + 1;

                    // GTR detail table
                    if (accessAvailabilityRate.onPeriod && accessAvailabilityRate.onPeriod.information && accessAvailabilityRate.onPeriod.information[0]) {
                        const rowKeys = ['monthName', 'siteName', 'serviceRefCom', 'serviceLevel', 'AARMeasure'];
                        sheet.cell("B" + index).value(accessAvailabilityRate.onPeriod.information[0].label).style("bold", true);
                        index = index + 1;

                        // Sort the array by date
                        accessAvailabilityRate.onPeriod.information[0].rows.sort(function(a, b) {
                            if (a.type !== 'title' && b.type !== 'title') {
                                return new Date(a.cells[0].value) - new Date(b.cells[0].value);
                            } else {
                                return 0;
                            }
                        });

                        accessAvailabilityRate.onPeriod.information[0].rows.forEach((row, i) => {
                            rowKeys.forEach((rowKey, j) => {

                                let result = _.find(row.cells, (s) => {
                                    return s.key === rowKey;
                                });

                                if (result && result.value) {
                                    let unit = '';
                                    if (i == 0) {
                                        sheet.cell((columnName[j + 1]) + index).value(result.value).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                                    } else {
                                        sheet.cell((columnName[j + 1]) + index).value(result.value + unit).style("border", true);
                                    }
                                }
                            });

                            index = index + 1;
                        });

                        index = index + 1;
                    }
                }

                if (GTTRFulfilment) {

                    sheet.cell("A" + index).value(GTTRFulfilment.longLabel).style("bold", true);
                    let evolution = getEvolutionSign(GTTRFulfilment);
                    sheet.cell("B" + index).value(evolution.value).style("fontColor", evolution.color);

                    index = index + 2;

                    const gtrBreach = _.find(GTTRFulfilment.metricsDefinitions, (s) => {
                        return s.name === 'incidentsExceedingGTTR';
                    });
                    const gtrAchieve = _.find(GTTRFulfilment.metricsDefinitions, (s) => {
                        return s.name === 'incidentsRespectingGTTR';
                    });
                    sheet.cell("B" + index).value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(GTTRFulfilment.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(gtrBreach.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("E" + index).value(gtrAchieve.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    // Sort the array by date
                    GTTRFulfilment.monthlyHistory.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    GTTRFulfilment.monthlyHistory.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {

                            const incidentsRespectingGTTR = _.find(element.metrics, (s) => {
                                return s.name === 'incidentsRespectingGTTR';
                            });

                            const incidentsExceedingGTTR = _.find(element.metrics, (s) => {
                                return s.name === 'incidentsExceedingGTTR';
                            });

                            sheet.cell("B" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(incidentsExceedingGTTR && incidentsExceedingGTTR.measure != null ? incidentsExceedingGTTR.measure : 'NA').style("border", true);
                            sheet.cell("E" + (index)).value(incidentsRespectingGTTR && incidentsRespectingGTTR.measure != null ? incidentsRespectingGTTR.measure : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    let conf = [{
                        metricName: 'incidentsRespectingGTTR',
                        typeOfData: 'metric',
                        dataFrom: 0
                    }, {
                        metricName: 'incidentsExceedingGTTR',
                        typeOfData: 'metric',
                        dataFrom: 0
                    }]

                    index = createThirteenMonthTable(sheet, [GTTRFulfilment], conf, index, {
                        fill: "4BB4E6",
                        fontColor: "000000"
                    }, 1);

                    index = index + 1;
                }

                if (GTTRFulfilment && hasInformationHref(GTTRFulfilment)) {
                    // GTR detail table
                    index = createGTRIMSdetailsTable(GTTRFulfilment, sheet, index, gtrService);
                    index = index + 1;
                }

                if (proactivityRate) {
                    // Taux de proactivité

                    sheet.cell("A" + index).value(proactivityRate.longLabel).style("bold", true);
                    let evolution = getEvolutionSign(proactivityRate);
                    sheet.cell("B" + index).value(evolution.value).style("fontColor", evolution.color);

                    index = index + 2;
                    const accessNumber = _.find(proactivityRate.metricsDefinitions, (s) => {
                        return s.name === 'totalAccessNumber';
                    });
                    sheet.cell("B" + index).value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(contractualTresholdValue).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(proactivityRate.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("E" + index).value(accessNumber.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    // Sort the array by date
                    proactivityRate.monthlyHistory.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    proactivityRate.monthlyHistory.forEach((element, i) => {
                        if (element && element.onPeriod ) {

                            const totalAccessNumber = _.find(element.metrics, (s) => {
                                return s.name === 'totalAccessNumber';
                            });

                            sheet.cell("B" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element.contractualThreshold ? element.contractualThreshold + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(element && element.measure != null && element.applicability.applicable ? element.measure + '%' : 'NA').style("border", true);
                            sheet.cell("E" + (index)).value(totalAccessNumber && totalAccessNumber.measure != null ? totalAccessNumber.measure : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    let conf = [{
                        metricName: 'proactivityRate',
                        type: 'contractualThreshold',
                        dataFrom: 0,
                        unit: '%',
                        label: tresholdValue
                    }, {
                        metricName: 'proactivityRate',
                        typeOfData: 'indicator',
                        dataFrom: 0
                    }, {
                        metricName: 'clientResponsibilityRate',
                        typeOfData: 'metric',
                        value: 20,
                        unit: '%',
                        dataFrom: 0,
                        label: paiCustRespThrLabel
                    }, {
                        metricName: 'clientResponsibilityRate',
                        typeOfData: 'metric',
                        unit: '%',
                        dataFrom: 0
                    }]

                    index = createThirteenMonthTable(sheet, [proactivityRate], conf, index, {
                        fill: "4BB4E6",
                        fontColor: "000000"
                    }, 1, true);

                    index = index + 2;
                }

                if (MSIFulfilment) {
                    // Interruption maximale de service
                    sheet.cell("A" + index).value(MSIFulfilment.longLabel).style("bold", true);
                    let evolution = getEvolutionSign(MSIFulfilment);
                    sheet.cell("B" + index).value(evolution.value).style("fontColor", evolution.color);

                    index = index + 2;
                    const imsBreach = _.find(MSIFulfilment.metricsDefinitions, (s) => {
                        return s.name === 'sitesExceedingMSI';
                    });
                    const  imsAchieve = _.find(MSIFulfilment.metricsDefinitions, (s) => {
                        return s.name === 'sitesRespectingMSI';
                    });
                    sheet.cell("B" + index).value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(MSIFulfilment.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(imsBreach.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("E" + index).value(imsAchieve.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    // Sort the array by date
                    MSIFulfilment.monthlyHistory.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    MSIFulfilment.monthlyHistory.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {

                            const sitesExceedingMSI = _.find(element.metrics, (s) => {
                                return s.name === 'sitesExceedingMSI';
                            });

                            const sitesRespectingMSI = _.find(element.metrics, (s) => {
                                return s.name === 'sitesRespectingMSI';
                            });

                            sheet.cell("B" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(sitesExceedingMSI && sitesExceedingMSI.measure != null ? sitesExceedingMSI.measure : 'NA').style("border", true);
                            sheet.cell("E" + (index)).value(sitesRespectingMSI && sitesRespectingMSI.measure != null ? sitesRespectingMSI.measure : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    let conf = [{
                        metricName: 'sitesRespectingMSI',
                        type: 'metric',
                        dataFrom: 0
                    }, {
                        metricName: 'sitesExceedingMSI',
                        typeOfData: 'metric',
                        dataFrom: 0
                    }]

                    index = createThirteenMonthTable(sheet, [MSIFulfilment], conf, index, {
                        fill: "4BB4E6",
                        fontColor: "000000"
                    }, 1);

                    index = index + 1;
                }

                if (MSIFulfilment && hasInformationHref(MSIFulfilment) ) {
                    // IMS detail table
                    index = createGTRIMSdetailsTable(MSIFulfilment, sheet, index, ims);
                }

                if (perfoRoundTripDelay) {
                    sheet.cell("A" + index).value(performanceLong).style("bold", true);
                    index = index + 2;
                    sheet.cell("B" + index).value(perfoRoundTripDelay.longLabel).style("bold", true);

                    index = index + 1;
                    sheet.cell("B" + index).value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(perfoRoundTripDelay.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(perfoRoutesBreach).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("E" + index).value(perfoTotalRoutes).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;
                    perfoRoundTripDelay.monthlyHistory.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    perfoRoundTripDelay.monthlyHistory.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {

                            const breachedRoutesNumber = _.find(element.metrics, (s) => {
                                return s.name === 'breachedRoutesNumber';
                            });

                            const totalRoutesNumber = _.find(element.metrics, (s) => {
                                return s.name === 'totalRoutesNumber';
                            });

                            sheet.cell("B" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(breachedRoutesNumber && breachedRoutesNumber.measure != null ? breachedRoutesNumber.measure : 'NA').style("border", true);
                            sheet.cell("E" + (index)).value(totalRoutesNumber && totalRoutesNumber.measure != null ? totalRoutesNumber.measure : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    index = index + 1;
                }

                if (perfoPacketLossRate) {
                    sheet.cell("B" + index).value(perfoPacketLossRate.longLabel).style("bold", true);

                    index = index + 1;
                    sheet.cell("B" + index).value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(perfoPacketLossRate.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(perfoRoutesBreach).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("E" + index).value(perfoTotalRoutes).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;
                    perfoPacketLossRate.monthlyHistory.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    perfoPacketLossRate.monthlyHistory.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {

                            const breachedRoutesNumber = _.find(element.metrics, (s) => {
                                return s.name === 'breachedRoutesNumber';
                            });

                            const totalRoutesNumber = _.find(element.metrics, (s) => {
                                return s.name === 'totalRoutesNumber';
                            });

                            sheet.cell("B" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(breachedRoutesNumber && breachedRoutesNumber.measure != null ? breachedRoutesNumber.measure : 'NA').style("border", true);
                            sheet.cell("E" + (index)).value(totalRoutesNumber && totalRoutesNumber.measure != null ? totalRoutesNumber.measure : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    index = index + 1;
                }

                if (perfoJitter) {
                    sheet.cell("B" + index).value(perfoJitter.longLabel).style("bold", true);

                    index = index + 1;
                    sheet.cell("B" + index).value('').style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("C" + index).value(perfoJitter.shortLabel).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("D" + index).value(perfoRoutesBreach).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("E" + index).value(perfoTotalRoutes).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;
                    perfoJitter.monthlyHistory.sort(function(a, b) {
                        return new Date(a.month) - new Date(b.month);
                    });

                    perfoJitter.monthlyHistory.forEach((element, i) => {
                        if (element && element.onPeriod && element.applicability &&
                            element.applicability.applicable) {
                            const breachedRoutesNumber = _.find(element.metrics, (s) => {
                                return s.name === 'breachedRoutesNumber';
                            });

                            const totalRoutesNumber = _.find(element.metrics, (s) => {
                                return s.name === 'totalRoutesNumber';
                            });

                            sheet.cell("B" + (index)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            sheet.cell("C" + (index)).value(element && element.measure != null ? element.measure + '%' : 'NA').style("border", true);
                            sheet.cell("D" + (index)).value(breachedRoutesNumber && breachedRoutesNumber.measure != null ? breachedRoutesNumber.measure : 'NA').style("border", true);
                            sheet.cell("E" + (index)).value(totalRoutesNumber && totalRoutesNumber.measure != null ? totalRoutesNumber.measure : 'NA').style("border", true);
                            index = index + 1;
                        }
                    });

                    index = index + 1;
                }

                if (performanceNew.tableData && performanceNew.tableData.length > 0) {
                    let threshold = oraI18n.oraI18nLabels.PERF_DET_THRESHOLD_SHORTLABEL;
                    let measure = oraI18n.oraI18nLabels.PERF_DET_MEASURE_SHORTLABEL;

                    sheet.cell("B" + index).value(perfDetTitle).style("bold", true);

                    index = index + 1;
                    // Header
                    sheet.range("B" + index + ":B" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_MONTH_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.range("C" + index + ":C" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_ENDPOINTA_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.range("D" + index + ":D" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_ENDPOINTB_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.range("E" + index + ":E" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_SERVICECLASS_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.range("F" + index + ":F" + (index + 1)).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_SAMPLERATE_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.range("G" + index + ":H" + index).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_ROUNDTRIPDELAY_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.range("I" + index + ":J" + index).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_PACKETLOSSRATE_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.range("K" + index + ":L" + index).merged(true).value(oraI18n.oraI18nLabels.PERF_DET_JITTER_SHORTLABEL).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    sheet.cell("G" + index).value(threshold).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("H" + index).value(measure).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("I" + index).value(threshold).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("J" + index).value(measure).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("K" + index).value(threshold).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                    sheet.cell("L" + index).value(measure).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);

                    index = index + 1;

                    
                   // if (tableData[0] && tableData[0].length > 0) {

                        // Sort the array by date
                        performanceNew.tableData.sort(function(a, b) {
                            return new Date(a.month) - new Date(b.month);
                        });

                        performanceNew.tableData.forEach((row) => {
                            let roundTripDelayTreshold = row.roundTripDelay && row.roundTripDelay.threshold != undefined ? row.roundTripDelay.threshold : 'NA';
                            let packetLossRateTreshold = row.packetLossRate && row.packetLossRate.threshold != undefined ? row.packetLossRate.threshold : 'NA';
                            let jitterTreshold = row.jitter && row.jitter.threshold != undefined ? row.jitter.threshold : 'NA';

                            let roundTripDelayMeasure = row.roundTripDelay && row.roundTripDelay.measure != undefined ? row.roundTripDelay.measure : 'NA';
                            let packetLossRateMeasure = row.packetLossRate && row.packetLossRate.measure != undefined ? row.packetLossRate.measure : 'NA';
                            let jitterMeasure = row.jitter && row.jitter.measure != undefined ? row.jitter.measure : 'NA';

                            sheet.cell("B" + index).value($filter('date')(new Date(row.month), "MMMM", '+0')).style("horizontalAlignment", "left").style("border", true);
                            sheet.cell("C" + index).value(row.path && row.path.endpointA ? row.path.endpointA : 'NA').style("horizontalAlignment", "left").style("border", true);
                            sheet.cell("D" + index).value(row.path && row.path.endpointB ? row.path.endpointB : 'NA').style("horizontalAlignment", "left").style("border", true);
                            sheet.cell("E" + index).value(row.serviceClass ? row.serviceClass : 'NA').style("horizontalAlignment", "left").style("border", true);
                            sheet.cell("F" + index).value(row.sampleRate != undefined ? row.sampleRate : 'NA').style("horizontalAlignment", "right").style("border", true);
                            sheet.cell("G" + index).value(roundTripDelayTreshold).style("horizontalAlignment", "right").style("border", true);
                            sheet.cell("H" + index).value(roundTripDelayMeasure).style("horizontalAlignment", "right").style("border", true);
                            sheet.cell("I" + index).value(packetLossRateTreshold).style("horizontalAlignment", "right").style("border", true);
                            sheet.cell("J" + index).value(packetLossRateMeasure).style("horizontalAlignment", "right").style("border", true);
                            sheet.cell("K" + index).value(jitterTreshold).style("horizontalAlignment", "right").style("border", true);
                            sheet.cell("L" + index).value(jitterMeasure).style("horizontalAlignment", "right").style("border", true);
                            index = index + 1;
                        });
                    //}
                    index = index + 1;

                }
            }

            function getEvolutionSign(indicator) {
                let evolution = {
                    value: '=',
                    color: '000000'
                };
                if (indicator.onPeriod.evolution > 0) {
                    evolution.value = '↗';
                    evolution.color = '008000';

                } else if (indicator.onPeriod.evolution < 0) {
                    evolution.value = '↘';
                    evolution.color = 'FF0000';
                }

                return evolution;
            }

            function createUsageTables(sheet, indicators, indexRow, conf) {

                const usageSentLabel = oraI18n.oraI18nLabels.usageSentLabel;
                const usageReceivedLabel = oraI18n.oraI18nLabels.usageReceivedLabel;

                // First table header
                sheet.cell("B" + indexRow).value('').style("fill", "A885D8").style("border", true);
                sheet.cell("C" + indexRow).value(usageSentLabel).style("bold", true).style("horizontalAlignment", "center").style("fontColor", "ffffff").style("fill", "A885D8").style("border", true);
                sheet.cell("D" + indexRow).value(usageReceivedLabel).style("bold", true).style("horizontalAlignment", "center").style("fontColor", "ffffff").style("fill", "A885D8").style("border", true);

                indexRow = indexRow + 1;

                // Sort the array by date
                indicators.monthlyHistory.sort(function(a, b) {
                    return new Date(a.month) - new Date(b.month);
                });

                indicators.monthlyHistory.forEach((element) => {
                    if (element && element.onPeriod) {
                        sheet.cell("B" + (indexRow)).value($filter('date')(new Date(element.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");

                        let inVolume = _.find(element.metrics, (s) => {
                            return s.name === conf[1].metricName;
                        });

                        let outVolume = _.find(element.metrics, (s) => {
                            return s.name === conf[0].metricName;
                        });

                        if (!element.applicability ||
                            !element.applicability.applicable) {
                            outVolume = null;
                            inVolume = null;
                        }

                        let unit1 = conf[0].unit ? conf[0].unit : '';
                        let unit2 = conf[1].unit ? conf[1].unit : '';
                        sheet.cell("C" + (indexRow)).value((outVolume && outVolume.measure != null ? conf[0].filter ? $filter(conf[0].filter)(outVolume.measure) : outVolume.measure : 'NA') + unit1).style("border", true);
                        sheet.cell("D" + (indexRow)).value((inVolume && inVolume.measure != null ? conf[1].filter ? $filter(conf[1].filter)(inVolume.measure) : inVolume.measure : 'NA') + unit2).style("border", true);
                        indexRow = indexRow + 1;
                    }
                });

                indexRow = createThirteenMonthTable(sheet, [indicators], conf, indexRow, {
                    fill: "A885D8",
                    fontColor: "ffffff"
                }, 1);

                return indexRow + 1;
            }

            function addInformationLine(sheet) {
                const dateToDisplay = $filter('sccvPeriodDisplay')(reports.report, true);
                // Information line
                sheet.cell("A1").value(perimeter.selectedPerimeter.label).style("bold", true);
                sheet.cell("D1").value(dateToDisplay).style("horizontalAlignment", "left").style("bold", true);
            }

            /**
             * @ngdoc method
             * @name createThirteenMonthTable
             * @methodOf ora-gui-core.directive:ora-export-to-excel
             * @description
             * Create the 13 month table
             */
            function createThirteenMonthTable(sheet, indicators, metricsName, indexRow, colors, indexColumn, displayAll) {

                indexRow = indexRow + 1;
                // Add label
                sheet.cell(columnName[indexColumn] + (indexRow)).value(oraI18n.oraI18nLabels.excelEvolution).style("bold", true);
                indexRow = indexRow + 1;

                // Create the header of the table
                sheet.cell(columnName[indexColumn] + (indexRow)).value('').style("bold", true).style("fontColor", colors.fontColor).style("horizontalAlignment", "center").style("fill", colors.fill).style("border", true);
                metricsName.forEach((element, i) => {
                    let label = '';
                    if (element.label) {
                        label = element.label;
                    } else if (element.typeOfData === 'indicator') {
                        label = indicators[element.dataFrom].shortLabel;
                    } else if (element.typeOfData === 'dimension') {
                        label = element.label;
                    } else {
                        const metricDefinition = _.find(indicators[element.dataFrom].metricsDefinitions, (s) => {
                            return s.name === element.metricName;
                        });

                        label = metricDefinition ? metricDefinition.shortLabel : '';

                    }
                    sheet.cell(columnName[i + indexColumn + 1] + (indexRow)).value(label).style("fontColor", colors.fontColor).style("bold", true).style("horizontalAlignment", "center").style("fill", colors.fill).style("border", true);
                });

                indexRow = indexRow + 1;
                let newIndex = indexRow;
                // Add value rows
                metricsName.forEach((element, k) => {
                    newIndex = indexRow;
                    if (indicators[element.dataFrom].annualHistory) {
                        indicators[element.dataFrom].annualHistory.forEach((year) => {
                            sheet.cell(columnName[indexColumn] + newIndex).value($filter('date')(new Date(year.year), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            let measure = 'NA';
                            let unit = element.unit ? element.unit : '';

                            if (element.value) {
                                measure = element.filter ? $filter(element.filter)(element.value) + unit : element.value + unit;
                            } else if (element.type === 'contractualThreshold') {
                                if (year.contractualThreshold != null) {
                                    measure = year.contractualThreshold + unit;
                                }
                            } else if (year.applicability &&
                                year.applicability.applicable) {
                                if (year.measure != null) {
                                    measure = element.filter ? $filter(element.filter)(year.measure) + unit : year.measure + unit;
                                }
                            }

                            sheet.cell(columnName[k + indexColumn + 1] + newIndex).value(measure).style("border", true).style("horizontalAlignment", "left");
                            newIndex = newIndex + 1;
                        });
                    } else {
                        indicators[element.dataFrom].monthlyHistory.forEach((month) => {
                            sheet.cell(columnName[indexColumn] + newIndex).value($filter('date')(new Date(month.month), "MMMM yyyy", '+0')).style("border", true).style("horizontalAlignment", "left");
                            let measure = 'NA';
                            let unit = element.unit ? element.unit : '';

                            if (month.applicability &&
                                month.applicability.applicable || displayAll) {

                                if (element.value) {
                                    measure = element.filter ? $filter(element.filter)(element.value) + unit : element.value + unit;
                                } else if (element.typeOfData === 'indicator') {
                                    if (month.measure != null) {
                                        measure = element.filter ? $filter(element.filter)(month.measure) + unit : month.measure + unit;
                                    }
                                } else if (element.type === 'contractualThreshold') {
                                    if (month.contractualThreshold != null) {
                                        measure = month.contractualThreshold + unit;
                                    }
                                } else {
                                    const metric = _.find(month.metrics, (s) => {
                                        return s.name === element.metricName;
                                    });
                                    if (metric && metric.measure != null) {
                                        measure = element.filter ? $filter(element.filter)(metric.measure) + unit : metric.measure + unit;
                                    }
                                }
                            }

                            sheet.cell(columnName[k + indexColumn + 1] + newIndex).value(measure).style("border", true).style("horizontalAlignment", "left");
                            newIndex = newIndex + 1;
                        });
                    }
                });

                return newIndex;
            }

            // Generate excel file with blob method
            scope.generateBlob = (type) => {
                let generate = (type === excelConst.MSCCR) ? scope.generateMSccr : scope.generatePerformance; 
                return generate()
                    .then(function(blob) {
                        if (!$$REDIRECTION_DISABLED && blob) {

                            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                                window.navigator.msSaveOrOpenBlob(blob, "out.xlsx");
                            } else {
                                var url = window.URL.createObjectURL(blob);
                                var a = document.createElement("a");
                                document.body.appendChild(a);
                                a.href = url;
                                 const name = type +'_period_' ;
                                a.download = name + reports.report.id + "_export.xlsx";
                                a.click();
                                window.URL.revokeObjectURL(url);
                                document.body.removeChild(a);
                            }
                        }
                        return blob;
                    })
                    .catch(function(err) {
                        throw err;
                    });
            }

            // Generate excel file with base 64 method
            scope.generateBase64 = (type) => {
                 let generate =  (type === excelConst.MSCCR) ? scope.generateMSccr : scope.generatePerformance; 
                generate("base64")
                    .then(function(base64) {
                        if (!$$REDIRECTION_DISABLED && base64) {
                            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                                throw new Error("Navigating to data URI is not supported in IE.");
                            } else {
                                 var link = document.createElement('a');
                                 const name = type +'_period_' ;
                                link.download = name + reports.report.id + "_export.xlsx";
                                link.href = "data:" + XlsxPopulate.MIME_TYPE + ";base64," + base64;
                                link.click();
                            }
                        }
                        return base64;
                    })
                    .catch(function(err) {
                        throw err;
                    });
            }

            // Decode html encoded characters
            scope.decodeStr = (str) => {
                if (str) {
                    return str.replace(/&#(\d+);/g, function(match, dec) {
                        return String.fromCharCode(dec);
                    });
                } else {
                    return '';
                }
            }

        function buildRoW(row){
            let res = {};
            let rowKeys = ['siteName', 'accessName', 'overrunNumber', 'averageDuration', 'maximumLoadRate', 'way','duration'];
            angular.forEach(rowKeys, function(rowKey){
                let cel = _.find(row.cells, (s) => {
                            return s.key === rowKey;
                        });
                res[rowKey] = cel.value;
            })
            return res;
        }

        function createGTRIMSdetailsTable(GTRMSIFulfilment, sheet, index, service){
            let rows = service.detailsData;
                if (rows.length > 0) {
                    sheet.cell("B" + index).value(GTRMSIFulfilment.onPeriod.information[0].label).style("bold", true);
                    index = index + 1;
                    rows.forEach((row, i) => {
                        let column = 1;
                        row.forEach((val) => {
                            if (i == 0) {
                                sheet.cell((columnName[column]) + index).value(val).style("bold", true).style("horizontalAlignment", "center").style("fill", "4BB4E6").style("border", true);
                            } else {

                                sheet.cell((columnName[column]) + index).value(val).style("border", true);
                            }
                            column = column + 1;
                        });

                        index = index + 1;
                    });

                    index = index + 1;
                }
                
                return index;
               
        }
        
        function hasInformationHref (indicator){
            return indicator.onPeriod && indicator.onPeriod.information &&
            indicator.onPeriod.information[0] && indicator.onPeriod.information[0].href;
        }

        }
    }
};

export default oraExportToExcel;
