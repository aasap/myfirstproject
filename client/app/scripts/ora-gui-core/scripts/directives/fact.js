'use strict'; //NOSONAR
import FactView from '../../views/fact.html';
/**
 * @ngdoc directive
 * @name ora-gui-sccv.directive:oraFact
 */

const oraFact = function(reports) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: FactView,
        link: function(scope) {
            scope.comment = {};
            scope.fact = {
                backgroundclass: 'bg-white',
                borderclass: 'border-color'
            };
        }
    };
};

export default oraFact;