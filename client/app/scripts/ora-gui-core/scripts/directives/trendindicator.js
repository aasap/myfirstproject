import trendIndicatorView from '../../views/trendIndicator.html';

/**
 * @ngdoc directive
 * @name ora-internal-gui.directive:trendIndicator
 * @description
 * # trendIndicator
 */
const trendIndicator = function(oraI18n) {
    'ngInject';

    return {
        templateUrl: trendIndicatorView,
        restrict: 'E',
        scope: {
            tooltips: '=tooltips',
            score: '=data'
        },
        replace: true,
        link: function(scope) {

            scope.getTooltipTitle = () => {
                if (scope.score === 0 || scope.score === null) {
                    scope.classIcon = 'glyphicon-triangle-right';
                    scope.tooltipTitle = 'GLOBAL_NO_CHANGE_LONGLABEL';
                } else if (scope.score > 0) {
                    scope.classIcon = 'glyphicon-triangle-top';
                    scope.tooltipTitle = 'GLOBAL_INCREASE_LONGLABEL';
                } else if (scope.score < 0) {
                    scope.classIcon = 'glyphicon-triangle-bottom';
                    scope.tooltipTitle = 'GLOBAL_DECREASE_LONGLABEL';
                }
            }

            scope.$watch('score', function() {
                scope.getTooltipTitle();
            });

            scope.$watch(() => oraI18n.updateLabels, function() {
                scope.getTooltipTitle();
            });
        }
    };
};

export default trendIndicator;