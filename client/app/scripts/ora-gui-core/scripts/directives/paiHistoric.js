//'use strict'; //NOSONAR

/**
 * @ngdoc directive
 * @name ora-gui-core.directive:non-contractual-table
 */

const paiHistoric = function(histogram, utils,  $window) {
    'ngInject';
    return {
	restrict: 'E',
	template: '<div id="{{historicId}}" class="svg-container"></div>',
	scope: {
	    historicId: '=historicId',
	    indicator: '=indicator',
	    historicOptions: '=historicOptions',
	},
	link: function(scope) {
	    scope.applicable = false;
	    scope.metrics = [];

	    scope.$watch(
		    () => scope.indicator,
		    (value) => {
			scope.indicator = value;
			computeData();
			drawPAIHistogram();
		    }, true);

	    angular.element($window).bind('resize', resizeHistogram);

	    scope.$on('$destroy', function() {
		angular.element($window).unbind('resize', resizeHistogram);
	    });


	    function resizeHistogram() {
		    drawPAIHistogram();
	    }
	    function drawPAIHistogram() {
		histogram.paiHistogram(
			scope.historicId,
			scope.indicator,
			scope.period,
			scope.historicOptions);
	    }

	    function computeData(){
		let onPeriod = new Set();
		angular.forEach(scope.indicator.monthlyHistory, function (month){
		    if (month.onPeriod) {
			onPeriod.add(Date.parse(month.month));
		    } 
		});

		scope.period = {};
		onPeriod = Array.from(onPeriod);
		scope.period.to = utils.arrayMax(onPeriod);
		scope.period.from = utils.arrayMin(onPeriod);
	    }

	}
    };
};

export default paiHistoric;