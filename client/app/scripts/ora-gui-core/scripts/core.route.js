//VIEWS
import coreStructure from '../views/core.structure.html';

const coreRoute = function($stateProvider, $urlRouterProvider) {
    'ngInject';
    $urlRouterProvider.otherwise('/');

    function getUser(user) {
        return user.getUser().then(function(userLogged) {

            // Disable redirection for unit tests
            if ($$REDIRECTION_DISABLED) {
                return userLogged;
            }

            if (!user.userLogged.allianceCode) {
                window.location = './401.html';
            } 
            return userLogged;
        });
    }

    function initSubsAndPerim(perimeter, subscription, cdPerim, cdRefcom) {
        return perimeter.getPerimetersById(cdPerim).then( ()=> {
            return subscription.getSubscriptionContractByRfecom(cdRefcom);
        });
    }

    function LoadData2(user, perimeter, subscription, $stateParams) {
        return getUser(user).then(function() {
            return initSubsAndPerim(perimeter, subscription, $stateParams.cdPerimeter, $stateParams.cdRefcom);
        });
    }

    $stateProvider
        .state('ora', {
            url: '/?cdRefcom&cdPerimeter',
            views: {
                '@': {
                    title: 'main',
                    templateUrl: coreStructure,
                    controller: 'MainController',
                    resolve: {
                        LoadData2: LoadData2
                    }
                },
               
            }
        });

};

export default coreRoute;