export default class user {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.user
     * @requires ora-gui-core.rest
     * @requires ora-gui-core.$http
     * @description
     * # user
     * Service in the ora-gui-core.
     */

    constructor(oraRest, rest, appConfig, googleAnalytics) {
        'ngInject';

        /**
         * @ngdoc property
         * @name ora-gui-core.user.#baseUrl
         * @propertyOf ora-gui-core.user
         * @description
         * Url de base pour user
         */
        var baseUrl = appConfig.urlApi + rest.USER;

        var service = this;

        service.userLogged = {};

        /**
         * @ngdoc
         * @name ora-gui-core.user#getUser
         * @methodOf ora-gui-core.user
         *
         * @description
         * Get the information for the current user
         * @example
         * getUser(periodId).then(function(data){ };
         * @returns {HttpPromise} resolve with fetched data, or fails without error description.
         */
        service.getUser = function() {
            return oraRest.get(baseUrl).then(function(result) {
                service.userLogged = result.data;
                googleAnalytics.user = result.data;
                return service.userLogged;
            });
        };

        return service;

    }
}