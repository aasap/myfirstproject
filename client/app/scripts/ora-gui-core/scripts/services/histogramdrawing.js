export default class histogramdrawing {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.histogram
     * @description
     * # histogram
     * Service in the ora-gui-core.
     */

    constructor() {

        var service = this;

        function linePath(data, fn, histoValues) {
            var l = d3.svg.line()
                .defined(function(d) {
                    // -1 is missing value
                    return d && d.y && d.y === -1 ? null : d
                })
                .x(function(d, i) {
                    return !fn || !fn.x ? histoValues.range.x(d.x) : fn.x(d, i, histoValues);
                })
                .y(function(d, i) {
                    return !fn || !fn.y ? histoValues.range.y(d.y) : fn.y(d, i, histoValues);
                });
            return l(data);
        }

        function creationDivTooltip() {
            return d3.select('body')
                // declare the tooltip div
                .append('div')
                // apply the 'tooltip' class
                .attr('class', 'nvtooltip xy-tooltip nv-pointer-events-none')
                .style('opacity', 0);
        }

        function tooltipMouseOver(tooltip, d, date, label, value) {
            tooltip.transition()
                .duration(500)
                .style('opacity', 0);
            tooltip.transition()
                .duration(200)
                .style('opacity', 1);
            tooltip.html(
                    '<table>' +
                    '<thead><tr><td colspan="3"><strong class="x-value">' + date + '</strong></td></tr></thead>' +
                    '<tbody><tr><td class="legend-color-guide">' +
                    '<div class="' + d.bgClass + '"></div>' +
                    '</td><td class="key">' + label + '</td><td class="value">' + value + '</td></tr></tbody>' +
                    '</table>')
                .style('left', (d3.event.pageX - (tooltip[0][0].clientWidth)) + 'px')
                .style('top', (d3.event.pageY - (tooltip[0][0].clientHeight)) + 'px');


        }

        function tooltipMouseOut(tooltip) {
            tooltip.transition()
                .duration(500)
                .style('opacity', 0)
                .style('left', 0)
                .style('top', 0);

        }

        function calculebarYTranslation(d, histoValues) {
            return '-' + (d.start && d.start > 0 ? histoValues.dimension.height - histoValues.range.y(d.start) : 0);
        }

        function addTooltip(svgObject, tickValues, fnLabel, fnValue) {
            var tooltipObject = creationDivTooltip();
            svgObject
                .on('mouseover', function(d, i) {
                    //Add tooltip on mouseover
                    tooltipMouseOver(tooltipObject, d, tickValues[i], fnLabel(d), fnValue(d));
                })
                .on('mouseout', function() {
                    tooltipMouseOut(tooltipObject);
                });
            return svgObject;
        }

        service.creationHistogramFrame = function(histoValues, axis) {

            var dimension = histoValues.dimension;
            var width = dimension.width + dimension.margin.left + dimension.margin.right;
            var height = dimension.height + dimension.margin.top + dimension.margin.bottom;
            var fontSize = 1.1;
            var svg = d3.select('#' + histoValues.histogramId).append('svg')
                //.attr('preserveAspectRatio', 'xMinYMid meet')
                //.attr('viewBox', '0 0 ' + width + ' ' + height)
                .attr('class', 'svg-content')
                .attr('width', width)
                .attr('height', height)

            .attr('class', 'histo-svg')
                .append('svg:g')
                .attr('transform', 'translate(' + dimension.margin.left + ',' + dimension.margin.top + ')');

            svg.append('g')
                .attr('class', 'y axis')
                .call(axis.y);

            svg.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + dimension.height + ')')
                .call(axis.x)
                .selectAll('text')
                .attr('y', 9)
                .attr('x', 0)
                .attr('dy', '.35em')
                .style('text-anchor', 'end')
                .style('font-size', fontSize + 'rem')
                .attr('transform', 'rotate(-45)');

            return svg;
        };

        service.addPath = function(histoValues, svgObject, fn) {
            //Default lineFunction
            fn = !fn ? linePath : fn;
            svgObject.selectAll('g')
                .data(function(d) {
                    return d.value.types.indexOf('line') > -1 ? [d.value] : [];
                }).enter()
                .append('path')
                .attr('class', function(d) {
                    return d.stroke.class;
                })
                .style('stroke-width', function(d) {
                    return d.stroke.width;
                })
                .style('fill', function(d) { //NOSONAR
                    return 'none';
                })
                .attr('d', function(d) {
                    return fn(d.data, d.fn, histoValues);
                });

            return svgObject;
        };

        service.addBar = function(histoValues, svgObject, tooltip, fnLabel, fnValue) {
            svgObject = svgObject.selectAll('g')
                .data(function(d) {
                    return d.value.types.indexOf('bar') > -1 ? d.value.data : [];
                }).enter()
                .append('rect')
                .attr('x', function(d) {
                    return histoValues.range.x(d.x);
                })
                .attr('width', histoValues.range.z.rangeBand())
                .attr('class', function(d) {
                    return 'histo-rect ' + d.fill.class;
                })
                .attr('y', function(d) {
                    return histoValues.range.y(d.y);
                })
                .attr('transform', function(d) {
                    return 'translate(0,' + calculebarYTranslation(d, histoValues) + ')';
                })
                .attr('height', function(d) {
                    return (histoValues.dimension.height - histoValues.range.y(d.y)) === 0 ? d.y : histoValues.dimension.height - histoValues.range.y(d.y);
                });

            if (tooltip) {
                svgObject = addTooltip(svgObject, histoValues.tickValues, fnLabel, fnValue);
            }

            return svgObject;
        };

        service.addCircle = function(histoValues, svgObject) {
            var range = histoValues.range;
            svgObject.selectAll('g')
                .data(function(d) {
                    // circle or line
                    return d.value.types.indexOf('circle') > -1 ? d.value.data : [];
                })

            .enter()
                .append('circle')
                .attr('cx', function(d) {
                    // -1 missing value
                    if (d.y === -1) {
                        return null;
                    } else {
                        return range.x(d.x) + range.x.rangeBand() * 0.5;
                    }
                })
                .attr('cy', function(d) {
                    // -1 missing value
                    if (d.y === -1) {
                        return null;
                    }
                    return range.y(d.y);
                })
                .attr('r', function(d) {
                    // r of circle to 0 for hide if -1
                    if (d.y === -1) { return 0 }
                    return 4
                })
                .attr('class', function(d) {
                    return d.colorCircle;
                });
            return svgObject;
        };



        return service;
    }
}