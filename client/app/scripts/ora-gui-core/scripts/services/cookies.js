export default class cookieService {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.cookieService
     * @description
     * # cookieService
     * Service in the ora-gui-core.
     */

    constructor(oraRest, rest, appConfig, user) {
        'ngInject';

        var service = this;
        var baseUrl = appConfig.urlApi + rest.USER;
        /**
         * @ngdoc property
         * @name extraSeconds
         * @propertyOf ora-gui-core.cookieService
         * @description
         */
        service.extraSeconds = 15000;
        /**
         * @ngdoc property
         * @name SecondsByDay
         * @propertyOf ora-gui-core.cookieService
         * @description
         */
        service.SecondsByDay = 24 * 60 * 60 * 1000;
        /**
         * @ngdoc property
         * @name timeoutCheckSession
         * @propertyOf ora-gui-core.cookieService
         * @description
         */
        service.timeoutCheckSession = 10000;

        /**
         * @ngdoc method
         * @name setCookie
         * @methodOf ora-gui-core.cookieService
         * @description
         */
        service.setCookie = function(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * service.SecondsByDay));
            var expires = 'expires=' + d.toUTCString();
            document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
        };
        /**
         * @ngdoc method
         * @name getCookie
         * @methodOf ora-gui-core.cookieService
         * @description
         */
        service.getCookie = function(cname) {
            var name = cname + '=';
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return '';
        };

        /**
         * @ngdoc method
         * @name calcOffset
         * @methodOf ora-gui-core.cookieService
         * @description
         */
        service.calcOffset = function() {
            var serverTime = service.getCookie('serverTime');
            serverTime = serverTime == null ? null : Math.abs(serverTime);
            var clientTimeOffset = (new Date()).getTime() - serverTime;
            service.setCookie('clientTimeOffset', clientTimeOffset, 1);
        };

        /**
         * @ngdoc method
         * @name checkSession
         * @methodOf ora-gui-core.cookieService
         * @description
         */
        service.checkSession = function() {
            var sessionExpiry = Math.abs(service.getCookie('sessionExpiry'));
            var timeOffset = Math.abs(service.getCookie('clientTimeOffset'));
            var localTime = (new Date()).getTime();

            // Disable redirection when test
            if ($$REDIRECTION_DISABLED) {
                return;
            }

            if ((localTime - timeOffset) > (sessionExpiry + service.extraSeconds)) { // extra seconds to make sure
                if (user.userLogged.internal) {
                    window.location = './timeout.html'; // redirect location
                } else {
                    window.location = './timeoutext.html'; // redirect location
                }

            } else {
                setTimeout(service.checkSession, 10000); // timeout on checkSession
            }
        };

        service.checkExternalAction = function() {
            oraRest.get(baseUrl).then(function() {
                service.checkSession();
            });

        };
        return service;
    }
}