export default class histogramperiod {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.histogram
     * @description
     * # histogram
     * Service in the ora-gui-core.
     */

    constructor() {

        var service = this;

        function getNbMonthPeriod(months, selectedPeriod) {

            if (!selectedPeriod.from || !selectedPeriod.to) {
                return 0;
            }

            var nbMonthTotal = months.length;

            var selectedPeriodBeginRange = 0;
            for (var l = 0; l < months.length; l++) {
                if (months[l] === selectedPeriod.from) {
                    selectedPeriodBeginRange = l;
                    break;
                }
            }
            return nbMonthTotal - selectedPeriodBeginRange;
        }

        function displayPeriod(svg, x, width, dimension, highlightClass) {
            svg.insert('rect', ':first-child')
                .attr('x', x)
                .attr('width', width)
                .attr('class', highlightClass)
                .attr('y', 0)
                .attr('height', dimension.height + dimension.margin.bottom);
            return svg;
        }

        service.displayPeriodSeries = function (svg, histoValues) {
            var nbMonthPeriod = getNbMonthPeriod(histoValues.months, histoValues.selectedPeriod);

            var step = histoValues.range.x(histoValues.months[1]) - histoValues.range.x(histoValues.months[0]);
            var padding = step - (histoValues.range.z.rangeBand() * histoValues.nbSeries);
            var highlightClass = histoValues.options.highlightClass ? histoValues.options.highlightClass : 'fill-purple';
            displayPeriod(svg,
                histoValues.range.x(histoValues.selectedPeriod.from) - padding / 2,
                Math.abs(step) * nbMonthPeriod, histoValues.dimension, highlightClass);
        };

        return service;
    }
}