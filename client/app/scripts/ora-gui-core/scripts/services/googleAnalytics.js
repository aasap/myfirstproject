
export default class googleAnalytics {

    /**
     * @ngdoc service
     * @name ora-gui-core.googleAnalytics
     * @description
     * # googleAnalytics
     * Service in the ora-gui-core.
     */

    constructor($timeout, $q, appConfig, reporTypeConst, oraI18n) {
        'ngInject';

        var service = this;
        // first calls should not be performed because it is automatic and not performed by the user
        service.firstCallToViewPerformed = false;

        service.user = {};

        service.domain = '';

        service.dataClient = function() {
            var data = {
                'profilCompte': 'Client',
                'cheminAcces': 'ECE'
            };
            if (service.user && service.user.internal) {
                data.profilCompte = 'RSC';
                data.cheminAcces = 'Guardian';
            }
            return data;
        };

        service.getInfoToLog = function(pageName, pageType) {
            var infoClient = service.dataClient();
            return {
                'sous_domaine': appConfig.googleAnalytics.sousDomaine,
                'univers_affichage': appConfig.googleAnalytics.universAffichage,
                'sous_univers': appConfig.googleAnalytics.sousUnivers,
                'sous_seg_commercial': appConfig.googleAnalytics.domaineMarketing,
                'code_univers': appConfig.googleAnalytics.codeUnivers,
                'titre_page': pageName,
                'type_page': pageType,
                'profil_compte_navigation': infoClient.profilCompte,
                'chemin_acces': infoClient.cheminAcces,
                'type_langue': oraI18n.selectedCdLang ? oraI18n.selectedCdLang.toUpperCase() : ''
            };
        };

        // Call to utag_data
        service.logPage = function(pageName, pageType) {
            var deferred = $q.defer();
            if (appConfig.googleAnalyticsEnabled) {
                if(pageName && pageType) {
                    window.utag_data = service.getInfoToLog(pageName, pageType);
                }

                var googleAnalyticsScriptElement = angular.element(
                        '<script type="text/javascript" async src="' + appConfig.googleAnalyticsUrl + '"></script>')
                    .appendTo(document.body);

                googleAnalyticsScriptElement.ready(function() {
                    deferred.resolve();
                });

            } else {
                deferred.resolve();
            }
            return deferred.promise;
        };

        // Call to utag_link
        service.logClickEvent = function(pageName, zone, elementName, event) {
            var deferred = $q.defer();

            //wait 2s before notify to be sure the library has been loaded
            $timeout(function() {
                if (appConfig.googleAnalyticsEnabled && window.utag) {
                    window.utag.link({
                        'track_page': pageName,
                        'track_zone': zone,
                        'track_nom': elementName,
                        'track_cible': '',
                        'track_type_evt': event
                    });
                }
                deferred.resolve();
            }, 2000);
            return deferred.promise;
        };

        // Call to utag_view
        service.logViewChange = function(pageName, pageType, domaineMarketing) {
            var deferred = $q.defer();

            if (service.firstCallToViewPerformed) {
                //wait 2s before notify to be sure the library has been loaded
                $timeout(function() {
                    if (appConfig.googleAnalyticsEnabled && window.utag) {
                        let infoPage = service.getInfoToLog(pageName, pageType);
                        infoPage.sous_seg_commercial = domaineMarketing;
                        window.utag.view(infoPage);
                    }
                    deferred.resolve();
                }, 2000);
            } else {
                service.firstCallToViewPerformed = true;
                deferred.resolve();
            }
            return deferred.promise;
        };

        service.getDomaineMarketting = () => {
            return  appConfig.googleAnalytics.domaineMarketing;
        }

        return service;
    }
}