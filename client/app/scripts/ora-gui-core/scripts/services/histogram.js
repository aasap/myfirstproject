export default class histogram {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.histogram
     * @description
     * # histogram
     * Service in the ora-gui-core.
     */

    constructor($filter, histobusiness, histogramlegend, histogramperiod,
        histogramdrawing, histogramValue, garConstant) {
        'ngInject';

        var service = this;
        var paddingXRangeHisto = 0.2;

        function getZRange(range, nbSeries) {
            return d3.scale.ordinal()
                .domain(d3.range(nbSeries))
                .rangeBands([0, range.x.rangeBand()]);
        }

        function selectSeries(svg, data) {
            return svg.selectAll('.serie')
                .data(data)
                .enter()
                .append('g')
                .attr('class', 'serie');
        }

        function paiTable(histoValues) {
            var lineTable = histoValues.formatedData.dataTable;
            var dimension = histoValues.dimension;
            var range = histoValues.range;
            var months = histoValues.months;
            //define dimension based on the histogram frame dimension
            var width = dimension.width + dimension.margin.left + dimension.margin.right;
            var height = dimension.height + dimension.margin.top + dimension.margin.bottom;
            var svgTable = d3.select('#' + histoValues.histogramId).append('svg')
                //.attr('preserveAspectRatio', 'xMinYMin meet')
                //.attr('viewBox', '0 0 ' + width + ' ' + height / 2);
                .attr('width', width)
                .attr('height', height / 2);
            var rectwh = range.x(months[1]) - range.x(months[0]);
            var fontSize = 11;
            //formating of the details table values
            var cumulstride = 0;
            var header = svgTable.selectAll('g')
                .data(d3.entries(lineTable))
                .enter()
                .append('g')
                .attr('transform', function(d) {
                    cumulstride += d.value.stride;
                    return 'translate(0,' + cumulstride + ')';
                });

            header.append('text')
                .attr('x', 0)
                .attr('y', 0)
                .text(function(d) {
                    return d.value.title;
                }).style('font-size', fontSize + 'px');

            header.append('line')
                .attr('x1', 0)
                .attr('y1', function(d) {
                    return d.value.strideLineText;
                })
                .attr('x2', width)
                .attr('y2', function(d) {
                    return d.value.strideLineText;
                })
                .attr('stroke', 'black')
                .attr('stroke-width', '1');

            var box = header.append('g')
                .attr('transform', function() {
                    return 'translate(0,0)';
                })
                .selectAll('g')
                .data(function(d) {
                    return d.value.data;
                })
                .enter()
                .append('g')
                .attr('transform', function(d) {
                    return 'translate(' + (range.x(d.date) + range.x.rangeBand() * 0.5 + dimension.margin.left - rectwh / 2) + ',-14 )';
                });

            box.append('rect')
                .attr('x', 0)
                .attr('y', 0)
                .attr('width', rectwh)
                .attr('height', 17)
                .attr('class', function(d) {
                    return d.bgColor;
                });

            box.append('text')
                .attr('x', function() {
                    return rectwh / 2;
                })
                .attr('y', 15)
                .text(function(d) {
                    return d.value;
                })
                .style('text-anchor', 'middle')
                .style('font-size', fontSize + 'px');
        }

        function yDomainCalculation(values) {
            var y = values.domainLimite.y;
            var yMax = y.max !== null ? y.max :
                d3.max(values.dataHisto, function(d) {
                    return d3.max(d.value.data, function(subData) {
                        return subData === null ? -Infinity : subData.y;
                    });
                });
            
            if(yMax < 0 ) {
        	yMax = 0;
            }
            var yMin = y.min !== null ? y.min :
                d3.min(values.dataHisto, function(d) {
                    return d3.min(d.value.data, function(subData) {
                        return subData === null ? Infinity : subData.y;
                    });
                });           

            return [yMin, yMax];
        }

 
        // Use to fix D3 IE issue on height calculation for svg histrogram
        function forceHeightResizeHistogram(histogramId, height) {
            var myElement = angular.element(document.querySelector('#' + histogramId + ' > svg'));
            if (myElement && myElement[0]) {
                myElement.css('height', height + 'px');
            }
        }

        function createHistogram(values) {
            d3.selectAll('#' + values.histogramId + '>svg').remove();

            values.dimension = histobusiness.calculateDimension(values);

            values.range.y = d3.scale.linear()
                .domain(yDomainCalculation(values))
                .range([values.dimension.height, 0]);

            values.range.x = d3.scale.ordinal()
                .domain(values.months.map(function(d) {
                    return d;
                }))
                .rangeBands([0, values.dimension.width], values.paddingXRange);

            values.range.z = getZRange(values.range, values.nbSeries);

            var axis = {};
            axis.x = d3.svg.axis()
                .scale(values.range.x)
                .orient('bottom')
                .tickFormat(function(d, i) {
                    return values.tickValues[i];
                });

            var tickValue = 7;

            axis.y = d3.svg.axis()
                .scale(values.range.y)
                // affichage de la grille
                .tickSize(-values.dimension.width, 0)
                .orient('left').ticks(tickValue)
                .tickFormat(function(tickValue) {
                    var format;
                    var v = tickValue;
                    var decimal = 0;
                    if (values.options.axis && values.options.axis.y && values.options.axis.y.format) {
                        format = values.options.axis.y.format;
                        decimal = format === 'p' ? 0 : 2;
                    }
                    return format ? $filter('number')(v, decimal) + '%' : $filter('number')(v);
                });

            values.setFnPeriod(histogramperiod.displayPeriodSeries);

            var svg = histogramdrawing.creationHistogramFrame(values, axis);

            svg = values.fnLegend ? values.fnLegend(svg, values) : svg;

            return svg;
        }

        service.paiHistogram = function(histogramId, dataKPI, selectedPeriod, options) {
            let fnFormatData =  histobusiness.formatDataPaiHistoComponent;
            var values = new histogramValue(histogramId, selectedPeriod, options, dataKPI,
        	    fnFormatData, histogramlegend.addLegendMultipleSeriesPai);
            values.paddingXRange = paddingXRangeHisto;
            values.size.left = 180;
            var svg = createHistogram(values);

            var serie = selectSeries(svg, values.dataHisto);
            histogramdrawing.addPath(values, serie);
            histogramdrawing.addCircle(values, serie);

            if (values.fnPeriod) {
                values.fnPeriod(svg, values);
            }

            if (values.formatedData.NA) {
                paiTable(values);
            }

            forceHeightResizeHistogram(histogramId, values.size.height);
        };

    }
}