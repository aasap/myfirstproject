export default class histogramlegend {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.histogram
     * @description
     * # histogram
     * Service in the ora-gui-core.
     */

    constructor() {

        var service = this;
        var fontSize = 11;

        service.addLegendMultipleSeries = function (svg, histoValues) {
            var legendSvg = svg.append('svg');
            var dimension = histoValues.dimension;
            var seriesClass = histoValues.options.seriesClass;
            var dataL = dimension.margin.left;
            var rectSize = 10;
            var textMargin = 20;
            var offset = rectSize + textMargin;
            // height of a char based on 1.4rem font size from boosted orange css
            var textHeight = 10;

            var items = legendSvg.selectAll('g')
                .data(Object.keys(seriesClass))
                .enter().append('g')
                .attr('class', 'legend')
                .attr('transform', function (d, i) {
                    var newdataL = dataL + offset * i;
                    dataL += (seriesClass[d].label.length) * textHeight;
                    var y = dimension.height + dimension.margin.bottom * 0.75;
                    return 'translate(' + newdataL + ',' + y + ')';
                });

            items.append('rect')
                .attr('x', 0)
                .attr('y', 0)
                .attr('width', rectSize)
                .attr('height', rectSize)
                .attr('class', function (d) {
                    if (seriesClass[d].fill) {
                        return seriesClass[d].fill.class;
                    }
                    return seriesClass[d].fillClass;
                });

            items.append('text')
                .attr('x', textMargin)
                .attr('y', rectSize)
                .text(function (d) {
                    return seriesClass[d].label;
                })
                .style('font-size', fontSize + 'px');

            return svg;
        };

        service.addLegendMultipleSeriesUsage = function (svg, histoValues) {
            var legendSvg = svg.append('svg');
            var dimension = histoValues.dimension;
            var seriesClass = histoValues.options.seriesClass;
            var dataL = dimension.margin.left;
            var rectSize = 10;
            var textMargin = 20;
            var offset = rectSize + textMargin;
            // height of a char based on 1.4rem font size from boosted orange css
            var textHeight = 10;

            var items = legendSvg.selectAll('g')
                .data(Object.keys(seriesClass))
                .enter().append('g')
                .attr('class', 'legend')
                .attr('transform', function (d, i) {
                    var newdataL = dataL + offset * i;
                    dataL += (seriesClass[d].label.length) * textHeight;
                    var y = dimension.height + dimension.margin.bottom * 0.75;
                    return 'translate(' + newdataL + ',' + y + ')';
                });

            items.append('line')
                .attr({
                    x1: 0,
                    y1: rectSize / 2,
                    x2: 15,
                    y2: rectSize / 2,
                    'class': function (d) {
                        return seriesClass[d].stroke.class;
                    },
                    'stroke-width': function (d) {
                        return seriesClass[d].stroke.width;
                    }
                });

            items.append('text')
                .attr('x', textMargin)
                .attr('y', rectSize)
                .text(function (d) {
                    return seriesClass[d].label;
                })
                .style('font-size', fontSize + 'px');

            return svg;
        };

        function getYLegendPai(i, dimension) {
            return dimension.height / 2 + 50 + (i - 1) * 30;
        }



        service.addLegendMultipleSeriesPai = function (svg, histoValues) {
            var dimension = histoValues.dimension;
            var seriesClass = histoValues.options.seriesClass;
            var legend = svg.append('g')
                .attr('class', 'legend')
                .attr('height', 100)
                .attr('width', 100)
                .attr('transform', 'translate(-40,-35)');

            var legendRect = legend.selectAll('circle')
                .data(Object.keys(seriesClass))
                .enter();

            legendRect.append('line')
                .filter(function (d) {
                    return seriesClass[d].types.indexOf('line') > -1;
                })
                .attr({
                    x1: -135,
                    y1: function (d, i) {
                        return getYLegendPai(i, dimension);
                    },
                    x2: -105,
                    y2: function (d, i) {
                        return getYLegendPai(i, dimension);
                    },
                    'class': function (d) {
                        return seriesClass[d].stroke.class;
                    },
                    'stroke-width': function (d) {
                        return seriesClass[d].stroke.width;
                    }
                });

            legendRect.append('circle')
                .filter(function (d) {
                    return seriesClass[d].types.indexOf('circle') > -1;
                })
                .attr('cx', -120)
                .attr('r', 5)
                .attr('cy', function (d, i) {
                    return getYLegendPai(i, dimension);
                })
                .attr('class', function (d) {
                    return seriesClass[d].fill.class;
                });

            legendRect.append('text')
                .attr('x', -130)
                .attr('y', function (d, i) {
                    return getYLegendPai(i, dimension) + 16;
                })
                .text(function (d) {
                    return seriesClass[d].label;
                })
                .style('font-size', fontSize + 'px');

            return svg;
        };

        return service;
    }
}