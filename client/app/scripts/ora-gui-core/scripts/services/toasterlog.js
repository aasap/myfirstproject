export default class toasterLog {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.toasterLog
     * @requires toaster $log
     * @description
     * # toasterLog
     * Service in the ora-gui-core.
     */

    constructor($log, toaster, googleAnalytics) {
        'ngInject';
        var service = this;

        /**
         * @ngdoc function
         * @name showToaster
         * @methodOf ora-gui-core.toasterLog
         * @requires toaster $log
         * @description
         * # toasterLog
         * Service in the ora-gui-core.
         */
        service.showToaster = function(contextualClass, msg, timeout, codeError) {
            $log.info(msg);
            let t = timeout ? timeout : 3000;
            toaster.pop(contextualClass, msg, null, t);

            // Track event with GA
            if (contextualClass === 'error') {
                let code = codeError ? codeError + ' : ' : '';
                googleAnalytics.logViewChange(code + msg, 'Erreur', googleAnalytics.domain);
            }
        };
        return service;
    }
}