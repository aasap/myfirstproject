export default class exportToExcelBusiness {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.exportToExcelBusiness
     * @requires ora-gui-services.pai
     * @description
     * # exportToExcelBusiness
     * Service in the ora-gui-core, that contains all business function for exportToExcel.
     */

    constructor(paiConstant, $filter, garConstant, oraI18n) {
        'ngInject';

        var service = this;

        service.getDetailData = function(indicator){
            
            service.information = indicator.onPeriod.information;
            if (!!service.information) {
                let titleRow = _.filter(service.information[0].rows, {
                    "type": "title"
                });
                
                let index = _.findIndex(titleRow[0].cells, function(o) { return o.key == 'gttrExceedingTime'; });
                if(index < 0 ){
                    index = titleRow[0].cells.length - 1;
                }
                titleRow[0].cells.splice(index,0,{
                    "key": "gttrLabel",
                    "value": oraI18n.oraI18nLabels.gtrDetType
                });

                let contentRows = _.filter(service.information[0].rows, {
                    "type": "content"
                });
                _.forEach(contentRows, row => {
                    setGttrLabelData(row);
                    setDateValueFormat(row);
                    let colGttrExceedingTime = _.filter(row.cells, {
                        "key": "gttrExceedingTime"
                    })[0];
                    let colServiceUnavailabilityDuration = _.filter(row.cells, {
                        "key": "serviceUnavailabilityDuration"
                    })[0];
                    if (!!colGttrExceedingTime.value) {
                        setColorAndGttrExceedingValue(colGttrExceedingTime, colServiceUnavailabilityDuration);
                    }
                })               
            }
        return service.information;
        }
                      
        function setGttrLabelData(contentRow) {
            let colgttrTime = _.filter(contentRow.cells, {
                "key": "gttrTime"
            })[0];
            let colgttrType = _.filter(contentRow.cells, {
                "key": "gttrType"
            })[0];
            let index = _.findIndex(contentRow.cells, function(o) { return o.key == 'gttrExceedingTime'; });
            if(index < 0 ){
                index = contentRow.cells.length - 1;
            }
            
            contentRow.cells.splice(index,0,{
                "key": "gttrLabel",
                "value": $filter('timeNumber')(colgttrTime.value, oraI18n.selectedCdLang) + ' ' + colgttrType.value
            });
        }

        function setDateValueFormat(contentRow) {
            let colStartDate = _.filter(contentRow.cells, {
                "key": "ticketStartDate"
            })[0];
            let colRecoveryDate = _.filter(contentRow.cells, {
                "key": "ticketRecoveryDate"
            })[0];
            colStartDate.value = $filter('date')(Date.parse(colStartDate.value.replace(/-/g, '/')), 'dd MMMM HH:mm');

            colRecoveryDate.value = $filter('date')(Date.parse(colRecoveryDate.value.replace(/-/g, '/')), 'dd MMMM HH:mm');
        }

        function setColorAndGttrExceedingValue(colData, colServiceUnavailabilityDuration) {
            if (parseInt(colData.value) > 0) {
                colServiceUnavailabilityDuration.classValue = "bg-red";
            } else {
                colServiceUnavailabilityDuration.classValue = "bg-green";
            }
            colData.value = $filter('timeNumber')(colData.value);
            colServiceUnavailabilityDuration.value = $filter('timeNumber')(colServiceUnavailabilityDuration.value, oraI18n.selectedCdLang);
        }

        return service;
    }
}