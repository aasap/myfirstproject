export default class authInterceptorService {

    constructor($q, $window) {
        'ngInject';

        var responseError = function(rejection) {
            if (rejection.status === 403 || rejection.status === 302) {
                $window.location = './403.html';
            }
            return $q.reject(rejection);
        };

        return {
            responseError: responseError
        };
    }
}