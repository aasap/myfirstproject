/**
 * @ngdoc service
 * @name ora-gui-core.histogramValue
 * @description
 * # histogram
 * Service in the ora-gui-core.
 */

const histogramValue = function($filter, $state) {
    'ngInject';

    var defaultSize = {
        height: 200,
        left: 55,
        bottom: 70,
        right: 30,
        top: 20
    };

    var defaultDomaineLimite = {
        y: {
            min: null,
            max: null
        }
    };

    function histogramValueAbstract(histogramId, selectedPeriod, options, dataKPI, computeFn, fnLegend, annualSpecialDisplay) {

        var c = this;

        c.histogramId = histogramId;
        c.selectedPeriod = selectedPeriod;
        c.options = options;
        c.nbSeries = 1;
        c.fnLegend = fnLegend;
        c.freqAnnual = (c.selectedPeriod.frequency === 'ANN' || c.selectedPeriod.frequency === 'yearly');
        //c.freqBilanAnnual = (c.selectedPeriod.frequency === 'BAN');
        c.size = angular.copy(defaultSize);
        c.domainLimite = angular.copy(defaultDomaineLimite);
        c.range = {
            y: {},
            x: {}
        };
        c.dimension = {};
        c.paddingXRange = false;
        //the parameter annualSpecialDisplay can be undefined/null in this case c.annualSpecialDisplay = false
        c.annualSpecialDisplay = (annualSpecialDisplay) ? annualSpecialDisplay : false;
        c.formatedData = computeFn(dataKPI, options.seriesClass);
        c.months = c.formatedData.months;
        c.dataHisto = d3.entries(c.formatedData.dataHisto);

        function calculateTickValuesByMonths() {
            var tickValues = [];
            var formatDate = c.annualSpecialDisplay && c.freqAnnual ? 'yyyy' : 'MMMM';
            let temp = $filter('orderBy')(c.months);
            angular.forEach(temp, function(month) {
        	 //added to avoid timezone issue 
            tickValues.push($filter('date')(new Date(month), formatDate,'+0'));
            });
            return tickValues;
        }

        c.tickValues = calculateTickValuesByMonths();

        //for current period highlighting
        c.setFnPeriod = function(fnPeriod) {
            c.fnPeriod = c.freqAnnual ? null : fnPeriod;
        };
    }

    return histogramValueAbstract;

}

export default histogramValue;
