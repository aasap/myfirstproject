export default class config {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.config
     * @description
     * # loadConfig
     * Service in the ora-gui-core.
     */

    constructor(oraRest, $q, message) {
        'ngInject';

        var self = this;
        self.config = null;

        /**
         * @ngdoc method
         * @name ora-gui-core.config#chargerConfig
         * @methodOf ora-gui-core.config
         * @description
         * Method to change the configuration
         * @returns {HttpPromise} resolve with fetched data, or fails without error description.
         */
        this.chargerConfig = function() {
            if (!self.config) {
                return oraRest.get('config/config.json', null, '', message.LOADING_ERROR + ' de la configuration.').then(function(result) {
                    self.config = result.data;
                    return {
                        data: self.config
                    };
                });
            } else {
                return $q(function(resolve) {
                    resolve({
                        data: self.config
                    });
                });
            }
        };
    }
}