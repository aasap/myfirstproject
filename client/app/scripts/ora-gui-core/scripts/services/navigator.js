export default class navigatorService {
    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.navigatorService
     * @description
     * # navigatorService
     * Service in the ora-gui-core.
     */

    constructor() {
        var service = this;
        service.isIE = false;
        service.getNavigator = function () {
            if (navigator.appName === 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== 'undefined' && $.browser.msie === 1)) {
                service.isIE = true;
            } else {
                service.isIE = false;
            }

        };
        return service;
    }
}