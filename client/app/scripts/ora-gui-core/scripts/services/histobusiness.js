export default class histobusiness {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-core.histobusiness
     * @requires ora-gui-services.pai
     * @description
     * # histobusiness
     * Service in the ora-gui-core, that contains all business function for histogram creation.
     */

    constructor(paiConstant, $filter,  oraI18n) {
        'ngInject';

        var service = this;
        var paiRespRateClientThreshold = paiConstant.RESP_RATE_CLIENT_THRESHOLD;

 
        
        /**
         * @ngdoc function
         * @name computeFillColorPaiComponent
         * @methodOf ora-gui-core.histobusiness
         * @description
         * # computeFillColorPaiComponent
         * compute pai color for the histogram and the table.
         */
        service.computeFillColorPaiComponent = function(month, naColor, normalColor) {
            return isNonApplicableValue(month)? naColor : normalColor;
        };

        function getPoint(x, y, firstPoint) {
            let resY = (!y && y !== 0) ? -1 : y;
            return {
                x: x,
                y: resY,
                firstPoint: firstPoint
            };
        }

        function getPointPaiCustThreshold(x, firstPoint) {
            return getPoint(x, paiRespRateClientThreshold, firstPoint);
        }


        function xLineFullPeriodPai(d, i, histoValues) { //NOSONAR
            var index = Math.round(i / 2);
            var nbMonth = histoValues.months.length;
            return index >= nbMonth ?
                histoValues.range.x(histoValues.months[nbMonth - 1]) + histoValues.range.x.rangeBand() :
                histoValues.range.x(histoValues.months[index]);
        }

        function xLine(d, range, padding) {
            return range.x(d.x) + (range.x.rangeBand() * padding);
        }

        function xLinePai(d, i, histoValues) { //NOSONAR
            return xLine(d, histoValues.range, 0.5);
        }


        service.calculateDimension = function(values) {
            var size = values.size;
            return {
                'margin': {
                    'top': size.top,
                    'right': size.right,
                    'bottom': size.bottom,
                    'left': size.left
                },
                'width': angular.element(document.querySelector('#' + values.histogramId)).width() - size.left - size.right,
                'height': size.height - size.top - size.bottom
            };
        };


        
        //***New PAI component****
        function isNonApplicableValue (month) {
            return ! month.applicability || ! month.applicability.applicable || (!month.measure && month.measure !== 0);
        }
        
        function getPaiDetailsTableStructure(indicator) {
            let paiEligCondLabel = oraI18n.oraI18nLabels.paiEligCondLabel;
            
            let clientResponsibilityRateMetric = _.find(indicator.metricsDefinitions, (metric) => {
                return metric.name === 'clientResponsibilityRate';
            });
            
            let totalIncidentNumberMetric = _.find(indicator.metricsDefinitions, (metric) => {
                return metric.name === 'totalIncidentNumber';
            });
            
            return {
                value: {
                    title: indicator.shortLabel,
                    data: [],
                    strideLineText: 4,
                    stride: 25,
                    length: 13,
                    fractionSize: 0,
                    colorFunction: function(applicability) {
                        return service.computeFillColorPaiComponent(applicability, 'fill-gray', 'fill-transparent');
                    }
                },
                label: {
                    title: paiEligCondLabel,
                    data: [],
                    strideLineText: 10,
                    stride: 25,
                    length: 0
                },
                clientResponsibilityRate: {
                    title: clientResponsibilityRateMetric.longLabel,
                    data: [],
                    strideLineText: 4,
                    stride: 25,
                    length: 13,
                    fractionSize: 0,
                    colorFunction: function(applicability) {
                        return ! applicability.applicable ? 'fill-gray' : 'fill-transparent';

                    }
                },
                totalIncidentNumber: {
                    title: totalIncidentNumberMetric.longLabel,
                    data: [],
                    strideLineText: 4,
                    stride: 19,
                    length: 13,
                    colorFunction: function(applicability) {
                        return ! applicability.applicable ? 'fill-gray' : 'fill-transparent';
                    }
                }
            };
        }
        
        service.formatDataPaiHistoComponent = function(indicator, seriesClass) {
            var containtNA = false;
            let monthlyHistory = indicator.monthlyHistory;
            var months = [];
            var data = seriesClass;
            data.paiOrange.data = [];
            data.paiOrange.fn = {
                x: xLinePai
            };
            data.paiCust.data = [];
            data.paiCust.fn = {
                x: xLinePai
            };
            data.paiCustThreshold.data = [];
            data.paiCustThreshold.fn = {
                x: xLineFullPeriodPai
            };
            var tableLine = getPaiDetailsTableStructure(indicator); 
            angular.forEach(monthlyHistory, function(month) {
                containtNA = containtNA || isNonApplicableValue(month);
                var date = Date.parse(month.month); 
                months.push(date);
                data.paiOrange.data.push({
                    'x': date,
                    'y':  ! month.measure && month.measure !== 0 ? -1 : month.measure ,
                    //'threshold': month.contractualThreshold,
                    'colorCircle': service.computeFillColorPaiComponent(month, 'fill-white', 'fill-brand-blue')
                });
                
                let clientResponsibilityRate = _.find(month.metrics, (metric) => {
                    return metric.name === 'clientResponsibilityRate';
                });
                
                data.paiCust.data.push({
                    'x': date,
                    'y':  (! clientResponsibilityRate.measure && clientResponsibilityRate.measure !==0) ? -1 : clientResponsibilityRate.measure,
                    'colorCircle': 'fill-black'
                });

                for (var i = 0; i <= 1; i++) {
                    var firstPoint = i === 0;
                    data.paiCustThreshold.data.push(getPointPaiCustThreshold(date, firstPoint));
                }
                
                let fractionSize = ( month.measure && month.measure > 0 && month.measure < 100) ? tableLine.value.fractionSize : 0;
                tableLine.value.data.push({
                    'date': date,
                    'value': ( ! month.measure && month.measure !== 0) ? '-' : $filter('number')(month.measure, fractionSize),
                    'bgColor': tableLine.value.colorFunction(month)
                });
                
                angular.forEach(month.metrics, function(metric) {
                    if(tableLine[metric.name]) {
                        let fractionSize = (tableLine[metric.name].fractionSize && metric.measure && metric.measure > 0 && metric.measure < 100) ? tableLine[metric.name].fractionSize : 0;
                        tableLine[metric.name].data.push({
                            'date': date,
                            'value': (!metric.measure && metric.measure !== 0) ? '-' : $filter('number')(metric.measure, fractionSize),
                            'bgColor': tableLine[metric.name].colorFunction(metric.applicability)
                        });
                    }
                });
            });
            return {
                dataTable: tableLine,
                dataHisto: data,
                NA: containtNA,
                months: months
            };
        };
        //** end new PAI component
        
        return service;
    }
}