// MODULES
import 'angular-animate';
import 'angular-cookies';
import 'angular-resource';
import 'angular-shims-placeholder';
import 'angular-touch';
import 'angular-ui-bootstrap';
import 'bootstrap/dist/js/bootstrap.js';
import 'jquery-validation/dist/jquery.validate.js';
import 'boosted/dist/js/boosted.js';
import 'angularjs-toaster';
import 'angular-smart-table';
import 'spin.js';
import 'angular-spinner';
import 'angular-ui-router';
import 'angular-local-storage';
import 'd3/d3.js';
import 'nvd3/build/nv.d3.js';
import 'angular-nvd3/dist/angular-nvd3.js';
import 'blob/index.js';
import 'angular-file-saver';
import 'angular-file-saver/dist/angular-file-saver.bundle.js';
import 'jquery-ui';
import 'jquery-ui/ui/widgets/tooltip.js';
import 'angular-dynamic-locale';
import 'angular-translate';
import 'angular-translate-loader-partial';

// Solution to import Rangy in webpack
(() => {
    window.taTools = {};
    window.rangy = require('rangy/lib/rangy-core');
})();

import 'rangy/lib/rangy-classapplier.js';
import 'rangy/lib/rangy-highlighter.js';
import 'rangy/lib/rangy-selectionsaverestore.js';
import 'rangy/lib/rangy-serializer.js';
import 'rangy/lib/rangy-textrange.js';

import 'textangular/dist/textAngular.umd.js';
import 'textangular/dist/textAngular-sanitize.js';

// SERVICES
import authInterceptorService from './services/authinterceptor.js';
import googleAnalytics from './services/googleAnalytics.js';
import exportToExcelBusiness from  './services/exportToExcelBusiness.js';
import config from './services/config.js';
import cookies from './services/cookies.js';
import {D3responsive} from '@05o/libraries';
import histobusiness from './services/histobusiness.js';
import histogramdrawing from './services/histogramdrawing.js';
import histogramlegend from './services/histogramlegend.js';
import histogramperiod from './services/histogramperiod.js';
import histogramvalue from './services/histogramvalue.js';
import histogram from './services/histogram.js';
import {modal} from '@05o/libraries'; 
import {commonModals} from '@05o/libraries';
import navigator from './services/navigator.js';
import {oraRest} from '@05o/libraries'; //'./services/orarest.js';
import user from './services/user.js';
import toasterLog from './services/toasterlog.js';
import {utils} from '@05o/libraries';
import {dataRefresh}  from '@05o/libraries';
import {oraI18n} from '@05o/libraries'; 

//Generic components SERVICES 
import {histogramGeneric} from '@05o/libraries'; 
import {histogramGenericLegend} from '@05o/libraries';
import {histogramGenericDrawing} from '@05o/libraries';
import {histogramGenericPeriod} from '@05o/libraries';
import {histogramGenericBusiness} from '@05o/libraries';
import {tableGenericBusiness} from '@05o/libraries';

//DIRECTIVES
import focus from './directives/autofocus.directive';
import {focusOnShow} from '@05o/libraries'; 
import oraTips from './directives/tipsDirective';
import stSummary from './directives/modalPerimetersDirective';
import {tamaxlength} from '@05o/libraries' 
import {oraNonContractualTable} from '@05o/libraries'; 
import {oraEvolutionTable} from '@05o/libraries';
import {oraGenericMonthlyTable} from  '@05o/libraries'; 
import {oraStackedHistogram} from '@05o/libraries'; 
import {oraFlatHistogram} from  '@05o/libraries';
import {oraLineHistogram} from '@05o/libraries'; 
import {oraLinedotsHistogram} from '@05o/libraries';
import {oraSection} from   '@05o/libraries'; 
import {oraGenericDetailedRecordTable} from '@05o/libraries'; 
import {oraGenericDetailsTableFiltred} from '@05o/libraries'; 
import {oraGeneralInformations} from '@05o/libraries'; 
import {oraFact} from '@05o/libraries'; 
import {oraSummary} from '@05o/libraries';
import {oraIndicatorTitle} from '@05o/libraries';
import {oraWarningMessage} from '@05o/libraries'; 
import {oraComments} from '@05o/libraries'; 
import oraTopIndicator from './directives/topIndicator.directive';
import {periodselection} from '@05o/libraries';
import paiHistoric from './directives/paiHistoric';
import oraExportToExcel from './directives/oraExportToExcel';
import trendIndicator from './directives/trendindicator';
// OTHERS
import coreRun from './core.run.js';
import coreConfig from './core.config.js';
import coreRoute from './core.route.js';
import coreConstants from './core.constant.js';
import coreValue from './core.value.js';

import '../../../i18n/angular-locale_fr.js';

//'use strict'; //NOSONAR

/**
 * @ngdoc function
 * @name ora-gui-core
 * @description
 * # ORA Core Module
 */
export default angular
    .module('ora-gui-core', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'ng.shims.placeholder',
        'toaster',
        'angularSpinner',
        'ui.router',
        'smart-table',
        'LocalStorageModule',
        'ngFileSaver',
        'textAngular',
        'tmh.dynamicLocale',
        'pascalprecht.translate'
    ]).factory('histogramValue', histogramvalue)
    .service({
        'utils': utils,
        'datarefresh': dataRefresh,
        'authInterceptorService': authInterceptorService,
        'googleAnalytics': googleAnalytics,
        'exportToExcelBusiness':exportToExcelBusiness,
        'config': config,
        'cookieService': cookies,
        'D3responsive': D3responsive,
        'histobusiness': histobusiness,
        'histogramdrawing': histogramdrawing,
        'histogramlegend': histogramlegend,
        'histogramperiod': histogramperiod,
        'histogram': histogram,
        'modal': modal,
        'commonModals': commonModals,
        'navigatorService': navigator,
        'oraRest': oraRest,
        'user': user,
        'toasterLog': toasterLog,
        'histogramGeneric': histogramGeneric,
        'histogramGenericLegend': histogramGenericLegend,
        'histogramGenericDrawing': histogramGenericDrawing,
        'histogramGenericPeriod': histogramGenericPeriod,
        'histogramGenericBusiness': histogramGenericBusiness,
        'tableGenericBusiness': tableGenericBusiness,
        'oraI18n':oraI18n
    }).directive({
        'focus': focus,
        'focusOnShow':focusOnShow,
        'oratips': oraTips,
        'stSummary': stSummary,
        'tamaxlength': tamaxlength,
        'oraNonContractualTable': oraNonContractualTable,
        'oraEvolutionTable': oraEvolutionTable,
        'oraSection': oraSection,
        'oraStackedHistogram': oraStackedHistogram,
        'oraFlatHistogram': oraFlatHistogram,
        'oraLineHistogram': oraLineHistogram,
        'oraLinedotsHistogram': oraLinedotsHistogram,
        'oraGenericMonthlyTable': oraGenericMonthlyTable,
        'oraGenericDetailedRecordTable': oraGenericDetailedRecordTable,
        'oraGenericDetailsTableFiltred': oraGenericDetailsTableFiltred,
        'oraSummary': oraSummary,
        'oraGeneralInformations': oraGeneralInformations,
        'oraFact': oraFact,
        'oraTopIndicator': oraTopIndicator,
        'oraIndicatorTitle': oraIndicatorTitle,
        'oraWarningMessage': oraWarningMessage,
        'oraComments': oraComments,
        'oraPeriodSelection': periodselection,
        'paiHistoric': paiHistoric,
        'oraExportToExcel': oraExportToExcel,
        'trendIndicator': trendIndicator
    }).config(
        coreRoute
    ).config(
        coreConfig
    ).value(
        coreConstants
    ).value(
        coreValue
    ).run(coreRun)
    .name;
