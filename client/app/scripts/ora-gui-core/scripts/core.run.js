const coreRun = function($rootScope, config,
    appConfig, navigatorService, paginationService,$templateCache) {
    'ngInject';

    config.chargerConfig()
        .then(function(result) {
            $.extend(appConfig, result.data);
            $rootScope.version = result.data.version;
        });

    $rootScope.$on('$stateChangeError', function(event, error) {
        event.preventDefault();
    });

    // CHANGE OF DOCUMENT DOMAIN IN CASE OF orange-business.com document domain.
    if (document.domain.indexOf('orange-business.com') !== -1) {
        try {
            document.domain = 'orange-business.com';
        } catch (e) {

        }
    }

    /*setInterval(() => {
        if (parent && parent.ece_adjust_iframe_height) {
            parent.ece_adjust_iframe_height();
        }
    }, 1000);*/

    navigatorService.getNavigator();
    paginationService.setTemplate($templateCache);
};

export default coreRun;