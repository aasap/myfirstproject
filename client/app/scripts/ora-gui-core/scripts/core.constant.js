/**
 * @ngdoc service
 * @name ora-gui-core.message
 * @description
 * Constant string for messages
 * @example
 * SUCCESS: 'Opération réussie',
 * LOADING_ERROR:'Erreur lors du chargement',
 * ......
 */


const coreConstants = {

    typeOfMonthlyData: {
        CONTRACTUAL_THRESHOLD: 'Contractual threshold',
        INDICATOR: 'Indicator',
        METRICS: 'Metrics'
    },

    message: {
        SUCCESS: 'Opération réussie ',
        LOADING_ERROR: 'Erreur lors du chargement',
        SAVING_ERROR: 'Erreur lors de la sauvegarde',
        SAVING_SUCCESS: 'Sauvegarde réussie ',
        PERIOD_SUMMARY: 'pour les mois de la période',
        PERIOD_DETAILS: 'de la table de détails de la période',
        PERIOD_HISTORIC: 'd\'historique de la période',
        PERIOD_VALIDATION_ERROR: 'La période ne peut être validée suite à un problème technique. Veuillez contacter votre administrateur.',
        DATA_LOADING_ERROR: 'Erreur lors du chargement des données',
        SERVICE_ERROR: 'Erreur lors du chargement du service associé a la subscription.',
        OFFERS_ERROR: 'Erreur lors de la récupération des offres.',
        VISIBILITY_ERROR: 'Erreur lors de la mise à jour de la visibilité de la subscription',
        REP_VISIBILITY_DATE_ERROR: 'Erreur lors de la mise à jour de la date de visibilité des périodes',
        RECALCULATING_STATUS: 'du status de recalcul',
        RELUNCH_ERROR: 'Erreur lors du lancement des personnalisations en attentes',
        MTP_MESSAGE: function(message) {
            return 'Installation en cours, ' + message + ' est actuellement désactivée et sera disponible dès la fin du déploiement.';
        },
        RUN_ONGOING_MESSAGE: function(message) {
            return 'Calcul des rapports en cours, ' + message + ' est actuellement désactivée et sera disponible dès la fin du calcul.';
        }
    },

    /**
     * @ngdoc service
     * @name ora-gui-core.rest
     * @description
     * Constant string for urls
     * @example
     * PERSONALIZATION: '/personalization',
     * THRESHOLD: '/thresholds',
     * ......
     */
    rest: {
        PERSONALIZATION: '/personalization',
        PERSONALIZATION_NEW: '/personalization/v2',
        THRESHOLD: '/thresholds',
        CALCULATION_MODE: '/calculation/mode',
        RECALCULATING: '/recalculating',
        SUBSCRIPTIONS: '/subscriptions',
        SERVICES: '/service',
        CURRENT: '/current',
        PERIMETERS: '/perimeters',
        EXPLANATION: '/explanation',
        USER: '/user',
        SCCR: '/sccr',
        SCCV: '/sccv',
        ALL: '/all',
        QOS: '/qos',
        HISTORIC: '/historic',
        DETAILS: '/details',
        PERIOD: '/period',
        PERIODS: '/periods',
        REPORTS: '/reports',
        BAN: '/ban',
        COMMENT: '/comment',
        INVENTORY: '/inventory',
        GAR: '/gar',
        GAR_VPN: '/vpn',
        GAR_ETHERNET: '/ethernet',
        PAI: '/pai',
        GTR: '/gtr',
        IMS: '/ims',
        AAR: '/aar',
        INCIDENT: '/incident',
        PERFORMANCE: '/performance',
        USAGEDATA: '/usagedata',
        USAGEACCESS: '/usageaccess',
        XLSEXPORT: '/export/xls',
        CONTRACT: '/contract',
        TICKETS: '/tickets',
        INC: '/inc',
        CHECKOUT_CONF: '/checkup/conf',
        CHECKOUT_API: '/checkup/apiState',
        EXTERNAL_API_VERSION: '/checkup/externalApi',
        SCCR_NEW_VISIBILITY: '/checkup/sccrNewVisibility',
        TOOLTIPS: '/tooltips',
        LABELS: '/labels',
        LASTREFRESH: '/lastrefresh',
        CHECKUP: '/checkup',
        DAILY_RUN_STATUS: '/runStatus',
        ONWAITING: '/onwaiting',
        QV: '/qv',
        RECALCULATION: '/plannedrecalculation',
        VISIBILITY: '/visibility'
    },

    stateConst: {
        MSCCR: 'ora.msccr'
    },
    reportingUsageConst: {
        MSCCR: 17,
    },

    excelConst: {
        MSCCR: 'MSCCR'
    },

    reporTypeConst: {
        MSCCR: 'msccr',
        SCCR:'sccr'
    },

    // old sccr constant

    optionsConstant: {
        PROACTIVITY: 'Proactivité',
        OPTIMISATION: 'Optimisation',
        PERFORMANCE: 'Performance',
        DISPONIBILITY: 'Disponibilité',
        ACCESS_REF: 'Accès de Référence',
        SUPPERSO: 'Supervision Personnalisée',
        DIGIMONI: 'Vision Equipement',
        BOO_PERFEAM: 'Performance Applicative EAM Riverbed',
        BOO_PERFNB: 'Performance Applicative Network Boost'
    },

    aarConstant: {

        TITLE: 'IND_QOS_AAR_SHORTLABEL',
        SUMMARY: [{
            displayHeaders: true,
            headers: [
                'MET_QOS_AAR_ACHIEVE_RATE_LONGLABEL',
                'MET_QOS_AAR_BREACH_NB_LONGLABEL',
                'MET_QOS_AAR_ACCESS_NB_SHORTLABEL'
            ]
        }],
        DETAILS: {
            title: 'AAR_DET_TITLE_LONGLABEL',
            headers: [
                [{
                        name: 'MET_SHARED_MONTH_SHORTLABEL'
                    },
                    {
                        name: 'MET_SHARED_SITE_NAME_SHORTLABEL'
                    },
                    {
                        name: 'MET_SHARED_REF_SHORTLABEL'
                    },
                    {
                        name: 'MET_QOS_CONTRACT_THR_LONGLABEL'
                    },
                    {
                        name: 'MET_QOS_MEASURE_SHORTLABEL'
                    }
                ]
            ]
        },
        MESSAGES: {
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL',
            AAR: ' de disponibilité par accès',
            AAR_CALCUL_MODE: ' du mode de calcule des disponibilitées par accès',
            AAR_THRESHOLD: ' des valeurs d\'engagement personnalisés pour la disponibilité par accès'
        }
    },

    garConstant: {
        TITLE: 'GLOBAL_GAR_SHORTLABEL',
        SUMMARY: [{
            displayHeaders: true,
            headers: [
                'MET_QOS_CONTRACT_THR_LONGLABEL',
                'MET_QOS_MEASURE_SHORTLABEL'
            ]
        }],
        HISTOGRAM_ID: {
            vpn: 'histoVPN',
            ethernet: 'histoETHERNET'
        },
        MESSAGES: {
            noAccessEligible: 'pas d\'accès éligible',
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL',
            lessMoisAccessEligible: 'QOS_GAR_NA_REASON_LONGLABEL',
            lessAnneeAccessEligible: 'QOS_GAR_NA_REASON_SHORTLABEL'
        },
        MIN_ACCESS: 5
    },

    newGarConstant: {
        HISTOGRAM_ID: {
            vpn: 'qosGarInternet',
            ethernet: 'qosGarEthernet'
        }
    },
    gtrConstant: {

        TITLE: 'IND_QOS_GTR_SHORTLABEL',
        SUMMARY: [{
            displayHeaders: true,
            headers: [
                'MET_QOS_GTR_ACHIEVE_RATE_LONGLABEL',
                'MET_QOS_GTR_BREACH_NB_LONGLABEL',
                'MET_QOS_GTR_ACHIEVE_NB_LONGLABEL'
            ]
        }],
        DETAILS: {
            title: 'GTR_DET_TITLE_LONGLABEL',
            headers: [
                [{
                        name: 'MET_SHARED_MONTH_SHORTLABEL'
                    },
                    {
                        name: 'GTR_DET_TICKETNUMBER_SHORTLABEL'
                    },
                    {
                        name: 'GTR_DET_OFFERGROUP_SHORTLABEL'
                    },
                    {
                        name: 'MET_SHARED_SITE_NAME_SHORTLABEL'
                    },
                    {
                        name: 'GTR_DET_TICKETSTARTDATE_SHORTLABEL'
                    },
                    {
                        name: 'GTR_DET_TICKETRECOVERYDATE_SHORTLABEL'
                    },
                    {
                        name: 'GTR_DET_UNAV_SERV_SHORTLABEL'
                    },
                    {
                        name: 'GTR_DET_UNAV_SUPP_SHORTLABEL'
                    },
                    {
                        name: 'IND_QOS_GTR_SHORTLABEL'
                    },
                    {
                        name: 'GTR_DET_EXCEEDINGTIME_SHORTLABEL'
                    }
                ]
            ]
        },
        HISTOGRAM_ID: 'histoGTR',
        MESSAGES: {
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL'
        }
    }

    ,
    imsConstant: {
        TITLE: 'IND_QOS_IMS_LONGLABEL',
        SUMMARY: [{
            displayHeaders: true,
            headers: [
                'MET_QOS_IMS_ACHIEVE_RATE_LONGLABEL',
                'MET_QOS_IMS_BREACH_NB_LONGLABEL',
                'MET_QOS_IMS_ACHIEVE_NB_LONGLABEL'
            ]
        }],
        DETAILS: {
            title: 'IMS_DET_TITLE_LONGLABEL',
            headers: [
                [{
                        name: 'MET_SHARED_SITE_NAME_SHORTLABEL'
                    },
                    {
                        name: 'MET_SHARED_REF_SHORTLABEL'
                    },
                    {
                        name: 'IMS_DET_OFFERGROUP_SHORTLABEL'
                    },
                    {
                        name: 'IMS_DET_MSICALCULATIONPERIOD_SHORTLABEL'
                    },
                    {
                        name: 'MET_QOS_CONTRACT_THR_SHORTLABEL'
                    },
                    {
                        name: 'IMS_DET_INCIDENTNUMBER_SHORTLABEL'
                    },
                    {
                        name: 'MET_QOS_MEASURE_SHORTLABEL'
                    }
                ]
            ]
        },
        HISTOGRAM_ID: 'histoIMS',
        MESSAGES: {
            noAccessEligible: 'QOS_IMS_NA_REASON_LONGLABEL',
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL'
        }
    }

    ,
    incidentConstant: {
        TITLE: 'SEC_INC_SHORTLABEL',
        SUMMARY: [{
            displayHeaders: true,
            headers: []
        }],
        HISTOGRAM_ID: 'histoINCIDENT',
        MESSAGES: {
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL'
        }
    }

    ,
    inventoryConstant: {
        TITLE: 'SEC_INV_SHORTLABEL',
        SUMMARY: [{
            displayHeaders: true,
            displayHeaderInline: true,
            title: 'IND_INV_OFF_SHORTLABEL',
            key: 'offers',
            headers: [
                'MET_INV_EVOLUTION_SHORTLABEL',
                'MET_INV_TOTAL_SHORTLABEL'
            ]
        }, {
            displayHeaders: false,
            title: 'IND_INV_SITE_SHORTLABEL',
            key: 'site'
        }, {
            displayHeaders: false,
            title: 'IND_INV_OPT_SCCR_LONGLABEL',
            key: 'option'
        }],
        HISTOGRAM_ID: 'histoINVENTORY'
    }

    ,
    paiConstant: {
        TITLE: 'IND_QOS_PROACT_SHORTLABEL',
        SUMMARY: [{
            displayHeaders: true,
            headers: [
                'MET_QOS_CONTRACT_THR_LONGLABEL',
                'MET_QOS_MEASURE_SHORTLABEL',
                'MET_QOS_PAI_ACCESS_NB_SHORTLABEL'
            ]
        }],
        HISTOGRAM_ID: 'histoPAI',
        MESSAGES: {
            mesureNotEligible: [
                'QOS_PAI_NA_REASON_SHORTLABEL',
                'QOS_PAI_NA_REASON_LONGLABEL'
            ],
            THRESHOLD: ' des valeurs d\'engagement personnalisées pour la proactivité',
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL'
        },
        INCIDENT_MIN: 3,
        RESP_RATE_CLIENT_THRESHOLD: 20
    }

    ,
    performanceConstant: {
        TITLE: 'IND_QOS_PERF_SHORTLABEL',
        SUMMARY: [{
            title: 'IND_QOS_PERF_TRANSIT_LONGLABEL',
            displayHeaders: true,
            headers: [
                'MET_QOS_PERF_ACHIEVE_RATIO_LONGLABEL',
                'MET_QOS_PERF_BREACH_NB_LONGLABEL',
                'MET_QOS_PERF_ROUTES_NB_LONGLABEL'
            ]
        }, {
            title: 'IND_QOS_PERF_PACKET_LOSS_LONGLABEL',
            displayHeaders: false,
            headers: [
                'MET_QOS_PERF_ACHIEVE_RATIO_LONGLABEL',
                'MET_QOS_PERF_BREACH_NB_LONGLABEL',
                'MET_QOS_PERF_ROUTES_NB_LONGLABEL'
            ]
        }, {
            title: 'IND_QOS_PERF_JITTER_SHORTLABEL',
            displayHeaders: false,
            headers: [
                'MET_QOS_PERF_ACHIEVE_RATIO_LONGLABEL',
                'MET_QOS_PERF_BREACH_NB_LONGLABEL',
                'MET_QOS_PERF_ROUTES_NB_LONGLABEL'
            ]
        }],
        DETAILS: {
            title: 'PERF_DET_TITLE_LONGLABEL',
            xlsExport: {
                enabled: true,
                url: ''
            },
            headersLine: 2,
            style: 'table-size-fixed',
            headers: [
                [{
                        name: 'MET_SHARED_MONTH_SHORTLABEL',
                        rowspan: 2,
                        style: 'width : 7%'
                    },
                    {
                        name: 'PERF_DET_END_A_SHORTLABEL',
                        rowspan: 2,
                        style: 'width : 12%',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_END_B_SHORTLABEL',
                        rowspan: 2,
                        style: 'width : 12%'
                    },
                    {
                        name: 'PERF_DET_COS_SHORTLABEL',
                        rowspan: 2,
                        style: 'width : 7%;',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_SIGN_SAMPL_LONGLABEL',
                        rowspan: 2,
                        style: 'width : 10%;',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_TRANSIT_LONGLABEL',
                        colspan: 2
                    },
                    {
                        name: 'PERF_DET_PACKET_LOSS_RATE_LONGLABEL',
                        colspan: 2
                    },
                    {
                        name: 'PERF_DET_JITTER_SHORTLABEL',
                        colspan: 2
                    }
                ],
                [{},
                    {},
                    {},
                    {},
                    {},
                    {
                        name: 'MET_QOS_CONTRACT_THR_SHORTLABEL',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'MET_QOS_MEASURE_SHORTLABEL'
                    },
                    {
                        name: 'MET_QOS_CONTRACT_THR_SHORTLABEL',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'MET_QOS_MEASURE_SHORTLABEL'
                    },
                    {
                        name: 'MET_QOS_CONTRACT_THR_SHORTLABEL',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'MET_QOS_MEASURE_SHORTLABEL'
                    }
                ]
            ]
        },
        MESSAGES: {
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL'
        }
    }

    ,

    commentConstant: {
        MAX_SIZE: 400000,
        MAX_SIZE_WARNING: 'La saisie dépasse la taille maximalle autorisée (423 264/400 000)',
        REQUIRED_WARNING: 'Vous devez fournir les faits marquants de la période'
    },

    usageIndicatorsConstant: {
        usageDataVolum: {
            detailsTableTitleOrder: ['siteName', 'accessName', 'overrunNumber', 'averageDuration', 'maximumLoadRate', 'way', 'wayCopy', 'duration']
        },
        usageDataLoad: {

        }
    },

    sectionNamesConstant: {
        USAGE: {
            name: 'usage',
            indicatorsOrder: ['SCCRDataVolume', 'SCCRDataLoad']
        }
    },

    dimensionValuesConstant: {
        YES: 'yes',
        ONLY: 'only',
        NO: 'no'
    },

    indicatorsName: {
        SCCR_DATA_VOLUME: 'SCCRDataVolume',
        SCCR_DATA_LOAD: 'SCCRDataLoad',
        SCCR_GAR_ETHERNET: 'globalEthernetAvailabilityRate',
        SCCR_GAR_VPN: 'globalVpnInternetAvailabilityRate',
        SCCR_IMS: 'MSIFulfilment',
        SCCR_PERFORMANCE: 'performance'
    },

    inapplicabilityReasonConstant: {
        NOT_SUBSCRIBED: 'notSubscribed',
        NOT_ENOUGH_OFFER: 'notEnoughOffers',
        NO_DATA: 'noData',
        NOT_CALCULABLE: 'notCalculable'
    },

    synonymStatusConstant: {
        READ: 'SWITCHED_TO_READ'
    }
}

export default coreConstants;