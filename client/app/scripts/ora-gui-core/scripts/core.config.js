const coreConfig = function(
  $httpProvider,
  localStorageServiceProvider,
  $provide,
  $locationProvider,
  $compileProvider,
  $translateProvider,
  tmhDynamicLocaleProvider
) {
  'ngInject';

  // Disable ng-scope and ng-binding from generated HTML
  if (!$$DEBUG_INFO_ENABLED) {
    $compileProvider.debugInfoEnabled(false);
  }

  // Set locale file path according to the selected language
  tmhDynamicLocaleProvider.localeLocationPattern('/oragui-msccr/i18n/angular-locale_{{locale}}.js');

  // Set translated labels and default prefered language
  $translateProvider.useLoader('$translatePartialLoader', {
    urlTemplate: '/ora-internal-provider-api/rest/sccr/labels?cdLang={lang}&{part}'
  });

  // Secure strategy
  //$translateProvider.useSanitizeValueStrategy('sanitize'); issue with sanitize and escape mode ==> So as labels come from database (no user interaction) they are trustable
  // Use cache for previous report datas
  $translateProvider.useLoaderCache(true);

  // HTML5 mode
  $locationProvider.html5Mode(true);

  // disable IE ajax request caching
  $httpProvider.defaults.headers.common['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
  $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
  $httpProvider.defaults.headers.common.Pragma = 'no-cache';
  $httpProvider.interceptors.push('authInterceptorService');

  localStorageServiceProvider.setPrefix('OR&A');

  // this demonstrates how to register a new tool and add it to the default toolbar
  $provide.decorator('taOptions', [
    '$delegate',
    function(taOptions) {
      // $delegate is the taOptions we are decorating

      //<i class="fa fa-file-pdf-o" aria-hidden="true"></i>

      //Keep H1, H2, P, B, I, U, S, bullets, numbers, undo/redo/undo all, indent.
      taOptions.toolbar = [
        ['h1', 'h2', 'p'],
        ['bold', 'italics', 'underline', 'strikeThrough'],
        ['ul', 'ol', 'redo', 'undo', 'clear'],
        ['justifyLeft', 'justifyCenter', 'justifyRight'],
        ['indent', 'outdent'],
        ['insertLink']
      ];
      return taOptions;
    }
  ]);
};

export default coreConfig;
