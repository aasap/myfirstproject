export default class MainController {
    //'use strict'; //NOSONAR

    /**
     * @ngdoc controller
     * @name ora-internal-gui.controller:MainController
     * @requires ora-internal-gui.modal
     * @requires ora-internal-gui.perimeter
     * @requires ora-internal-gui.subscription
     * @requires ora-internal-gui.googleAnalytics
     * @requires ora-internal-gui.user
     * @description
     * # MainCtrl
     * Controller of the ora-internal-gui
     */

    constructor($scope, $state, $http, $q, usSpinnerService, utils, appConfig, oraI18n, cookieService) {
        'ngInject';

        // Watch pour l'activation/desactivation automatique du spinner
        $scope.$watch(
            function() {
                return ($http.pendingRequests.length > 0);
            },
            function(flag) {
                flag ? usSpinnerService.spin('ora-spinner') : usSpinnerService.stop('ora-spinner');
            }, true);

         //load labels before the init of any SCCX controller, to be use in case of error (API not responding) during periods retrieving
         $scope.getLabel = function() {
            return oraI18n.setNewLanguage('fr').then(function() {
                oraI18n.getLabelsByCdLang(0, null);
            });
        };

        function activate() {
            cookieService.calcOffset();
            cookieService.checkSession();
            let promises = [];
            promises.push(utils.getExternalApi());
            promises.push($scope.getLabel());
             $q.all(promises).finally( () =>{
                if (utils.externalApi && utils.externalApi != undefined) {
                    appConfig.msccr.redirectionUrl = utils.externalApi;
                }
                $state.go('ora.msccr');
             });
        }

        activate();
        
    }
}