const sccvConstant = {

    InventoryTitleConstant: {
        EVOLUTION: 'Évolution',
        TOTAL: 'Total'
    },
    generalInformation: {
        AUTOMATIC: "automatic"
    },

    reportLifeCycleStatus: {
        ONGOING: "onGoing",
        TOBEVALIDATED: "toBeValidated",
        VALIDATED: "validated"
    },
    frequency: {
        MONTHLY: 'monthly',
        QUARTELY: 'quarterly',
        BIYEARLY: 'biYearly',
        YEARLY: 'yearly'
    },
    typeOfReport: {
        BAN: 'annualReview',
        PERIODIC: 'periodicReport'
    },
    commentFact: {
        'comment': {
            'content': ""
        }
    },
    commentSection: {
        "name": "",
        "comment": {
            "content": ""
        }
    }
}

export default sccvConstant;