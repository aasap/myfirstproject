
// CONSTANTS

import sccvConstant from './sccvConstant.js';
//SERVICES
import comment from './services/comment.js';

//import export2 from './services/export.js';

import {ims} from '@05o/libraries';
import {aar} from '@05o/libraries';
import {loadData} from  '@05o/libraries'; 
import oraLocalStorage from './services/oraLocalStorage.js';
import oraTips from './services/oraTips.js';

import {perimeter} from '@05o/libraries';
import {reports}  from '@05o/libraries'; 
import {subscription} from '@05o/libraries';
import validatorService from './services/validator.service.js';
import periodConstant from './services.constant.js';
import {gtr} from '@05o/libraries';
import performanceNew from './services/performance.js';
import explanation from './services/explanation';
import usageData from './services/usagedata';
import {paginationService} from  '@05o/libraries';

//'use strict'; //NOSONAR

/**
 * @ngdoc function
 * @name ora-gui-services
 * @description
 * # ORA Services Module
 */
export default angular
    .module('ora-gui-services', [
        'ora-gui-core'
    ])
    /*.directive({
        'commentdirective': commentDirective
    })*/
    .service({
        'comment': comment,
        'paginationService': paginationService,
        'perimeter': perimeter,
        //'ExportService': export2,
        'ims': ims,
        'loadData': loadData,
        'oraLocalStorage': oraLocalStorage,
        'oratips': oraTips,
        'reports': reports,
        'subscription': subscription,
        'validator': validatorService,
        'gtrService': gtr,
        'aarService': aar,
        'performanceNew': performanceNew,
        'explanation': explanation,
        'usagedata': usageData
    })
    .value(periodConstant)
    .constant(sccvConstant);