import commentView from './comment.html';

/**
 * @ngdoc directive
 * @name ora-gui-sccr.directive:comment
 * @restrict 'E'
 * @scope
 * @description
 * Affiche les commentaires
 * @example
 *   <div comment type-section="INVENTORY"> </div>
 */

const commentDirective = function(modal, period, user, $filter, oraI18n, comment, utils, message) {
    'ngInject';
    return {
        templateUrl: commentView,
        scope: {
            typeSection: '@'
        },
        replace: true,
        restrict: 'A',

        link: function(scope) {
            scope.comments = comment.comments;
            scope.utils = utils;
            scope.commentMTPMessage = message.MTP_MESSAGE('l\'édition de commentaire');

            scope.titreSection = '';
            angular.forEach(comment.types, function(type) {
                if (type && type.name && scope.typeSection === type.name) {
                    scope.titreSection = type.title;
                }
            });

            function getPopOverMessage() {
                if (scope && scope.commentSection) {
                    let label = scope.commentSection.allianceCode === 'AUTO' ? oraI18n.oraI18nLabels.commentAutoUpdLabel : oraI18n.oraI18nLabels.commentManualUpdLabel;
                    let date = $filter('date')(scope.commentSection.commentDate, 'dd/MM/yyyy');
                    label = label ? label.replace('{date}', date) : '';
                    scope.messagePopOver = label.replace('{user}', scope.commentSection.userFirstName + ' ' + scope.commentSection.userLastName);
                }
            }

            scope.$watch(() => oraI18n.updateLabels, function() {
                if (scope.commentSection && scope.commentSection.comment) {
                    scope.commentFormated = scope.commentSection.comment;
                    getPopOverMessage();
                } else {
                    scope.commentFormated = null;
                    scope.messagePopOver = null;
                }
            });

            scope.$watch('comments.' + scope.typeSection, function(value) {
                scope.commentSection = value;

                if (scope.commentSection && scope.commentSection.comment) {
                    scope.commentFormated = scope.commentSection.comment;
                    getPopOverMessage();
                } else {
                    scope.commentFormated = null;
                    scope.messagePopOver = null;
                }
            });

            scope.displayNoCommentMessage = function() {
                return (scope.typeSection === 'FACT' || !scope.isPerodValidated()) && scope.isUserIntern();
            };

            scope.isUserIntern = function() {
                return user.userLogged.internal;
            };

            scope.isPerodValidated = function() {
                //return period.isValidated(period.selected);
            };

            scope.editComment = function() {
                var modalInstance = modal.comments(
                    scope.titreSection,
                    scope.commentSection ? scope.commentSection.comment : '',
                    scope.typeSection);
                modalInstance.result.then(function() {
                    comment.getComments();
                });
            };
        }
    };
}

export default commentDirective;