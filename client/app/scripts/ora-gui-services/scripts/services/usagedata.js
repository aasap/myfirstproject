export default class usagedata {

    /**
     * @ngdoc service
     * @name ora-gui-sccr.usagedata
     * @requires ora-gui-sccr.rest
     * @requires ora-gui-sccr.oraRest
     * @description # usagedata Service in the ora-gui-sccr.
     */

    constructor($filter, sectionNamesConstant, indicatorsName, dimensionValuesConstant, $q, utils, appConfig, oraRest, reports, reporTypeConst, subscription, message) {
        'ngInject';

        var service = this;
        service.conversionBase = 1000;
        service.topCharges = [];

        service.buildUsageAccesRefData = (report) => {
            if (!report) return;
            let section = _.find(report.sections, (s) => {
                return s.name === sectionNamesConstant.USAGE.name;
            });

            if (!section) return;
            let loadDataIndicator = _.find(section.indicators, (indic) => {
                return indic.name === indicatorsName.SCCR_DATA_LOAD;
            });

            let volumeDataIndicatorTmp = _.find(section.indicators, (indic) => {
                return indic.name === indicatorsName.SCCR_DATA_VOLUME;
            });
            loadDataIndicator.volumIndicator = volumeDataIndicatorTmp;

            let volumeDataIndicatorDimensionValues = (volumeDataIndicatorTmp.dimension) ? angular.copy(volumeDataIndicatorTmp.dimension.values) : [];

            let histoindex = 0;
            if (loadDataIndicator.dimension) {
                angular.forEach(loadDataIndicator.dimension.values, (accessRef) => {
                    accessRef.volumeMonthlyHistory = [];
                    accessRef.volumeMonthlyHistoryConverted = [];
                    let index = _.findIndex(volumeDataIndicatorDimensionValues, (value) => {
                        return value.dimensionValue === accessRef.dimensionValue;
                    });
                    let isVolumetryApplicable = false;
                    if (index >= 0) {
                        let dataScaled = service.scaleUsageVolumetryData(volumeDataIndicatorDimensionValues[index].monthlyHistory);
                        accessRef.volumeMonthlyHistory = volumeDataIndicatorDimensionValues[index].monthlyHistory;
                        accessRef.volumeMonthlyHistoryConverted = dataScaled.monthlyHistory
                        accessRef.volumetryTitle = volumeDataIndicatorTmp.dimension.dimensionIndicatorLabel;
                        accessRef.volumetryLegendUnit = dataScaled.legendUnit;
                        isVolumetryApplicable = volumeDataIndicatorDimensionValues[index].applicability.applicable;
                    }
                    accessRef.idHisto = {
                        loadData: 'sccrNewUsageAccessLoad' + histoindex,
                        volumeData: 'sccrNewUsageAccessVolume' + histoindex
                    };
                    histoindex++;
                    accessRef.applicability.applicable = accessRef.applicability.applicable || isVolumetryApplicable;
                });
            } else {
                loadDataIndicator.dimension = {
                    values: []
                };
            }
            angular.forEach(volumeDataIndicatorDimensionValues, (value) => {
                if (_.findIndex(loadDataIndicator.dimension.values, (val) => {
                        return val.dimensionValue === value.dimensionValue;
                    }) < 0 && volumeDataIndicatorTmp.dimensionProvided !== dimensionValuesConstant.NO) {
                    let dataScaled = service.scaleUsageVolumetryData(value.monthlyHistory);
                    value.volumeMonthlyHistory = value.monthlyHistory;
                    value.volumeMonthlyHistoryConverted = dataScaled.monthlyHistory;
                    value.volumetryLegendUnit = dataScaled.legendUnit;
                    value.monthlyHistory = [];
                    value.idHisto = {
                        loadData: 'sccrNewUsageAccessLoad' + histoindex,
                        volumeData: 'sccrNewUsageAccessVolume' + histoindex
                    };
                    value.volumetryTitle = volumeDataIndicatorTmp.dimension.dimensionIndicatorLabel;
                    histoindex++;
                    loadDataIndicator.dimension.values.push(value);
                }
            });
        };

        service.scaleUsageVolumetryData = function(monthlyHistoryPram) {
            let monthlyHistory = angular.copy(monthlyHistoryPram);
            if (monthlyHistory && monthlyHistory[0] && monthlyHistory[0].metrics) {
                var max = 0;
                monthlyHistory.forEach(function(value) {
                    if (value.applicability.applicable) {
                        value.metrics.forEach(function(metric) {
                            if (metric.measure > max) {
                                max = metric.measure;
                            }
                        });
                    }
                });

                var scale = 0;
                while (Math.pow(service.conversionBase, scale) < max) {
                    scale++;
                }

                if (scale > 0) {
                    monthlyHistory.forEach(function(value) {
                        value.metrics.forEach(function(metric) {
                            if (value.applicability.applicable) {
                                metric.measure = metric.measure /
                                    Math.pow(service.conversionBase, scale - 1);
                                metric.measure = Math
                                    .round(metric.measure * 10) / 10;
                            }
                        });
                    });
                }
                let unit = (scale > 0) ? scale - 1 : 0;
                let legendUnit = $filter('unitConverter')(unit, true, true);
                return {
                    monthlyHistory: monthlyHistory,
                    legendUnit: '(' + legendUnit + ')'
                };
            }
            return {
                monthlyHistory: [],
                legendUnit: ''
            };
        }

        service.getTopChargeData = (indicator) => {
           if (utils.sccvApiDisabled || utils.sccrApiDisabled ) {
                return oraRest.get('mocks/mock_topCharge.json').then((result) => {
                        service.topCharges = result.data;
                        return result;
                    },
                    function errorHandler(error) {
                        return $q.reject(error);
                    });

            }
            let type = reporTypeConst.MSCCR;
            let href = indicator && indicator.onPeriod && indicator.onPeriod.information ?
                indicator.onPeriod.information[0].href : null;
            const productId = reports.getSubscription(type).cdRefcom.split('_')[0];
            const ressource = subscription.getApiRessourcePath(reports.getSubscription(type), type);
            const idReport = reports.selected.id;
            if (href && productId && idReport) {
                const url = '/' + appConfig[type].apiName + '/' + productId + ressource + '/reports/'
                 + idReport + '/' + href + '?sort=-durationRate,-meanLoadRate';
                return oraRest.get(appConfig[type].redirectionUrl + appConfig[type].basePath + url,
                    null, message.DATA_LOADING_ERROR + ' Top charge').then((result) => {
                        service.topCharges = result.data;
                        return result;
                    },
                    function errorHandler(error) {
                        return $q.reject(error);
                    });
            }
            return $q.reject();
        }

        return service;
    }
}