export default class oraLocalStorage {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-services.oraLocalStorage
     * @description
     * # oraLocalStorage
     * Service in the ora-gui-services.
     */

    constructor(localStorageService, perimeter, user, $log) {
        'ngInject';
        var service = this;

        function getUser() {
            return (user.userLogged.internal ? '' : '_' + user.userLogged.allianceCode);
        }

        function getPerimeterKey() {
            return 'selectedPerimeter' + getUser();
        }

        function getContractKey() {
            return 'selectedContractPerimeter#' + perimeter.selectedPerimeter.bidPerimeter + getUser();
        }

        service.isSaveContract = true;

        service.savePerimeter = function() {
            $log.info('Save perimeter', perimeter.selectedPerimeter);
            return localStorageService.set(getPerimeterKey(), perimeter.selectedPerimeter);
        };

        service.loadPerimeter = function() {
            var perimeter = localStorageService.get(getPerimeterKey());
            $log.info('Load perimeter', perimeter);
            return perimeter;
        };

        service.saveContract = function(selectedSubscription) {
            if (service.isSaveContract) {
                $log.info('Save contract', selectedSubscription);
                selectedSubscription.date = Date.now();
                return localStorageService.set(getContractKey(), selectedSubscription);
            }
        };

        service.loadContract = function() {
            var contract = localStorageService.get(getContractKey());
            $log.info('Load contract', contract);
            return contract;
        };

        return service;
    }
}