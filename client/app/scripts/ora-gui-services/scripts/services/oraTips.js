export default class oratips {

    //'use strict'; //NOSONAR


    constructor(appConfig, rest, oraRest) {
        'ngInject';
        var service = this;

        service.baseUrl = appConfig.urlApi + rest.SCCR + rest.TOOLTIPS;
        service.getTooltips = function () {
            return oraRest.get(service.baseUrl).then(function (results) {
                return results.data;
            });
        }
        return service;
    }
}