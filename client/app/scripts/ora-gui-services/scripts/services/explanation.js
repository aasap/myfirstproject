export default class explanation {

    /**
     * @ngdoc service
     * @name ora-gui-services.explanation
     * @requires ora-gui-services.rest
     * @requires ora-gui-services.$http
     * @description
     * # explanation
     * Service in the ora-gui-services.
     */

    constructor(oraRest, appConfig, rest, message, $q) {
        'ngInject';

        var service = this;

        /**
         * @ngdoc property
         * @name ora-gui-services.explanation.#baseUrl
         * @propertyOf ora-gui-services.explanation
         * @description
         * Url base for explanation
         */
        var baseUrl = appConfig.urlApi + rest.EXPLANATION;
        var checkUrl = appConfig.urlApi + rest.EXPLANATION + '/checkExplanation';

        /**
         * @ngdoc property
         * @name explanation
         * @propertyOf ora-gui-services.explanation
         * @description
         *
         */
        service.explanationKpis = [];

        service.explanations = [];

        /**
         * @ngdoc
         * @name ora-gui-services.explanation#checkExplanations
         * @methodOf ora-gui-services.explanation
         * @description
         * Get the all the explanation for OR&A
         * @example
         * checkExplanations().then(function(data){ };
         * @returns {HttpPromise} resolve with fetched data, or fails without error description.
         */
        service.checkExplanations = function(reportId) {
            return oraRest.get(checkUrl + '?bidReport=' + reportId, {},
                'Explication', message.LOADING_ERROR + ' des explications').then(function(result) {
                service.explanationKpis = result.data;
                return result.data;
            }, () => service.explanationKpis = []);
        };

        /**
         * @ngdoc
         * @name ora-gui-services.explanation#getExplanations
         * @methodOf ora-gui-services.explanation
         * @description
         * Get the all the explanation for OR&A
         * @example
         * getExplanations().then(function(data){ };
         * @returns {HttpPromise} resolve with fetched data, or fails without error description.
         */
        service.getExplanations = function(bidReport, cdExplanation) {
            var tmp = null;
            if (service.explanations && service.explanations.length > 0) {
                service.explanations.forEach(explanation => {
                    tmp = (explanation.bidReport === bidReport && explanation.cdExplanation === cdExplanation) ?
                        explanation : null;
                });

                if (tmp && tmp.data) {
                    return $q.resolve(tmp.data);
                }
            }

            return oraRest.get(baseUrl + '/getExplanation?bidReport=' + bidReport + '&cdExplanation=' + cdExplanation, {},
                'Explication', message.LOADING_ERROR + ' des explications').then(function(result) {
                service.explanations.push({
                    bidReport: bidReport,
                    cdExplanation: cdExplanation,
                    data: result.data.lbExplanation
                });
                return result.data.lbExplanation;
            });
        };
    }

}