export default class performance {

    /**
     * @ngdoc service
     * @name ora-gui-services.performanceTable
     * @requires ora-gui-services.rest
     * @requires ora-gui-core.oraRest
     * @description
     * # pai
     * Service for performance table in the ora-gui-services.
     */

    constructor(oraRest, performanceConstant, message, subscription, reports, $q, appConfig, utils) {
        'ngInject';

        var service = this;

        /**
         * @ngdoc property
         * @name tableData
         * @propertyOf ora-gui-services.performanceTable
         * @description
         * Last Data returned by the service getPerformanceTable
         */
        service.tableData = [];

        /**
         * @ngdoc property
         * @name numberOfPages
         * @propertyOf ora-gui-services.performanceTable
         * @description
         * 
         */
        service.numberOfPages = 0;

        /**
         * @ngdoc property
         * @name xTotal
         * @propertyOf ora-gui-services.performanceTable
         * @description
         * 
         */
        service.xTotal = 0;

        /**
         * @ngdoc method
         * @name ora-gui-services.performanceTable#getPerformanceTable
         * @methodOf ora-gui-services.performanceTable
         * @description
         * Get the performance table data for a specific cdRefcom
         * @example
         * getPerformanceTable().then(function(data){ };
         * @return {HttpPromise} resolve with fetched data, or fails without error description.
         */
        service.getPerformanceTable = (href, limit, offset, filter) => {
            service.tableData = [];
            let limitPath = limit ? '&limit=' + limit : '';
            let offsetPath = offset ? '&offset=' + offset : '';
            let filterPath = filter ? filter : '';
           
            if(utils.sccrApiDisabled ){

                let num = offset ? offset/4 : 0;
                return oraRest.get("mocks/mockPerfDetails"+num+".json").then((result) => {
                        let xTotal = 80; 
                        result.numberOfPages = Math.ceil(xTotal / limit);
                        service.tableData = result.data;
                        return result.data;
                    },
                    function errorHandler(error) {
                        return $q.reject(error);
                    });
    
                }
            
            let productId = subscription.selectedSubscription.cdRefcom;
            let idReport = reports.selected.id;
           
            if (href && productId && idReport) {
                let url = '/msccr/' + productId + '/reports/' + idReport + '/' + href + '?' + filterPath + limitPath + offsetPath;
                return oraRest.get(appConfig["msccr"].redirectionUrl + appConfig["msccr"].basePath + url,
                    performanceConstant.TITLE, message.DATA_LOADING_ERROR + ' ' + message.PERIOD_DETAILS).then((result) => {
                        // Store the total of values for excelExport
                        service.xTotal = result.headers(["x-total-count"]);
                        service.numberOfPages = Math.ceil(service.xTotal / limit);
                        service.tableData = result.data;
                        return result.data;
                    },
                    function errorHandler() {
                        service.numberOfPages = 0;
                        service.tableData = [];
                        return $q.when();
                    });
            }

            return $q.when();
        };

        service.getAllPerformanceTable = (href) => {
            return service.getPerformanceTable(href).then((data) => {
                if (data && data.length >= service.xTotal) {
                    return data;
                } else if (service.xTotal && service.xTotal > 0) {
                    return service.getPerformanceTable(href, service.xTotal);
                }

                return [];
            });
        };

        service.getUrlPerformanceDetailTable = () => {
            const report = reports.report;
            let href = undefined;

            const qualityOfService = report ? _.filter(report.sections, {
                "name": "qualityOfService"
            })[0] : null;

            if (qualityOfService && qualityOfService.indicators) {

                const performanceRoundTripDelay = _.filter(qualityOfService.indicators, {
                    "name": "performanceRoundTripDelay"
                })[0];

                const performanceJitter = _.filter(qualityOfService.indicators, {
                    "name": "performanceJitter"
                })[0];

                const performancePacketLossRate = _.filter(qualityOfService.indicators, {
                    "name": "performancePacketLossRate"
                })[0];

                href = performanceRoundTripDelay && performanceRoundTripDelay.onPeriod && performanceRoundTripDelay.onPeriod.information ? performanceRoundTripDelay.onPeriod.information[0].href : null;
                href = performanceJitter && performanceJitter.onPeriod && performanceJitter.onPeriod.information ? performanceJitter.onPeriod.information[0].href : href;
                href = performancePacketLossRate && performancePacketLossRate.onPeriod && performancePacketLossRate.onPeriod.information ? performancePacketLossRate.onPeriod.information[0].href : href;
            }

            return href;
        }

    }
}