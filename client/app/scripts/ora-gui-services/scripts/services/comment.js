export default class comment {

    /**
     * @ngdoc service
     * @name ora-gui-sccr.comment
     * @requires ora-gui-sccr.rest
     * @requires ora-gui-sccr.$http
     * @description
     * # comment
     * Service in the ora-gui-sccr.
     */

    constructor(oraRest, rest, appConfig,  message, googleAnalytics) {
        'ngInject';

        /**
         * @ngdoc property
         * @name ora-gui-sccr.comment.#baseUrl
         * @propertyOf ora-gui-sccr.comment
         * @description
         * Url de base pour SCCR comment
         */
        var baseUrl = appConfig.urlApi + rest.SCCR + rest.COMMENT;

        var service = this;

        service.comments = [];

        service.types = [{
                name: 'FACT',
                title: 'HIGHLIGHTS_LONGLABEL'
            },
            {
                name: 'DECISION',
                title: 'Vous nous avez dit, nous avons décidé'
            },
            {
                name: 'INVENTORY',
                title: 'SEC_INV_SHORTLABEL'
            },
            {
                name: 'QUALITY',
                title: 'SEC_QOS_SHORTLABEL'
            },
            {
                name: 'USAGE',
                title: 'SEC_USAGE_DATA_SHORTLABEL'
            },
            {
                name: 'INCIDENT',
                title: 'SEC_INC_SHORTLABEL'
            }
        ];

        /**
         * @ngdoc method
         * @name ora-gui-sccr.comment#getComments
         * @methodOf ora-gui-sccr.comment
         * @description
         * Method to get comments
         * @param {int} periodId ID of the concerned period
         * @returns {HttpPromise} resolve with fetched data, or fails without error description.
         */
        service.getComments = function() {
            return oraRest.get(baseUrl, {
                params: {
                    'period': '123'
                }
            }, 'Commentaires', message.ERROR + 'des commentaires.').then(function(result) {
                angular.forEach(service.types, function(value) {
                    service.comments[value.name] = null;
                });
                angular.forEach(result.data, function(value) {
                    service.comments[value.type] = value ? value : {};
                });
            });
        };

        /**
         * @ngdoc method
         * @name updateComment
         * @methodOf ora-gui-sccr.comment
         * @description
         * Method to update comment
         * @param {int} periodId ID of the concerned period
         * @param {string} comment comment
         * @param {String} type type
         * @returns {HttpPromise} resolve with fetched data, or fails without error description.
         */
        service.updateComment = function(periodId, type, comment) {
            let eventName = type === 'FACT' ? 'Editer faits marquants' : 'Editer commentaire section';
            googleAnalytics.logViewChange(eventName, 'Reporting', 'Connectivité');
            return oraRest.put(baseUrl, comment, {
                params: {
                    'period': periodId,
                    'type': type
                }
            }, null, null, null, null, message.MTP_MESSAGE('l\'édition de commentaire'));
        };

        //TO be repalced with api call for fetching the sccv comments 
        service.setSccvComments = function() {
            angular.forEach(service.types, function(value) {
                service.comments[value.name] = null;
            });
        };
        return service;
    }
}