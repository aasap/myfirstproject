export default class validator {

    //'use strict'; //NOSONAR

    /**
     * @ngdoc service
     * @name ora-gui-personalisation.personalisation
     * @description
     * # personalisation
     * Service for personalisation in the ora-gui-personalisation.
     */

    constructor(regexp, toaster) {
        'ngInject';
        var service = this;

        /**
         * @ngdoc method
         * @name validateEdit
         * @methodOf ora-gui-personnalisation.controller:PersonnalisationgarVpnCtrl
         * @description
         * True if customThreshold of row.newValues is null || match with percent regexp  ELSE FALSE
         */
        service.validateGarPaiEdit = function (row) {
            if (row.newValues.customThreshold === null) {
                return true;
            }
            row.newValues.customThreshold = row.newValues.customThreshold.replace(',', '.');
            if (row.newValues.customThreshold.match(regexp.PERCENT)) {
                return true;
            }
            toaster.pop('error', 'La saisie est incorrecte le format attendu est xxx.xx et doit etre compris entre 0 et 100');
            return false;
        };

        return service;

    }
}