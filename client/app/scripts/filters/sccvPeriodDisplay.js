/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:sccvperiodDisplay
 * @description
 * # sccvperiodDisplay
 * Filter in the ora-internal-gui.
 */

import sccvConstant from "../ora-gui-services/scripts/sccvConstant";
const sccvperiodDisplay = function($filter) {
    'ngInject';
    return function(report, fullDate) {
        if (report && report.period) {

            //let HourInMS = 3600000 * 24;
            let periodFrom = new Date(report.period.monthStart);
            let periodTo = new Date(report.period.monthEnd);
            //- HourInMS;
            let month = new Date(periodTo).getMonth() + 1; // getMonth() give the month number [0-11], so +1 to have [1-12]
            let prefix;
            let nbMonth;
            let date;

            /*
            Period to is always one day before so the expected display day
            Time is in ms so need to
            */

            if (report.type === sccvConstant.typeOfReport.BAN) {
                date = $filter('date')(periodTo, fullDate ? 'yyyy' : 'yy', '+0');
            } else if (report.period.frequency === sccvConstant.frequency.MONTHLY) {
                date = $filter('date')(new Date(periodFrom), fullDate ? 'MMMM yyyy' : 'MMM yy', '+0');
            } else {
                date = $filter('date')(periodTo, 'yyyy', '+0');
            }

            if (report.period.frequency === sccvConstant.frequency.QUARTELY) {
                prefix = 'T';
                nbMonth = 3;
            } else if (report.period.frequency === sccvConstant.frequency.BIYEARLY) {
                prefix = 'S';
                nbMonth = 6;
            }
            return (prefix ? prefix + '' + (Math.ceil((month / nbMonth))) + ' ' : '') + date;
        }
    };
};

export default sccvperiodDisplay;