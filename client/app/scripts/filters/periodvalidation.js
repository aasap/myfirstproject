/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:periodValidation
 * @description
 * # periodValidation
 * Filter in the ora-internal-gui.
 */
const periodValidation = function($filter, oraI18n, period, periodConstant, subscription) {
    'ngInject';
    return function(selectedPeriod) {
        var label = oraI18n.oraI18nLabels.onGoing;
        var dateDifferenceMax = 10;
        if (selectedPeriod) {
            if (period.isValidated(selectedPeriod)) {
        	let localDate = moment(selectedPeriod.validationDate).format();
                var valDate = $filter('date')(localDate, 'dd MMMM');

                let labelTmp = selectedPeriod.validationType === '1' ? oraI18n.oraI18nLabels.manualValidLabel : oraI18n.oraI18nLabels.autoValidLabel;
                let labelTmp2 = labelTmp ? labelTmp.replace('{date}', valDate) : '';
                label = labelTmp2 && labelTmp2.indexOf('{csm}') > -1 ? labelTmp2.replace('{csm}', selectedPeriod.validatorFirstName + ' ' + selectedPeriod.validatorLastName) : labelTmp2;
            } else if (period.isToBeValidated(selectedPeriod)) {
                var a = moment();
                //to = dtEnd current period
                //remove one month to get the last month of the current period
                //go to end of this month
                //add val duration of the selected subscription
                var b = moment(moment(selectedPeriod.to)
                    .subtract(1, 'month')
                    .endOf('month')
                    .add(
                        subscription.selectedSubscription.valDuration,
                        'days'));
                var dateDifference = b.diff(a, 'days');

                if (dateDifference >= dateDifferenceMax) {
                    let labelTmp = oraI18n.oraI18nLabels.autoValidWarnShortLabel;
                    label = labelTmp ? labelTmp.replace('{days}', dateDifference) : '';
                } else {
                    let labelTmp = oraI18n.oraI18nLabels.autoValidWarnLongLabel;
                    label = labelTmp ? labelTmp.replace('{days}', dateDifference) : '';
                }

                if (dateDifference === 1) {
                    label = label ? label.replace('days', 'day') : '';
                    label = label ? label.replace('jours', 'jour') : '';
                }
            }
        }
        return label;
    };
}

export default periodValidation;