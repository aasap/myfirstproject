/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:colorPerso
 * @description
 * # colorPerso
 * Filter in the ora-internal-gui.
 */
const colorPerso = function ($sce) {
    'ngInject';
    return function (data) {
        var color = (data === 'personnalisé') ? 'darkviolet' : '#666';
        var res = '<span style="color:' + color + '">';
        res += data + '</span>';
        var htmlString = $sce.trustAsHtml(res);
        return htmlString;
    };
}

export default colorPerso;