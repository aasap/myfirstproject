/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:capitalize
 * @description capitalize first letter
 * # capitalize
 * Filter in the ora-internal-gui.
 */
const capitalize = function () {
    return function (input) {
        if (input) {
            input = input.toLowerCase().trim();
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
    };
};

export default capitalize;
