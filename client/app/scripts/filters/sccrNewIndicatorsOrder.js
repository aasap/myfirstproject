/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:sccrNewIndicatorsOrder
 * @description return a list of indicators in the right order
 * # sccrNewIndicatorsOrder
 * Filter in the ora-internal-gui.
 */

function checkOrder(data, orderedArray) {
  let tmp = [];
  _.forEach(orderedArray, function (indicatorName) {
    _.forEach(data, function (indicator) {
      if (indicatorName === indicator.name) {
        tmp.push(indicator);
      }
    });
  });
  return tmp;
}

const sccrNewIndicatorsOrder = function () {
  'ngInject';


  let inventoryIndicators = ['SCCROffersEvolution'];

  let incidentIndicators = ['incidentsWithServiceInterruptionNumber'];

  let usageIndicators = ['SCCRDataVolume','SCCRDataLoad'];  
 
  let qosIndicators = ['globalVpnInternetAvailabilityRate', 'globalEthernetAvailabilityRate', 'accessAvailabilityRate', 'GTTRFulfilment', 'proactivityRate','MSIFulfilment', 'performance'];

  return function (indicators, sectionName) {

    if (sectionName === 'inventory') {
      indicators = checkOrder(indicators, inventoryIndicators);
    }
    if (sectionName === 'incident') {
      indicators = checkOrder(indicators, incidentIndicators);
    }
    if (sectionName === 'usage') {
      indicators = checkOrder(indicators, usageIndicators);
    }
    if (sectionName === 'qualityOfService') {
      indicators = checkOrder(indicators, qosIndicators);
    } 

    return indicators;
  };
};

export default sccrNewIndicatorsOrder;
