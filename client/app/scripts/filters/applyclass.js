/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:applyClass
 * @description Apply class options on row
 * # applyClass
 * Filter in the ora-internal-gui.
 */
const applyClass = function () {
    return function (value, row) {
        var classes = {};
        for (var attr in value) {
            classes[attr] = value[attr](row);
        }
        return classes;
    };
}

export default applyClass;
