/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:sccrIndicatorApplicability
 * @description return boolean
 * # sccrIndicatorApplicability
 * Filter in the ora-internal-gui.
 */

const sccrIndicatorApplicability = function (indicatorsName) {
  'ngInject';

  return function (indicator) {
   return indicator.applicability ? (indicator.applicability.applicable? (indicator.applicability.applicable) : 
   ((indicator.name === indicatorsName.SCCR_GAR_ETHERNET ||indicator.name === indicatorsName.SCCR_GAR_VPN ||indicator.name === indicatorsName.SCCR_IMS) ? true :
    indicator.applicability.applicable)) : false;     
  };
};

export default sccrIndicatorApplicability;