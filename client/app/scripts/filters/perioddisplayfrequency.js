/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:periodDisplayFrequency
 * @description
 * # periodDisplayFrequency
 * Filter in the ora-internal-gui.
 */
const periodDisplayFrequency = function () {
  return function (value,isSubscription) {
    var frequency = {
      MEN: 'Mensuelle',
      TRI: 'Trimestrielle',
      SEM: 'Semestrielle',
      ANN: 'Annuelle'
    };

    if (value) {
	if(isSubscription){
	   return frequency[value.lbFrequency];
	}
      return frequency[value.frequency];
    }
  };
}

export default periodDisplayFrequency;
