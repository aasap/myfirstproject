/**
 * @ngdoc filter
 * @name ora-internal-gui.filter:sectionsOrder
 * @description return a list of sections in the right order
 * # sectionsOrder
 * Filter in the ora-internal-gui.
 */
const sectionsOrder = function () {
  'ngInject';

  let sectionsOrdered = ['inventory', 'incident', 'qualityOfService', 'usage'];

  return function (sections) {
    let tmp = [];
    _.forEach(sectionsOrdered, function (SectionName) {
      _.forEach(sections, function (section) {
        if (SectionName === section.name) {
          tmp.push(section);
        }
      });
    });
    return tmp;
  };
};

export default sectionsOrder;
