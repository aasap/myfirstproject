export default class SccvCtrl {

    /**
     * @ngdoc controller
     * @name ora-gui-sccv.controller:SccvCtrl
     * @requires ora-gui-sccv.reports
     * @description
     * # gui.controller:sccvCtrl
     * Controller of the ora-gui-sccv
     */

    constructor($scope, user, reports, oraI18n, comment, typeOfMonthlyData, indicatorsName, usagedata, sectionNamesConstant, utils, modal) {
        'ngInject';

        var vm = this;
        vm.user = user;
        //vm.control = control;
        vm.reports = reports;
        vm.indicatorsName = indicatorsName;
        vm.apiUnavailableMessage = '';
        /*
         * Watch for check change on reports.report value
         * Store modification add
         */
        vm.report = reports.report;

        vm.recalculating;

        comment.setSccvComments();
        //vm.tooltips = tooltipsPreload;
        vm.messages = {
            noReportAvailableMsg: reports.reportAvailabilityMsg,
            personnalisationAction: `Suite à une action de personnalisation, le calcul des indicateurs est en cours.
		Veuillez patienter quelques instants.`,
            subscriptionInvisible: `Le tableau de bord Service Client Conseil Réseaux est en cours de développement.
		Votre Responsable Service Client vous informera de sa mise à disposition.`
        };
       
        $scope.$watch(() => utils.onPersoNewCalculationStatus, function(newValue) {
            if (newValue) {
                vm.recalculating = utils.getCurrentKpiStatus("INC");
            }
        });

        $scope.$watch(() => reports.report, function(newValue, oldValue) {
            
            if (newValue) {
                oraI18n.setNewLanguage(newValue.language).then(function() {
                    newValue.period ? oraI18n.getLabelsByCdLang(newValue.period.monthStart, newValue.period.monthEnd) : oraI18n.getLabelsByCdLang(0, null);
                    if (newValue !== oldValue) {
                        vm.report = reports.report;
                        vm.buildUsageAccesRefData();
                        vm.buildPerformanceData();
                    }
                });
                const qosSection = _.find(vm.reports.report.sections, (s) => {
                    return s.name === "qualityOfService";
                });
                let globalVpn = null;
                let globalEthernet = null;
                if(qosSection)
                {
                    globalVpn = _.find(qosSection.indicators, (s) => {
                        return (s.name === 'globalVpnInternetAvailabilityRate'  && s.onPeriod && s.onPeriod.applicability && s.onPeriod.applicability.applicable);
                    });

                    globalEthernet = _.find(qosSection.indicators, (s) => {
                        return (s.name === 'globalEthernetAvailabilityRate'  && s.onPeriod && s.onPeriod.applicability && s.onPeriod.applicability.applicable);
                    });
                }
                vm.summaryConfiguration = [
                    {
                        "section": "qualityOfService",
                        "indicatorName": !globalVpn && globalEthernet ? globalEthernet.name : "globalVpnInternetAvailabilityRate",
                        "typeOfData": typeOfMonthlyData.INDICATOR,
                        "longLabel": true
                    },
                        {
                            "section": "qualityOfService",
                            "indicatorName": "GTTRFulfilment",
                            "typeOfData": typeOfMonthlyData.INDICATOR
                        },
                        {
                            "section": "qualityOfService",
                            "indicatorName": "MSIFulfilment",
                            "typeOfData": typeOfMonthlyData.INDICATOR
                        },
                        {
                            "section": "qualityOfService",
                            "indicatorName": "proactivityRate",
                            "typeOfData": typeOfMonthlyData.INDICATOR,
                            "longLabel": true
                        },
                        {
                            "section": "inventory",
                            "indicatorName": "SCCRSitesEvolution",
                            "typeOfData": typeOfMonthlyData.INDICATOR
                        },
                        {
                            "section": "incident",
                            "indicatorName": "incidentsWithServiceInterruptionNumber",
                            "typeOfData": typeOfMonthlyData.INDICATOR
                        },
                        {
                            "section": "usage",
                            "indicatorName": "SCCRDataVolume",
                            "typeOfData": typeOfMonthlyData.METRICS,
                            "metricsName": "outVolume",
                            "checkIsSubcribed": true,
                            "conversionFilter": 'unitConverter'
                        },
                        {
                            "section": "usage",
                            "indicatorName": "SCCRDataVolume",
                            "typeOfData": typeOfMonthlyData.METRICS,
                            "metricsName": "inVolume",
                            "checkIsSubcribed": true,
                            "conversionFilter": 'unitConverter'
                        }
                    ];
            }
        });

        vm.buildPerformanceData = () => {
            if (vm.report && _.find(vm.report.sections, (s) => {
                    return s.name === "qualityOfService";
                })) {
                if (!vm.report) return;
                let section = _.find(vm.report.sections, (s) => {
                    return s.name === "qualityOfService";
                });

                if (!section) return;
                let performanceIndicator = _.find(section.indicators, (indic) => {
                    return indic.name === "performance";
                });
                if (!performanceIndicator) {
                    let roundTripIndicator = _.find(section.indicators, (indic) => {
                        return indic.name === "performanceRoundTripDelay";
                    });
                    let packetLossIndicator = _.find(section.indicators, (indic) => {
                        return indic.name === "performancePacketLossRate";
                    });
                    let jitterIndicator = _.find(section.indicators, (indic) => {
                        return indic.name === "performanceJitter";
                    });
                    if (roundTripIndicator || packetLossIndicator || jitterIndicator) {
                        section.indicators.push({
                            "name": "performance",
                            "shortLabel": "Performance",
                            "longLabel": "Performance",
                            "applicability": {
                                "applicable": ((roundTripIndicator.applicability.applicable && roundTripIndicator.onPeriod.applicability.applicable) || (packetLossIndicator.applicability.applicable && packetLossIndicator.onPeriod.applicability.applicable) || (jitterIndicator.applicability.applicable && jitterIndicator.onPeriod.applicability.applicable))
                            }
                        })
                    }
                }

            }
        }

        vm.buildUsageAccesRefData = () => {
            if (vm.report && _.find(vm.report.sections, (s) => {
                    return s.name === sectionNamesConstant.USAGE.name;
                })) {
                usagedata.buildUsageAccesRefData(vm.report);
            }
        };
  
        /*$scope.$watch(function(){
            return utils.onWaitingPersoNew;
        }, function(val, oldVal) {
            if (!val || _.isEqual(val, oldVal) || utils.disableInput) {
                return;
            }

            if (val.length > 0 && user.userLogged.internal && (reports.selected != null && reports.selected.lifecycleState !== 'validated')) {
                modal.onDemandPerso();
               
            }
        });*/

        $scope.$watch(() => oraI18n.oraI18nLabels.unavailableAPIerrorMessag, function(value) {
            if (reports.isAPInotResponding) {
                vm.apiUnavailableMessage = value;
            }
        });

        $scope.$watch(() => reports.isAPInotResponding, function(value) {
            if (value) {
                vm.apiUnavailableMessage = oraI18n.oraI18nLabels.unavailableAPIerrorMessag;
            }
        });

        vm.buildUsageAccesRefData();
        vm.buildPerformanceData();
    }
}