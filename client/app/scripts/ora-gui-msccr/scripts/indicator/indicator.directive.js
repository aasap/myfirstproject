'use strict'; //NOSONAR
import indicatorView from './indicator.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraIndicator
 */

const oraIndicator = function(dimensionValuesConstant, inapplicabilityReasonConstant, utils) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: indicatorView,
        scope: {
            indicator: '=indicator',
            section : '=section',
            frequency:'=frequency'
        },
    	link: function(scope){
            scope.dimensionValuesConstant = dimensionValuesConstant;
            scope.inapplicabilityReasonConstant = inapplicabilityReasonConstant;
            scope.garTitle = function(indicatorVal) {
                return (indicatorVal.applicability && indicatorVal.applicability.applicable === false && indicatorVal.applicability.inapplicabilityCode === scope.inapplicabilityReasonConstant.NOT_SUBSCRIBED);
            };

            if (scope.indicator.name === "MSIFulfilment"){
              scope.recalculating = utils.getCurrentKpiStatus("IMS");
            } else if (scope.indicator.name === "proactivityRate"){
              scope.recalculating = utils.getCurrentKpiStatus("PAI");
            } else if (scope.indicator.name === "accessAvailabilityRate"){
              scope.recalculating = utils.getCurrentKpiStatus("AAR");
            } else if (scope.indicator.name === "globalVpnInternetAvailabilityRate"){
              if (utils.isRecalculatingDisplayedForInternet === false){
                utils.isRecalculatingDisplayedForInternet = true;
              }
              scope.recalculating = utils.getCurrentKpiStatus("GAR");
            } else if (scope.indicator.name === "globalEthernetAvailabilityRate"){
              if (utils.isRecalculatingDisplayedForInternet === false){
                scope.recalculating = utils.getCurrentKpiStatus("GAR");
              }
            }
    	}
    };
};

export default oraIndicator;
