'use strict'; // NOSONAR
//ROUTES
import mSccrRoute from './msccr.route';

//CONTROLLERS
import mSccrCtrl from './msccr.controller';
//DIRECTIVES
import oraIndicatorSccr from './indicator/indicator.directive';
import {imsTable} from  '@05o/libraries';
import {aarTable} from  '@05o/libraries';
import oraSccrQosIms from './qos/ims/ims.directive';
// INCIDENTS
import oraIncidentsIndicator from './incident/incident.directive';
//Inventory 
import oraParcOptions from './inventory/options/options.directive';
import oraParcOffers from './inventory/offers/offers.directive';


import {gtrTable} from '@05o/libraries';

//USAGE VOLUME DATA

import oraSccrDataVolume from './usage/volumetry/sccrDataVolum';

//USAGE DATA LOAD

import oraSccrDataLoad from './usage/accessLoad/sccrDataLoad';

//QOS

import oraQosGarInternet from './qos/gar/vpnInternet/garInternet.directive';
import oraQosGarEthernet from './qos/gar/ethernet/garEthernet.directive';
import paiIndicator from './qos/pai/paiIndicatorDirective';
import oraQosAar from './qos/aar/aar.directive';
import oraQosGttr from './qos/gttr/gttr.directive';



import performanceTable from './qos/performance/performanceTableDirective';
import oraPerfoIndicators from './qos/performance/indicators/perfoIndicators.directive';

import constants from './msccr.constant.js';
/**
 * @ngdoc function
 * @name ora-gui-sccv
 * @description
 * # ora-gui-sccv
 * SCCV Module
 */

export default angular
    .module('ora-gui-msccr', [
        'ora-gui-services'
    ]).constant(constants)
    .directive({
        'oraIndicatorSccr': oraIndicatorSccr,
        'oraParcOptions': oraParcOptions,
        'oraParcOffers': oraParcOffers,
        'oraIncidentsIndicator': oraIncidentsIndicator,
        'oraSccrDataVolume': oraSccrDataVolume,
        'oraSccrDataLoad': oraSccrDataLoad,
        'oraQosGarInternet': oraQosGarInternet,
        'oraQosGarEthernet': oraQosGarEthernet,
        'oraQosAar': oraQosAar,
        'paiIndicator': paiIndicator,
        'oraQosGttr': oraQosGttr,
        'oraSccrQosIms': oraSccrQosIms,
        'performanceTable': performanceTable,
        'oraPerfoIndicators': oraPerfoIndicators,
        'imsTable': imsTable,
        'gtrTable': gtrTable,
        'aarTable' : aarTable
    })
    .controller({
        'mSccrCtrl': mSccrCtrl
    })
    .config(mSccrRoute)
    .name;