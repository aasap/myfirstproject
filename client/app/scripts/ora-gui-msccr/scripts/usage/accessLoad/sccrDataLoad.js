'use strict'; //NOSONAR
import oraSccrDataLoadView from './sccrDataLoad.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraSccrDataLoad
 */

const oraSccrDataLoad = function(dimensionValuesConstant, oraI18n, $filter) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraSccrDataLoadView,
        scope: {
            indicator: '=indicator'
        },
        link: function(scope) {
            scope.$watch(
                () => scope.indicator.dimension,
                (value) => {
                    scope.indicator.dimension = value;
                    scope.indicator.dimension.values = $filter('orderBy')(scope.indicator.dimension.values, ['dimensionGroup','dimensionValue']);
                }, true);

            scope.usageDataVolumeHistoricOptions = {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-purple',
                seriesClass: [{
                        name: 'outVolume',
                        stroke: 'stroke-brand-dark-blue',
                        typeHisto: 'line'
                    },
                    {
                        name: 'inVolume',
                        stroke: 'stroke-brand-dark-yellow',
                        typeHisto: 'line'
                    }
                ]
            };
            scope.usageStyles = 'usage-table-style';
            scope.dimensionValuesConstant = dimensionValuesConstant;
            scope.legendUnit = '(' + scope.indicator.unit + ')';
            scope.accesRefTitle = oraI18n.oraI18nLabels.accesRefLong;
            scope.$watch(
                    () => oraI18n.oraI18nLabels.accesRefLong,
                    (value) => {
                	scope.accesRefTitle = value;
                    }, true);
            scope.usageDataLoadHistoricOptions = {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-purple',
                seriesClass: [{
                        name: 'outLoad',
                        bgClass: 'bg-brand-dark-blue',
                        fill: 'fill-brand-dark-blue',
                        typeHisto: 'bar'
                    },
                    {
                        name: 'inLoad',
                        bgClass: 'bg-brand-dark-yellow',
                        fill: 'fill-brand-dark-yellow',
                        typeHisto: 'bar'
                    }
                ]
            };

            scope.dataLoadIndicatorInfo = {
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "contractual": scope.indicator.contractual,
                "unit": scope.indicator.unit,
                "tobeConverted": false,
                "conversionFilterName": ''
            };

            scope.dalaVolumeIndicatorInfo = {
                "name": scope.indicator.volumIndicator.name,
                "shortLabel": scope.indicator.volumIndicator.shortLabel,
                "longLabel": scope.indicator.volumIndicator.longLabel,
                "contractual": scope.indicator.volumIndicator.contractual,
                "unit": '',
                "tobeConverted": true,
                "conversionFilterName": 'unitConverter',
                "conversionFilterAddidionalArg": true
            };

            scope.usageDataVolumeConfigurations = [{
                    "name": "outVolume",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
                {
                    "name": "inVolume",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
            ];

            scope.usageDataLoadConfigurations = [{
                    "name": "outLoad",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
                {
                    "name": "inLoad",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
            ];

            scope.isNewRefcom = function(index) {
                if (!scope.indicator.dimension.values[index].dimensionGroup) {
                    return false;
                }
                let res = index > 0 ? scope.indicator.dimension.values[index-1].dimensionGroup !== scope.indicator.dimension.values[index].dimensionGroup : true;
                return res;
            }

            scope.buildRefcomTitle = function(dimensionGroup) {
                return scope.accesRefTitle ? scope.accesRefTitle.replace('{refcom}', dimensionGroup) : '';
            }

        }
    };
};

export default oraSccrDataLoad;