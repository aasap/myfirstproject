'use strict'; //NOSONAR
import oraSccrDataVolumeView from './sccr.data.volume.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraSccrDataVolume
 */

const oraSccrDataVolume = function($filter, usageIndicatorsConstant, usagedata, oraI18n) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraSccrDataVolumeView,
        scope: {
            indicator: '=indicator'
        },
        link: function(scope) {
            scope.maxTopThreshold = '';
            scope.filtredDetailsTabConf = {};
            scope.$watch(
                () => scope.indicator,
                (value) => {
                    scope.indicator.monthlyHistory = value.monthlyHistory;
                    prepareDetailedData();
                    scope.detailsTableConfigurations = scope.getDetailsTableConfig();
                    scope.volumetryDataScaled = usagedata.scaleUsageVolumetryData(scope.indicator.monthlyHistory);
                    if(scope.indicator.onPeriod.information[0] && scope.indicator.onPeriod.information[0].href){
                        usagedata.getTopChargeData(scope.indicator).then(function(res){
                            scope.topcharges = res.data;
                            scope.maxTopThreshold = scope.topcharges.length > 0 ? scope.topcharges[0].maxThreshold : '';
                            setTopChargeFiltredConf();
                        });
                    }
                   
                    formatDetailsTableData();
                }, true);

            scope.$watch(() => oraI18n.updateLabels, function() {
                if (scope.indicator) {
                    prepareDetailedData();
                    formatDetailsTableData();
                    setTopChargeFiltredConf();
                }
            });

            function prepareDetailedData() {
                scope.infoCopy = [];
                scope.infoCopy = angular.copy(scope.indicator.onPeriod.information);
                
                if (!!scope.infoCopy && scope.infoCopy[0].rows) {
                    let titleRow = _.filter(scope.infoCopy[0].rows, {
                        "type": "title"
                    });
                    let wayColValue = _.filter(titleRow[0].cells, {
                        "key": "way"
                    });
                    titleRow[0].cells.push({
                        "key": "wayCopy",
                        "value": wayColValue.value
                    });

                    let contentRows = _.filter(scope.infoCopy[0].rows, {
                        "type": "content"
                    });
                    _.forEach(contentRows, row => {
                        let colway = _.filter(row.cells, {
                            "key": "way"
                        })[0];
                        row.cells.push({
                            "key": "wayCopy",
                            "value": colway.value
                        });
                    });
                }
            }

            scope.historicOptions = {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-purple',
                seriesClass: [{
                        name: 'outVolume',
                        stroke: 'stroke-brand-dark-blue',
                        typeHisto: 'line'
                    },
                    {
                        name: 'inVolume',
                        stroke: 'stroke-brand-dark-yellow',
                        typeHisto: 'line'
                    }
                ]
            };

            scope.indicatorInfo = {
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "contractual": scope.indicator.contractual,
                "unit": scope.indicator.unit,
                "tobeConverted": true,
                "conversionFilterName": 'unitConverter',
                "conversionFilterAddidionalArg": true
            };

            scope.configurations = [{
                    "name": "outVolume",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
                {
                    "name": "inVolume",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "showInapplicabilityReason": true
                },
                
            ];
            function setTopChargeFiltredConf(){
                scope.filtredDetailsTabConf = {
                    headers: [
                        {
                           title: oraI18n.oraI18nLabels.usageTopAccessSiteShort,
                           tooltip: oraI18n.oraI18nLabels.usageTopAccessSiteLong,
                        },
                        {
                            title: oraI18n.oraI18nLabels.usageTopAccessMnemocicShort,
                            tooltip: oraI18n.oraI18nLabels.usageTopAccessMnemocicLong,
                        },
                        {
                            title: oraI18n.oraI18nLabels.usageTopDurationRateShort ? oraI18n.oraI18nLabels.usageTopDurationRateShort.replace('{maxThreshold}',scope.maxTopThreshold) :'',
                            tooltip: oraI18n.oraI18nLabels.usageTopDurationRateLong,
                            cssClass:'bold'
                        },
                        {
                            title: oraI18n.oraI18nLabels.usageTopMeanLoadRateShort ? oraI18n.oraI18nLabels.usageTopMeanLoadRateShort.replace('{maxThreshold}',scope.maxTopThreshold) : '',
                            tooltip: oraI18n.oraI18nLabels.usageTopMeanLoadRateLong,
                            cssClass:'bold'
                        },
                        {
                            title: oraI18n.oraI18nLabels.usageTopOverRunNumberShort,
                            tooltip: oraI18n.oraI18nLabels.usageTopOverRunNumberLong
                        },
                        {
                            title: oraI18n.oraI18nLabels.usageTopMeanDurationMinutesShort,
                            tooltip: oraI18n.oraI18nLabels.usageTopMeanDurationMinutesLong
                        },
                        {
                            title: oraI18n.oraI18nLabels.usageTopMaxLoadRateShort,
                            tooltip: oraI18n.oraI18nLabels.usageTopMaxLoadRateLong
                        },
                        {
                            title: oraI18n.oraI18nLabels.usageTopRecurrenceShort,
                            tooltip: oraI18n.oraI18nLabels.usageTopRecurrenceLong
                        }
                    ],
                    filters: [
                        {
                            key:'OUT',
                            value: oraI18n.oraI18nLabels.usageSentLabel
                        },
                        {
                            key:'IN',
                            value: oraI18n.oraI18nLabels.usageReceivedLabel
                        },
                        {
                            key:'',
                            value: oraI18n.oraI18nLabels.usageSentLabel +' + '+ oraI18n.oraI18nLabels.usageReceivedLabel
                        },
                        
                    ],
                    fields: [
                        {
                            name:'access.site',
                            unit:'',
                            isFunct: true,
                            funct: (obj) => {
                                return obj.access.site;
                            },
                            cssClass:'padding-left'
                        },
                        {
                            name:'access.mnemonic',
                            unit:'',
                            isFunct: true,
                            funct: (obj) => {
                                return obj.access.mnemonic;
                            },
                            cssClass:'padding-left'
                        },
                        {
                            name:'durationRate',
                            unit:' %',
                            isFunct: false,
                            cssClass:'centred-orange bold'
                          
                        },
                        {
                            name:'meanLoadRate',
                            unit:' %',
                            isFunct: false,
                            cssClass:'centred'
                          
                        },
                        {
                            name:'overrunNumber',
                            unit:'',
                            isFunct: false,
                            cssClass:'centred'
                          
                        },
                        {
                            name:'meanDurationInMinutes',
                            unit:'',
                            isFunct: false,
                            cssClass:'centred',
                            filterName: 'timeNumber'
                          
                        },
                        {
                            name:'maxLoadRate',
                            unit:' %',
                            isFunct: false,
                            cssClass:'centred'
                          
                        },
                        {
                            name:'recurrence',
                            unit:'',
                            isFunct: false,
                            cssClass:'centred'
                          
                        },
    
                    ]
                }
            }
 
            scope.usageStyles = 'usage-table-style';
            scope.volumetryDataScaled = usagedata.scaleUsageVolumetryData(scope.indicator.monthlyHistory);
            scope.detailsTableConfigurations = [];

            scope.getDetailsTableConfig = () => {
                var detailsConfigSorted = [];

                if (!!scope.infoCopy && scope.infoCopy[0].rows) {
                    let detailsConfig = _.find(scope.infoCopy[0].rows, (value) => {
                        return value.type === "title";
                    });

                    angular.forEach(scope.detailsTableTitleOrder, function(value) {
                        let tmp = _.find(detailsConfig.cells, (val) => {
                            return val.key === value;
                        });
                        if (tmp.key === 'maximumLoadRate') {
                            tmp.unit = '%';
                        }
                        else if(tmp.key ==='averageDuration'){
                            tmp.filterName = 'timeNumber';
                        }
                        detailsConfigSorted.push(tmp);
                    });
                }
                return detailsConfigSorted;
            };

            scope.sortData = function(data) {
                let index = _.findIndex(data[0], function(val) {
                    return val.cellkey === 'duration';
                });
                if (index < 0) {
                    data.sort();
                } else {
                    data.sort(function(a, b) {
                	if (Number(a[index].cellData) === Number(b[index].cellData)) {
                            //sort by the siteName (index = 0)
                            return a[0].cellData > b[0].cellData ? 1 : -1;
                        }
                        return Number(a[index].cellData) < Number(b[index].cellData) ? 1 : -1;
                    });
                }
                return data;
            };

            function formatDetailsTableData() {
                let toNotBeFormatedKeys = ['siteName','accessName','way'];
                if (!!scope.infoCopy && scope.infoCopy[0].rows) {
                    angular.forEach(scope.infoCopy[0].rows, (row) => {
                        let duration = _.find(row.cells, function(cel) {
                            return cel.key === 'duration';
                        });
                        duration.hidden = true;

                        let wayCopy = _.find(row.cells, function(cel) {
                            return cel.key === 'wayCopy';
                        });
                        wayCopy.hidden = true;

                        if (row.type === 'title') { 
                            angular.forEach(row.cells, (cel) => {
                                 toNotBeFormatedKeys.includes(cel.key) ? cel.isNotRefcom = false : cel.isNotRefcom = true ;
                            });
                        }
                        if (row.type === 'content') {
                            let way = _.find(row.cells, function(cel) {
                                return cel.key === 'way';
                            });
                            way.value = (wayCopy.value === 'OUT') ? oraI18n.oraI18nLabels.usageSentLabel : oraI18n.oraI18nLabels.usageReceivedLabel;
                        }
                    });
                }
            };

            scope.detailsTableTitleOrder = usageIndicatorsConstant.usageDataVolum.detailsTableTitleOrder;
            scope.historicId = 'sccrNewUsageVolumHisto';

        }
    };
};

export default oraSccrDataVolume;