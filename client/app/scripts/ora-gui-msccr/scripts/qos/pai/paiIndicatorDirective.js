'use strict'; //NOSONAR
import paiView from './paiView.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:paiIndicator
 */

const paiIndicator = function(oraI18n, paiConstant) {
    'ngInject';
    return {
	restrict: 'E',
	templateUrl: paiView,
	scope: {
	    indicator: '=indicator'
	},
	link: function(scope){
	    scope.PAIhistoricId = "PAIhistoricId";
	    scope.historicOptions = getHistoOptions();
	    scope.naTextShort = paiConstant.MESSAGES.mesureNotEligible[0];
	    scope.naTextLong = paiConstant.MESSAGES.mesureNotEligible[1];
	    scope.$watch(
		    () => scope.indicator.monthlyHistory,
		    (value) => {
			scope.indicator.monthlyHistory = value;
			scope.displayNaText = hasNaValue();
			}, true);
		
	    scope.indicatorInfo = {
	                "name": scope.indicator.name,
	                "shortLabel": scope.indicator.shortLabel,
	                "longLabel": scope.indicator.longLabel,
	                "contractual": false,
	                "unit": scope.indicator.unit,
	                "tobeConverted": false,
	    };

	    scope.configurations = [
                    {
                        "name": scope.indicator.name,
                        "typeOfData": 'Indicator',
                        "isColor": false,
                        "nonApplicableValue": 'NA',
                        "isTranslated": true,
	                    "unit":scope.indicator.unit
                    },            
	                {
	                    "name": "totalAccessNumber",
	                    "typeOfData": 'Metrics',
	                    "isTranslated": true,
	                    "isColor": false,
	                    "noNaColumn": true
	                },
	    ];
	    
	    function hasNaValue(){
			let containtNA = false;
			angular.forEach(scope.indicator.monthlyHistory, function(month){
				if(month.onPeriod){
				containtNA = containtNA || !month.applicability.applicable;
				}
			});
			return containtNA;
	    }
	    function getHistoOptions() {
			let paiCustRespThrLabel = oraI18n.oraI18nLabels.paiCustRespThrLabel;
			let clientResponsibilityRateMetric = _.find(scope.indicator.metricsDefinitions, (metric) => {
				return metric.name === 'clientResponsibilityRate';
			});

			return {
				highlightClass: 'fill-brand-light-blue',
				axis: {
				y: {
					format: 'p'
				}
				},
				seriesClass: {
				'paiOrange': {
					label: scope.indicator.shortLabel,
					stroke: {
					'class': 'stroke-brand-blue',
					width: 1
					},
					fill: {
					'class': 'fill-brand-blue'
					},
					types: ['line', 'circle']
				},
				'paiCust': {
					label: clientResponsibilityRateMetric.shortLabel,
					stroke: {
					'class': 'stroke-black',
					width: 1
					},
					fill: {
					'class': 'fill-black'
					},
					types: ['line', 'circle']
				},
		
				'paiCustThreshold': {
					label: paiCustRespThrLabel,
					stroke: {
					'class': 'stroke-black',
					width: 1
					},
					fill: {
					'class': 'none'
					},
					types: ['line']
				}
				}
			};
	    }
	}
    };
};

export default paiIndicator;
