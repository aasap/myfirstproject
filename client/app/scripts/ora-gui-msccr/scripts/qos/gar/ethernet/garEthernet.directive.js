'use strict'; //NOSONAR
import oraSccrGarEthernetView from './garEthernet.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraSccrGarInternet
 */

const oraQosGarEthernet = function(newGarConstant) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraSccrGarEthernetView,
        scope: {
            indicator: '=indicator',
            frequency: '=frequency'
        },
        link: function(scope) {
            scope.$watch(
                () => scope.indicator,
                (value) => {
                    scope.indicator = value;
                }, true);

            scope.frequency = scope.frequency;

            scope.garEthernetIndicatorInfo = {
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "contractual": scope.indicator.contractual,
                "unit": scope.indicator.unit,
                "tobeConverted": false,
                "conversionFilterName": '',
                //"isAnnualReport": ((scope.frequency === 'ANN' || scope.frequency === 'yearly') ? true : false)
            };

            scope.garEthernetConfigurations = [
                {
                    "name": "globalEthernetAvailabilityRate",
                    "typeOfData": 'Indicator',
                    "isColor": false,
                    "unit": scope.indicator.unit,
                    "isTranslated": true
                },
            ];

            /*if (scope.frequency === 'ANN' || scope.frequency === 'yearly') {
                scope.histoYearlyData = angular.copy(scope.indicator.annualHistory);
                _.forEach(scope.histoYearlyData, garYearData => {
                    setIndicatorInHistoData(garYearData);
                });
            } else {*/
                scope.histoMonthlyHistData = angular.copy(scope.indicator.monthlyHistory);
                _.forEach(scope.histoMonthlyHistData, garMonthData => {
                    setIndicatorInHistoData(garMonthData);
                });
            //}

            function setIndicatorInHistoData(monthOrYearHistoData) {
                monthOrYearHistoData.indicators = [];
                monthOrYearHistoData.indicators.push({
                    "name": scope.indicator.name,
                    "measure": monthOrYearHistoData.measure,
                    "applicability": monthOrYearHistoData.applicability ? monthOrYearHistoData.applicability : null,
                    //"threshold": monthOrYearHistoData.contractualThreshold
                });
            }

            scope.histoMetricsDefinition = [{
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "unit": scope.indicator.unit
            }];
            scope.historicId = newGarConstant.HISTOGRAM_ID.ethernet;
            scope.historicOptions = {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-blue',
                seriesClass: [{
                    name: scope.indicator.name,
                    bgClass: 'bg-brand-blue',
                    fill: 'fill-brand-blue',
                    typeHisto: 'bar',
                    typeOfData: "indicator"
                }]
            };

            scope.oneBarOption = {
                highlightClass: 'fill-brand-light-blue',
                axis: {
                    y: {
                        format: ',.2'
                    }
                },
                unit: scope.indicator.unit,
                seriesClass: {
                    measure: {
                        label: scope.indicator.name,
                        longLabel : scope.indicator.longLabel,
                        stroke: {
                            'class': 'fill-brand-light-blue'
                        },
                        types: ['bar'],
                        fillColor : 'fill-brand-blue',
                        bgClass: 'bg-brand-blue',
                        noContractual : true,
                    }
                },
                //frequency: scope.frequency
            };

        }
    };
};

export default oraQosGarEthernet;