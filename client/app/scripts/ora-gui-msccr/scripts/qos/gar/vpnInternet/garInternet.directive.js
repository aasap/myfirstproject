'use strict'; //NOSONAR
import oraSccrGarInternetView from './garInternet.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraSccrGarInternet
 */

const oraQosGarInternet = function(newGarConstant) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraSccrGarInternetView,
        scope: {
            indicator: '=indicator',
            frequency: '=frequency'
        },
        link: function(scope) {
            scope.$watch(
                () => scope.indicator,
                (value) => {
                    scope.indicator = value;
                }, true);
            scope.frequency = scope.frequency;
            scope.garInternetIndicatorInfo = {
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "contractual": scope.indicator.contractual,
                "unit": scope.indicator.unit,
                "tobeConverted": false,
                "conversionFilterName": '',
                "isAnnualReport": ((scope.frequency === 'ANN' || scope.frequency === 'yearly') ? true : false)
            };

            scope.garInternetConfigurations = [
                {
                    "name": "globalVpnInternetAvailabilityRate",
                    "typeOfData": 'Indicator',
                    "isColor": false,
                    "unit": scope.indicator.unit,
                    "isTranslated": true
                }
            ];

            if (scope.frequency === 'ANN' || scope.frequency === 'yearly') {
                scope.histoYearlyData = angular.copy(scope.indicator.annualHistory);
                _.forEach(scope.histoYearlyData, garYearData => {
                    setIndicatorInHistoData(garYearData);
                });
            } else {
                scope.histoMonthlyHistData = angular.copy(scope.indicator.monthlyHistory);
                _.forEach(scope.histoMonthlyHistData, garMonthData => {
                    setIndicatorInHistoData(garMonthData);
                });
            }

            function setIndicatorInHistoData(monthOrYearData) {
                monthOrYearData.indicators = [];
                monthOrYearData.indicators.push({
                    "name": scope.indicator.name,
                    "measure": monthOrYearData.measure,
                    "applicability": monthOrYearData.applicability ? monthOrYearData.applicability : null
                });
            }

            scope.histoMetricsDefinition = [{
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "unit": scope.indicator.unit
            }];
            scope.historicId = newGarConstant.HISTOGRAM_ID.vpn;
            scope.historicOptions = {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-blue',
                seriesClass: [{
                    name: scope.indicator.name,
                    bgClass: 'bg-brand-blue',
                    fill: 'fill-brand-blue', 
                    typeHisto: 'bar',
                    typeOfData: "indicator"
                }]
            };

            scope.oneBarOption = {
                highlightClass: 'fill-brand-light-blue',
                axis: {
                    y: {
                        format: ',.2'
                    }
                },
                unit: scope.indicator.unit,
                seriesClass: {
                    measure: {
                        label: scope.indicator.name,
                        longLabel : scope.indicator.longLabel,
                        stroke: {
                            'class': 'fill-brand-light-blue'
                        },
                        types: ['bar'],
                        fillColor : 'fill-brand-blue',
                        bgClass: 'bg-brand-blue',
                        noContractual : true,
                    }
                },
                frequency: scope.frequency
            };

        }
    };
};

export default oraQosGarInternet;