'use strict'; //NOSONAR
import oraQosGttrView from './gttr.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraQosGttr
 */

const oraQosGttr = function(reporTypeConst) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraQosGttrView,
        scope: {
            indicator: '=indicator'
        },
        link: function(scope) {
            scope.$watch(
                () => scope.indicator,
                (value) => {
                    scope.indicator = value;
                    setGttrData();
                }, true);

            function setGttrData() {
                scope.monthlyHistory = scope.indicator.monthlyHistory;
                scope.metricsDefinitions = scope.indicator.metricsDefinitions;
                setMonthlyTableData();
                setStackedHistoData();
            }

            function setMonthlyTableData() {

                scope.indicatorGeneralInfo = {
                    "name": scope.indicator.name,
                    "shortLabel": scope.indicator.shortLabel,
                    "longLabel": scope.indicator.longLabel,
                    "contractual": scope.indicator.contractual,
                    "unit": scope.indicator.unit,
                    "tobeConverted": false,
                    "conversionFilterName": ''
                };

                scope.configurations = [{
                        "name": "GTTRFulfilment",
                        "typeOfData": 'Indicator',
                        "isColor": false,
                        "nonApplicableValue": '-',
                        "isTranslated": true
                    },
                    {
                        "name": "incidentsExceedingGTTR",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "nonApplicableValue": '-'
                    },
                    {
                        "name": "incidentsRespectingGTTR",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "nonApplicableValue": '-'

                    }
                ];
            }

            function setStackedHistoData() {
                scope.historicOptions = {
                    isStartEnable: true,
                    asLegend: true,
                    annualSpecialDisplay: false,
                    highlightClass: 'fill-brand-light-blue',
                    seriesClass: [{
                            name: 'incidentsRespectingGTTR',
                            bgClass: 'bg-green',
                            fill: 'fill-green',
                            typeHisto: 'bar',
                            unit: ''
                        },
                        {
                            name: 'incidentsExceedingGTTR',
                            bgClass: 'bg-red',
                            fill: 'fill-red',
                            typeHisto: 'bar',
                            unit: ''
                        }
                    ]
                };
                scope.historicId = 'msccrHistoGTTR';
            }
             /*Genric detailed record table data */
             scope.type = reporTypeConst.MSCCR;
        }
    };
};

export default oraQosGttr;