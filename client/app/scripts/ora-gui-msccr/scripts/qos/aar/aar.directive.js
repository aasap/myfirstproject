'use strict'; //NOSONAR
import oraQosAarView from './aar.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraQosAar
 */

const oraQosAar = function(reporTypeConst) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraQosAarView,
        scope: {
            indicator: '=indicator'
        },
        link: function(scope) {
            scope.$watch(
                () => scope.indicator,
                (value) => {
                    scope.indicator = value;
                }, true);

            scope.aarIndicatorInfo = {
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "contractual": scope.indicator.contractual,
                "unit": scope.indicator.unit,
                "tobeConverted": false,
                "conversionFilterName": ''
            };

            scope.aarConfigurations = [{
                    "name": "accessAvailabilityRate",
                    "typeOfData": 'Indicator',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "isTranslated": true
                },
                {
                    "name": "accessExceedingAAR",
                    "typeOfData": 'Metrics',
                    "isColor": true,
                    "nonApplicableValue": '-',
                    "isTranslated": true
                },
                {
                    "name": "totalAccessAAR",
                    "typeOfData": 'Metrics',
                    "isColor": false,
                    "nonApplicableValue": '-',
                    "isTranslated": true
                }
            ];

            scope.information = scope.indicator.onPeriod.information;
  
            scope.type = reporTypeConst.MSCCR;
        }
    };
};

export default oraQosAar;