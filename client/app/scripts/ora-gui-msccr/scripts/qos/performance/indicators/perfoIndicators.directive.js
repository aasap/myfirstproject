'use strict'; //NOSONAR
import perfoIndicatorsView from './perfoIndicators.html';
/**
 * @ngdoc directive
 * @name ora-gui-msccr.directive:oraParcOffers
 */

const oraPerfoIndicators = function(oraI18n) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: perfoIndicatorsView,
        scope: {
            section: '=section'
        },
        link: function(scope) {

            scope.$watch(
                () => scope.section,
                (value) => {
                    scope.section = value;
                    setPerfoIndData();
                }, true);

            scope.$watch(() => oraI18n.updateLabels, function() {
                setPerfoIndData();
            });

            function setPerfoIndData() {
                setRoundTripDelayData();
                setPacketLossRateData();
                setJitterData();
            };

            function setRoundTripDelayData() {
                scope.roundTripDelayInfo = _.filter(scope.section.indicators, {
                    "name": "performanceRoundTripDelay"
                })[0];

                scope.roundTripDelayIndInfo = {
                    "name": scope.roundTripDelayInfo.name,
                    "shortLabel": oraI18n.oraI18nLabels.MET_QOS_PERF_ACHIEVE_RATIO_SHORTLABEL,
                    "longLabel": oraI18n.oraI18nLabels.MET_QOS_PERF_ACHIEVE_RATIO_SHORTLABEL,
                    "contractual": scope.roundTripDelayInfo.contractual,
                    "unit": scope.roundTripDelayInfo.unit,
                    "tobeConverted": false,
                    "conversionFilterName": ''
                };

                scope.roundTripDelayConfigurations = [{
                        "name": "performanceRoundTripDelay",
                        "typeOfData": 'Indicator',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": true
                    },
                    {
                        "name": "breachedRoutesNumber",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": true
                    },
                    {
                        "name": "totalRoutesNumber",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": true
                    }
                ];
            }

            function setJitterData() {
                scope.jitterInfo = _.filter(scope.section.indicators, {
                    "name": "performanceJitter"
                })[0];

                scope.jitterIndInfo = {
                    "name": scope.jitterInfo.name,
                    "shortLabel": oraI18n.oraI18nLabels.MET_QOS_PERF_ACHIEVE_RATIO_SHORTLABEL,
                    "longLabel": oraI18n.oraI18nLabels.MET_QOS_PERF_ACHIEVE_RATIO_SHORTLABEL,
                    "contractual": scope.jitterInfo.contractual,
                    "unit": scope.jitterInfo.unit,
                    "tobeConverted": false,
                    "conversionFilterName": ''
                };                
                let isHeaderVisible = !(scope.isApplicable(scope.roundTripDelayInfo)) && !(scope.isApplicable(scope.packetLossRateInfo));
                scope.jitterConfigurations = [{
                        "name": "performanceJitter",
                        "typeOfData": 'Indicator',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": isHeaderVisible
                    },
                    {
                        "name": "breachedRoutesNumber",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": isHeaderVisible
                    },
                    {
                        "name": "totalRoutesNumber",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": isHeaderVisible
                    }
                ];

            }

            function setPacketLossRateData() {
                scope.packetLossRateInfo = _.filter(scope.section.indicators, {
                    "name": "performancePacketLossRate"
                })[0];

                scope.packetLossRateIndInfo = {
                    "name": scope.packetLossRateInfo.name,
                    "shortLabel": oraI18n.oraI18nLabels.MET_QOS_PERF_ACHIEVE_RATIO_SHORTLABEL,
                    "longLabel": oraI18n.oraI18nLabels.MET_QOS_PERF_ACHIEVE_RATIO_SHORTLABEL,
                    "contractual": scope.packetLossRateInfo.contractual,
                    "unit": scope.packetLossRateInfo.unit,
                    "tobeConverted": false,
                    "conversionFilterName": ''
                };                
                let isHeaderVisible = !scope.isApplicable(scope.roundTripDelayInfo);
                scope.packetLossRateConfigurations = [{
                        "name": "performancePacketLossRate",
                        "typeOfData": 'Indicator',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": isHeaderVisible
                    },
                    {
                        "name": "breachedRoutesNumber",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": isHeaderVisible
                    },
                    {
                        "name": "totalRoutesNumber",
                        "typeOfData": 'Metrics',
                        "isColor": false,
                        "isTranslated": true,
                        "isHeaderVisible": isHeaderVisible
                    }
                ];
            }

            scope.isApplicable = function(perfoIndicator){
                if(perfoIndicator){
                    //if indicator is not applicable or is not applicable for all the months, then no title no table               
                    return (perfoIndicator.applicability && perfoIndicator.applicability.applicable) ?  (perfoIndicator.onPeriod && perfoIndicator.onPeriod.applicability && perfoIndicator.onPeriod.applicability.applicable) :false;
                }
                else{
                    return false;
                }
               
            }          
        }
    };
};

export default oraPerfoIndicators;