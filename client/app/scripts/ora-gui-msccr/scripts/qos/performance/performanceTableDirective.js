'use strict'; //NOSONAR
import performanceTableView from './performanceTableView.html';
import './pagination.html';

/**
 * @ngdoc directive
 * @name ora-gui-msccr.directive:performanceTable
 */

const performanceTable = function(performanceNew, performanceConstantNew, appConfig) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: performanceTableView,
        controllerAs: 'vm',
        scope: {},
        controller: function() {

            var vm = this;

            const filter = "engagementAchieved=false";

            vm.detailsValues = [];
            vm.structure = performanceConstantNew.DETAILS;
            vm.numberItemsDisplayed = appConfig.numberItemsDisplay || 20;
            vm.numberOfPages = 0;

            vm.getDate = (month) => {
                return new Date(month);
            }

            vm.callServer = function callServer(tableState) {

                vm.isLoading = true;

                let pagination = tableState.pagination || {};
                let start = pagination.start || 0; // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                let number = vm.numberItemsDisplayed; // Number of entries showed per page.
                let offset = start; // Offset applied when call the service 

                // If values already load once don't call service
                if (vm.detailsValues[start]) {
                    vm.details = vm.detailsValues[start];
                    vm.isLoading = false;
                    return;
                }

                let href = performanceNew.getUrlPerformanceDetailTable();

                if (href) {
                    // Call service to get perf data
                    performanceNew.getPerformanceTable(href, number, offset, filter).then(() => {
                        vm.detailsValues[start] = performanceNew.tableData;
                    }).finally(function() {
                        // Always execute this on both error and success
                        vm.details = performanceNew.tableData;
                        tableState.pagination.numberOfPages = performanceNew.numberOfPages;
                        tableState.pagination.number = vm.numberItemsDisplayed;
                        vm.numberOfPages = performanceNew.numberOfPages;
                        vm.isLoading = false;
                    });
                }
            };
        },
        bindToController: true
    };
};

export default performanceTable;