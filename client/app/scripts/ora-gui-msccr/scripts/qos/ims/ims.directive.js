'use strict'; //NOSONAR
import oraSccrQosImsView from './ims.html';
/**
 * @ngdoc directive
 * @name ora-gui-msccr.directive:oraSccrQosIms
 */

const oraSccrQosIms = function($filter, reporTypeConst, oraI18n) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraSccrQosImsView,
        scope: {
            indicator: '=indicator'
        },
        link: function(scope) {
            
            const setImsData = () => {
                setMonthlytableData();
                setStackedHistogramData();
            }
            
            scope.$watch(() => scope.indicator, function(value) {
                scope.indicator = value;
                if (value) {
                    setImsData();
                }
            });

            scope.sortData = function(data) {
                data.sort(function(a, b) {
                    if (a && b && (a.length === 7) && (b.length === 7)) {
                        return (parseInt(b[6].cellData) - parseInt(a[6].cellData));
                    }
                });

                _.forEach(data, colVal => {
                    let measureCol = _.filter(colVal, {
                        "cellkey": "MSIMeasure"
                    })[0];
                    if (measureCol) {
                        measureCol.cellData = $filter('timeNumber')(measureCol.cellData, oraI18n.selectedCdLang);
                    }
                });
                return data;
            }

            /*Genric monthly table data */
            function setMonthlytableData() {
                scope.monthlyHistory = scope.indicator.monthlyHistory;
                scope.metricsDefinitions = scope.indicator.metricsDefinitions;
                scope.indicatorGeneralInfo = {
                    "name": scope.indicator.name,
                    "shortLabel": scope.indicator.shortLabel,
                    "longLabel": scope.indicator.longLabel,
                    "contractual": scope.indicator.contractual,
                    "unit": scope.indicator.unit
                };
                scope.configurations = [{
                        "name": "MSIFulfilment",
                        "typeOfData": 'Indicator',
                        "isColor": false,
                        "isTranslated": true
                    },
                    {
                        "name": "sitesExceedingMSI",
                        "typeOfData": 'Metrics',
                        "isColor": false
                    },
                    {
                        "name": "sitesRespectingMSI",
                        "typeOfData": 'Metrics',
                        "isColor": false
                    }
                ]
            }

            /*tacked histogram data */
            function setStackedHistogramData() {
                scope.historicOptions = {
                    isStartEnable: true,
                    asLegend: true,
                    annualSpecialDisplay: false,
                    highlightClass: 'fill-brand-light-blue',
                    seriesClass: [{
                            name: 'sitesRespectingMSI',
                            bgClass: 'bg-green',
                            fill: 'fill-green',
                            typeHisto: 'bar',
                            unit: ''
                        },
                        {
                            name: 'sitesExceedingMSI',
                            bgClass: 'bg-red',
                            fill: 'fill-red',
                            typeHisto: 'bar',
                            unit: ''
                        },
                    ]
                };
                scope.historicId = 'mSccrHistoMSI';
            }

            
            scope.type = reporTypeConst.MSCCR;
        }
    };
};

export default oraSccrQosIms;