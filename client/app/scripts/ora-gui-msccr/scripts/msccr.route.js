//VIEWS
import mSccrView from './msccr.html';

const mSccrRoute = function($stateProvider) {
    'ngInject';

    $stateProvider
        .state('ora.msccr', {
            resolve: {
                sccrInit: (loadData) => loadData.init('msccr')
            },
            views: {
                '@ora': {
                    parent: true,
                    templateUrl: mSccrView,
                    controller: 'mSccrCtrl',
                    controllerAs: 'ctrl'
                }
            }
        });
}

export default mSccrRoute;