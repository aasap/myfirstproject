'use strict'; //NOSONAR
import incidentView from './incident.html';
/**
 * @ngdoc directive
 * @name ora-gui-core.directive:oraIncidents
 */

const oraIncidents = function () {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: incidentView,
        scope: {
            indicator: '=indicator'
        },
        link: function (scope) {
            
            scope.indicatorGeneralInfo = {
                "name": scope.indicator.name,
                "shortLabel": scope.indicator.shortLabel,
                "longLabel": scope.indicator.longLabel,
                "unit": scope.indicator.unit,
                "contractual": scope.indicator.contractual
            };

            scope.historicOptions = {
                isStartEnable: true,
                asLegend: true,
                annualSpecialDisplay: false,
                highlightClass: 'fill-brand-light-yellow',
                seriesClass: [{
                    name: 'OrangeResponsibility',
                    bgClass: 'bg-orange',
                    fill: 'fill-orange',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'CustomerResponsibility',
                    bgClass: 'bg-dark',
                    fill: 'fill-black',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'ThirdPartyResponsibility',
                    bgClass: 'bg-incident-tiers-gray',
                    fill: 'fill-incident_tiers',
                    typeHisto: 'bar',
                    unit: ''
                },
                {
                    name: 'OtherResponsibility',
                    bgClass: 'bg-incident-others-gray',
                    fill: 'fill-incident_others',
                    typeHisto: 'bar',
                    unit: ''
                }
                ]
            };

            scope.historicId = 'mSccrHistoINCIDENT';

            scope.configurations = [
                {
                    'name': 'OrangeResponsibility'
                }, {
                    'name': 'CustomerResponsibility'
                }, {
                    'name': 'ThirdPartyResponsibility'
                }, {
                    'name': 'OtherResponsibility'
                }
            ];
        }
    };
};

export default oraIncidents;