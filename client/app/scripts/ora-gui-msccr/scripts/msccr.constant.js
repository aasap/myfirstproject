const constants = {
    performanceConstantNew: {
        DETAILS: {
            title: 'PERF_DET_TITLE_LONGLABEL',
            xlsExport: {
                enabled: true,
                url: ''
            },

            headersLine: 2,
            style: 'table-size-fixed',
            headers: [
                [{
                        name: 'PERF_DET_MONTH_SHORTLABEL',
                        rowspan: 2
                    },
                    {
                        name: 'PERF_DET_ENDPOINTA_SHORTLABEL',
                        rowspan: 2
                    },
                    {
                        name: 'PERF_DET_ENDPOINTB_SHORTLABEL',
                        rowspan: 2
                    },
                    {
                        name: 'PERF_DET_SERVICECLASS_SHORTLABEL',
                        rowspan: 2,
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_SAMPLERATE_SHORTLABEL',
                        rowspan: 2,
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_ROUNDTRIPDELAY_SHORTLABEL',
                        colspan: 2
                    },
                    {
                        name: 'PERF_DET_PACKETLOSSRATE_SHORTLABEL',
                        colspan: 2
                    },
                    {
                        name: 'PERF_DET_JITTER_SHORTLABEL',
                        colspan: 2
                    }
                ],
                [{},
                    {},
                    {},
                    {},
                    {},
                    {
                        name: 'PERF_DET_THRESHOLD_SHORTLABEL',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_MEASURE_SHORTLABEL'
                    },
                    {
                        name: 'PERF_DET_THRESHOLD_SHORTLABEL',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_MEASURE_SHORTLABEL'
                    },
                    {
                        name: 'PERF_DET_THRESHOLD_SHORTLABEL',
                        class: 'custom_break_word'
                    },
                    {
                        name: 'PERF_DET_MEASURE_SHORTLABEL'
                    }
                ]
            ]
        },
        MESSAGES: {
            recalculating: 'GLOBAL_RECALC_WARN_LONGLABEL'
        }
    }
}

export default constants;
