'use strict'; //NOSONAR
import oraParcOptionsView from './options.html';
/**
 * @ngdoc directive
 * @name ora-gui-msccr.directive:oraParcOptions
 */
const oraParcOptions = function($filter, oraI18n) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: oraParcOptionsView,
        scope: {
            indicator: '=indicator'
        },
        link: function(scope) {
            scope.subscribedMetrics = ["applicativePerformanceEAMRiverbed", "applicativePerformanceNetworkBoost", "personalizedSupervision", "digitalMonitoring"];

            scope.$watch(
                () => scope.indicator,
                (value) => {
                    if (!!value) {
                        scope.optionsLabel = value.longLabel;
                        scope.onPeriod = value.onPeriod;
                        scope.metricsDefinitions = value.metricsDefinitions;
                        scope.optionsIndInfo = {
                            'name': 'Options'
                        };
                        setMetricsData();
                    }
                }, true);

            function setMetricsData() {
                scope.onPeriodCopy = angular.copy(scope.onPeriod);
                if (!!scope.onPeriodCopy && !!scope.onPeriodCopy.metrics) {
                    _.forEach(scope.onPeriodCopy.metrics, metric => {
                        if (!!metric.measure && metric.measure > 0) {
                            if (_.indexOf(scope.subscribedMetrics, metric.name) > -1) {
                                metric.isSubscribed = true;
                            } else {
                                metric.isSubscribed = null;
                            }
                        } else {
                            metric.isSubscribed = false;
                        }
                        var metricsDef = _.find(scope.metricsDefinitions, function(obj) {
                            return obj.name === metric.name;
                        });
                        metric.longLabel = metricsDef.longLabel;
                    });
                }
                sortMetricsData();
            }

            function sortMetricsData() {

                let optionsWithMeasure = _.filter(scope.onPeriodCopy.metrics, {
                    "isSubscribed": null
                });
                optionsWithMeasure = $filter('orderBy')(optionsWithMeasure, ['-measure', 'name']);
                let subscribedOptions = _.filter(scope.onPeriodCopy.metrics, {
                    "isSubscribed": true
                });
                subscribedOptions = $filter('orderBy')(subscribedOptions, 'longLabel');
                let nonSubscribedOptions = _.filter(scope.onPeriodCopy.metrics, {
                    "isSubscribed": false
                });
                nonSubscribedOptions = $filter('orderBy')(nonSubscribedOptions, 'longLabel');

                scope.onPeriodCopy.metrics = optionsWithMeasure.concat(subscribedOptions).concat(nonSubscribedOptions);
            }
        }
    };
};

export default oraParcOptions;