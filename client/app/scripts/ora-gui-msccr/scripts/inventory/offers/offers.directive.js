'use strict'; //NOSONAR
import parcOffersView from './offers.html';
/**
 * @ngdoc directive
 * @name ora-gui-msccr.directive:oraParcOffers
 */

const oraParcOffers = function($filter) {
    'ngInject';
    return {
        restrict: 'E',
        templateUrl: parcOffersView,
        scope: {
            onPeriod: '=onPeriod',
            monthlyHistory: '=monthlyHistory',
            metricsDefinitions: '=metricsDefinitions',
            section: '=section'
        },
        link: function(scope) {

            scope.$watchGroup(['onPeriod', 'monthlyHistory', 'metricsDefinitions', 'section'],
                (newValues) => {
                    scope.onPeriod = newValues[0];
                    scope.monthlyHistory = newValues[1];
                    scope.metricsDefinitions = newValues[2];
                    scope.section = newValues[3];
                    setOffersData();
                    setSitesData();
                    setOptionsData();
                    setDataForHistogram();
                }, true);

            function setOffersData() {
                scope.offersIndicatorInfo = _.filter(scope.section.indicators, {
                    "name": "SCCROffersEvolution"
                })[0];
                scope.offersMonthlyHistory = scope.monthlyHistory;
                scope.offersMetricsDefinitions = scope.metricsDefinitions;
                scope.offersOnPeriod = scope.onPeriod;
                scope.offersIndInfo = {'name':'Offers'};                
                scope.offersOnPeriod.metrics = _.filter(scope.offersOnPeriod.metrics, function(metric) {
                    return !(metric.measure === 0 && metric.evolution === 0);
                });
                setMetricsOrder();
            }

            function setSitesData() {
                scope.sitesIndicatorInfo = _.filter(scope.section.indicators, {
                    "name": "SCCRSitesEvolution"
                })[0];
                scope.sitesMonthlyHistory = scope.sitesIndicatorInfo.monthlyHistory;
                scope.sitesMetricsDefinitions = scope.sitesIndicatorInfo.metricsDefinitions;
                scope.sitesOnPeriod = scope.sitesIndicatorInfo.onPeriod;
                scope.sitesIndInfo= {'name' :'Sites', 'unit':scope.sitesIndicatorInfo.unit};     
                   
            }

            function setOptionsData() {
                scope.optionsIndicatorInfo = _.filter(scope.section.indicators, {
                    "name": "SCCROptionsEvolution"
                })[0];               
            }

            function setMetricsOrder() {
                if(scope.offersMetricsDefinitions){
                    scope.offersMetricsDefinitions = $filter('orderBy')(scope.offersMetricsDefinitions, ['shortLabel','name']);
                    let item_order = []; //scope.metricsDefinitions.map(p => p.name);
                    angular.forEach( scope.offersMetricsDefinitions, function(metric){
                	item_order.push(metric.name);
                    });
                    scope.offersOnPeriod.metrics = mapOrder(scope.offersOnPeriod.metrics, item_order, 'name');
                }
            }

            function mapOrder(array, order, key) {
                array.sort(function(a, b) {
                    var A = a[key],
                        B = b[key];

                    if (order.indexOf(A) < order.indexOf(B)) {
                        return -1;
                    } else {
                        return 1;
                    }

                });
                return array;
            }

            function setDataForHistogram() {
                scope.historicId = 'sccrNewParcOffers';
                scope.historicOptions = {
                    isStartEnable: true,
                    asLegend: true,
                    annualSpecialDisplay: false,
                    highlightClass: 'fill-brand-light-green',
                    seriesClass: [{
                            name: scope.sitesIndicatorInfo.name,
                            bgClass: 'bg-brand-blue',
                            fill: 'fill-brand-blue',
                            typeHisto: 'bar',
                            typeOfData: "indicator"
                        },
                        {
                            name: scope.offersIndicatorInfo.name,
                            bgClass: 'bg-brand-black',
                            fill: 'fill-brand-black',
                            typeHisto: 'bar',
                            typeOfData: "indicator"
                        }
                    ]
                };
                scope.histoMonthlyHistData = angular.copy(scope.offersMonthlyHistory);
                scope.histoMetricsDefinition = [
                    {
                        "name": scope.sitesIndicatorInfo.name,
                        "shortLabel": scope.sitesIndicatorInfo.shortLabel,
                        "longLabel": scope.sitesIndicatorInfo.longLabel,
                        "unit": scope.sitesIndicatorInfo.unit
                    },
                    {
                        "name": scope.offersIndicatorInfo.name,
                        "shortLabel": scope.offersIndicatorInfo.shortLabel,
                        "longLabel": scope.offersIndicatorInfo.longLabel,
                        "unit": scope.offersIndicatorInfo.unit
                    }
                ];
                _.forEach(scope.histoMonthlyHistData, offersMonthData => {
                    offersMonthData.indicators = [];    
                    _.forEach(scope.sitesMonthlyHistory, sitesMonthlyData => {
                        if (offersMonthData.month === sitesMonthlyData.month) {                                                                            
                            offersMonthData.indicators.push({
                                "name": scope.sitesIndicatorInfo.name,
                                "measure": sitesMonthlyData.measure,
                                "applicability" : sitesMonthlyData.applicability ? sitesMonthlyData.applicability :null
                            });                                               
                            offersMonthData.indicators.push({
                                "name": scope.offersIndicatorInfo.name,
                                "measure": offersMonthData.measure,
                                "applicability" : offersMonthData.applicability ? offersMonthData.applicability :null
                            });
                          
                            
                        }
                    })
                });
            }
        }
    };
};

export default oraParcOffers;