import 'angular';
import 'lodash';

import './ora-gui-core/scripts/core.module.js';
import './ora-gui-services/scripts/services.module.js';
import './ora-gui-msccr/scripts/msccr.module.js';


import MainController from './app.controller.js';

//FILTERS
import applyclass from './filters/applyclass.js';
import capitalize from './filters/capitalize.js';
import {periodDisplay} from '@05o/libraries'; //'./filters/perioddisplay.js';
import perioddisplayfrequency from './filters/perioddisplayfrequency.js';
import {periodNamingDisplay} from '@05o/libraries'; //'./filters/periodnamingdisplay.js';
import periodvalidation from './filters/periodvalidation.js';
import {signNumber} from '@05o/libraries'; // './filters/signnumber.js';
import {startFrom} from '@05o/libraries'; //'./filters/startFrom.js';
import {timeNumber} from '@05o/libraries'; //'./filters/timenumber.js';
import {unitConverter} from '@05o/libraries'; //'./filters/unitConverter.js';
import {utc} from '@05o/libraries'; ////'./filters/utc.js';
import sccrNewIndicatorsOrder from './filters/sccrNewIndicatorsOrder.js';
import {genericPeriodDisplay} from '@05o/libraries'; //'./filters/sccvPeriodDisplay.js';
import sccrIndicatorApplicability from './filters/sccrIndicatorApplicability.js';
import {printPeriod} from '@05o/libraries';

//CSS

import 'boosted/dist/css/bootstrap-orange2015.css';
import 'boosted/dist/css/bootstrap-orange-theme2015.min.css';
import 'boosted/dist/css/boosted2015.min.css';
import 'angularjs-toaster/toaster.css';
import 'nvd3/build/nv.d3.css';
import 'textangular/dist/textAngular.css';
import 'components-font-awesome/css/font-awesome.css';

import '../styles/ora/less/ora.less';
import '../styles/ora/icon.less';
import sectionsOrder from "./filters/sectionsOrder";

export default angular
    .module('ora-internal-gui', [
        'ora-gui-msccr',
    ])
    .filter({
        'periodDisplayFrequency': perioddisplayfrequency,
        'applyClass': applyclass,
        'capitalize': capitalize,
        'periodDisplay': periodDisplay,
        'periodNamingDisplay': periodNamingDisplay,
        'periodValidation': periodvalidation,
        'signNumber': signNumber,
        'startFrom': startFrom,
        'timeNumber': timeNumber,
        'unitConverter': unitConverter,
        'utc': utc,
        'sccrNewIndicatorsOrder': sccrNewIndicatorsOrder,
        'sectionsOrder': sectionsOrder,
        'sccvPeriodDisplay': genericPeriodDisplay,
        'sccrIndicatorApplicability': sccrIndicatorApplicability,
        'printPeriod': printPeriod
    }).controller('MainController', MainController).name;