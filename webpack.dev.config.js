var webpack = require('webpack');
var path = require('path');
var config = require('./webpack.config');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HappyPack = require('happypack');

var ExtractTextPlugin = require("extract-text-webpack-plugin");
let extractCSS = new ExtractTextPlugin('css/vendor.css');
let extractLESS = new ExtractTextPlugin('css/style.css');

config.output = {
    filename: '[name].bundle.js',
    publicPath: '/',
    path: path.resolve(__dirname, 'client/app')
};

config.module.rules = config.module.rules.concat([{
        test: /\.js$/,
        exclude: [/app\/lib/, /node_modules/],
        use: [{
                loader: 'ng-annotate-loader'
            },
            {
                loader: 'happypack/loader'
            }
        ]
    },
    {
        test: /\.html$/,
        use: [{
                loader: 'ngtemplate-loader?relativeTo=' + (path.resolve(__dirname, './client/app'))
            },
            {
                loader: 'html-loader'
            }
        ]
    },
    {
        test: /\.(css|scss|sass)$/,
        use: extractCSS.extract({
            fallback: 'style-loader',
            use: [{
                    loader: 'css-loader',
                    options: {
                        url: false
                    }
                },
                {
                    loader: 'sass-loader'
                }
            ]
        })
    },
    {
        test: /\.less$/,
        use: extractLESS.extract({
            fallback: 'style-loader',
            use: [{
                    loader: 'css-loader',
                    options: {
                        url: false
                    }
                },
                {
                    loader: 'less-loader'
                }
            ]
        })
    },
    {
        test: /\.(woff(2)?|eot|ttf|svg)(.?[a-z0-9=.]+)?$/,
        use: [{
            loader: 'file-loader',
            options: {
                name: 'fonts/[name].[ext]'
            }
        }]
    },
    {
        test: /\.(png|jpeg|jpg)(.?[a-z0-9=.]+)?$/,
        use: [{
            loader: 'file-loader',
            options: {
                name: 'assets/images/[name].[ext]'
            }
        }]
    }
]);

config.plugins = config.plugins.concat([

    //Use to export css in external files vendor.css and style.css
    extractCSS,
    extractLESS,

    // Enable ng-scope and ng-binding from generated HTML
    new webpack.DefinePlugin({
        "$$DEBUG_INFO_ENABLED": true,
        "$$REDIRECTION_DISABLED": false
    }),

    new HappyPack({
        loaders: ['babel-loader?presets[]=env']
    }),

    // Injects bundles in your index.html instead of wiring all manually.
    // It also adds hash to all injected assets so we don't have problems
    // with cache purging during deployment.
    new HtmlWebpackPlugin({
        template: 'client/app/index.html',
        favicon: 'client/app/favicon.ico',
        inject: 'body',
        hash: true
    }),

    // Static css files needed for static html pages (maintenance, tiemout ...)
    new CopyWebpackPlugin([{
            from: 'client/app/timeout.html',
            to: ''
        },
        {
            from: 'client/app/403.html',
            to: ''
        },
        {
            from: 'client/app/404.html',
            to: ''
        },
        {
            from: 'client/app/maintenance.html',
            to: ''
        },
        {
            from: 'client/app/timeoutext.html',
            to: ''
        },
        {
            from: 'client/app/favicon.ico',
            to: ''
        },
        {
            from: 'client/app/config/config.json',
            to: 'config'
        },
        {
            from: 'client/app/mocks',
            to: 'mocks'
        },
        {
            from: 'client/app/i18n',
            to: 'oragui-msccr/i18n'
        }
    ]),

    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

    // Adds webpack HMR support. It act's like livereload,
    // reloading page after webpack rebuilt modules.
    // It also updates stylesheets and inline assets without page reloading.
    new webpack.HotModuleReplacementPlugin()
]);

module.exports = config;