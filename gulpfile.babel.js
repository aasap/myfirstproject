'use strict';

import gulp from 'gulp';
import webpack from 'webpack';
import path from 'path';
import sync from 'run-sequence';
import rename from 'gulp-rename';
import replace from 'gulp-replace';
import template from 'gulp-template';
import fs from 'fs';
import yargs from 'yargs';
import lodash from 'lodash';
import gutil from 'gulp-util';
import serve from 'browser-sync';
import del from 'del';
import clean from 'gulp-clean';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import colorsSupported from 'supports-color';
import historyApiFallback from 'connect-history-api-fallback';
import proxyMiddleware from 'http-proxy-middleware';
var Server = require('karma').Server;

let root = 'client/app';

// helper method for resolving paths
let resolveToApp = (glob = '') => {
  return path.join(root, '', glob); // app/{glob}
};

let resolveToComponents = (glob = '') => {
  return path.join(root, '/components', glob); // app/components/{glob}
};

// map of all paths
let paths = {
  entry: ['babel-polyfill', path.join(__dirname, root, '/scripts/app.js')],
  output: root,
  dest: path.join(__dirname, '/dist')
};

// use webpack.config.js to build modules
gulp.task('webpack', ['clean'], cb => {
  const config = require('./webpack.dist.config');
  config.entry.app = paths.entry;

  webpack(config, (err, stats) => {
    if (err) {
      throw new gutil.PluginError('webpack', err);
    }

    gutil.log(
      '[webpack]',
      stats.toString({
        colors: colorsSupported,
        chunks: false,
        errorDetails: true
      })
    );

    cb();
  });
});

gulp.task('move-files', cb => {
  // replace base href in index.html file
  gulp
    .src(['./.tmp/index.html'], {
      base: './.tmp/'
    })
    .pipe(replace('<base href="/" />', '<base href="/oragui-msccr/" />'))
    .pipe(gulp.dest('./dist'));

  gulp
    .src(['./.tmp/css/*', './.tmp/app.bundle.js', './.tmp/vendor.bundle.js', './.tmp/app.bundle.js.gz', './.tmp/vendor.bundle.js.gz'], {
      base: './.tmp/'
    })
    .pipe(gulp.dest('./dist'));

  gulp
    .src(['./.tmp/fonts/*'], {
      base: './.tmp/'
    })
    .pipe(gulp.dest('./dist/css'));

  gulp
    .src(['./.tmp/assets/images/*'], {
      base: './.tmp/'
    })
    .pipe(gulp.dest('./dist'));
});

gulp.task('serve', () => {
  const config = require('./webpack.dev.config');
  config.entry.app = [
    // this modules required to make HRM working
    // it responsible for all this webpack magic
    'webpack-hot-middleware/client?reload=true'
    // application entry point
  ].concat(paths.entry);

  var compiler = webpack(config);

  var proxy = proxyMiddleware('/ora-internal-provider-api', {
    target: 'http://10.24.240.85',
    //target: 'http://localhost:8080',
    changeOrigin: true,
    onProxyReq(proxyReq, req, res) {
      // add custom header to request
      /*proxyReq.setHeader('sm_universalid', 'HFJV9818');
      proxyReq.setHeader('sccr_api_disabled', 'true');
      proxyReq.setHeader('sccv_api_disabled', 'true');*/
      // or log the req
  }

  });

  var proxySccr = proxyMiddleware('/ora-reporting-api-msccr', {
    target: 'http://localhost:3003',
    changeOrigin: true
  });

  serve({
    port: process.env.PORT || 4000,
    open: true,
    startPath: '/oragui-msccr/',
    middleware: [
      proxy,
      proxySccr,
      historyApiFallback(),
      webpackDevMiddleware(compiler, {
        stats: {
          colors: colorsSupported,
          chunks: false,
          modules: false
        }
        //publicPath: config.output.publicPath
      }),
      webpackHotMiddleware(compiler)
    ]
  });
});

gulp.task('watch', ['serve']);

gulp.task('clean', cb => {
  del([paths.dest, path.join(__dirname, '/.tmp')]).then(function(paths) {
    gutil.log('[clean]', paths);
    cb();
  });
});

/**
 * Run test once and exit
 */
gulp.task('test', function(done) {
  new Server(
    {
      configFile: __dirname + '/karma.conf.js',
      singleRun: true
    },
    done
  ).start();
});

gulp.task('ngdocs', [], function() {
  var gulpDocs = require('gulp-ngdocs');
  var options = {
    html5Mode: false,
    navTemplate: 'client/app/documentation/html/nav.html',
    image: 'client/app/images/obs-left-black.png',
    title: false,
    startPage: '/guide',
    styles: [
      'client/app/documentation/css/boosted.min.css',
      'client/app/documentation/css/boosted2015.min.css',
      'client/app/documentation/css/boostedIE8.min.css',
      'client/app/documentation/css/boostedIE82015.min.css',
      'client/app/documentation/css/bootstrap-orange.min.css',
      'client/app/documentation/css/bootstrap-orange-theme.min.css',
      'client/app/documentation/css/bootstrap-orange-theme2015.min.css',
      'client/app/documentation/css/bootstrap-orange-themeIE8.min.css',
      'client/app/documentation/css/bootstrap-orange-themeIE82015.min.css',
      'client/app/documentation/css/bootstrap-orange2015.min.css',
      'client/app/documentation/css/bootstrap-orangeIE8.min.css',
      'client/app/documentation/css/bootstrap-orangeIE82015.min.css',
      'client/app/documentation/css/styles.css'
    ]
  };
  return gulpDocs
    .sections({
      api: {
        glob: ['client/app/scripts/**/*.js', 'client/app/documentation/api/index.ngdoc'],
        title: 'OR&A Documentation'
      },
      guide: {
        glob: ['client/app/documentation/guide/*.ngdoc'],
        title: 'OR&A Style Guide'
      }
    })
    .pipe(gulpDocs.process(options))
    .pipe(gulp.dest('./docs'));
});

gulp.task('connect_ngdocs', function() {
  var connect = require('gulp-connect');
  connect.server({
    root: 'docs',
    livereload: false,
    fallback: 'docs/index.html',
    port: 8083
  });
});

gulp.task('default', ['watch']);
