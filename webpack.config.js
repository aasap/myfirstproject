var path = require('path');
var webpack = require('webpack');
// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

process.noDeprecation = true;

module.exports = {
  devtool: 'source-map',
  entry: {},
  module: {
    rules: []
  },

  node: {
    fs: 'empty'
  },

  plugins: [
    // Bypass if you not want to analyse bundle
    // new BundleAnalyzerPlugin(),

    // Automatically move all modules defined outside of application directory to vendor bundle.
    // If you are using more complicated project structure, consider to specify common chunks manually.
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function(module, count) {
        return module.resource && module.resource.indexOf(path.resolve(__dirname, 'client')) === -1;
      }
    }),

    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      moment: 'moment/moment.js'
    })
  ]
};
